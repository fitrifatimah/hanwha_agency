﻿using Microsoft.IdentityModel.Tokens;
using Sales.Illustration.Web.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Sales.Illustration.Web.Controllers;
using static Sales.Illustration.Web.Controllers.LoginApiController;

namespace Sales.Illustration.Web.Controllers
{
    public class ApiRiskClassController : ApiController
    {
        //public static string secret = "cc9d2f0b-8434-4da1-b4cd-0822f05fbe35";

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(string token)
        {
            if (ValidateToken(token) != null)
            {
                using (OnlineEntities on = new OnlineEntities())
                {

                    var dt = on.RiskClasses.AsNoTracking().ToList();

                    ResponseData res = new ResponseData();
                    res.Message = "Success";
                    res.Status = "OK";
                    res.Data = dt;

                    return Request.CreateResponse(HttpStatusCode.OK, res);
                    //return on.RiskClasses.AsNoTracking().ToList();
                }
            }
            else
            {
                ResponseToken res = new ResponseToken();
                res.Message = "Invalid Token";
                res.Status = "Failed";
                res.Token = null;

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public static string ValidateToken(string token)
        {
            if (token == null)
                return null;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Global.secret);
            try
            {
                SecurityToken validatedToken;
                tokenHandler.ValidateToken(token, new TokenValidationParameters { ValidateIssuerSigningKey = true, IssuerSigningKey = new SymmetricSecurityKey(key), ValidateIssuer = false, ValidateAudience = false, ClockSkew = TimeSpan.Zero }, out validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                var username = jwtToken.Claims.First(x => x.Type == "username").Value;
                return username;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
        private class ResponseData
        {
            public string Message { get; set; }
            public string Status { get; set; }
            public List<RiskClass> Data { get; set; }
        }

        private class ResponseToken
        {
            public string Message { get; set; }
            public string Status { get; set; }
            public string Token { get; set; }
            //public List<RiskClass> Data { get; set; }
        }
    }
}