﻿using Microsoft.IdentityModel.Tokens;
using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{

    public class LoginApiSignatureController : Controller
    {
        Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
        public static string secret = "cc9d2f0b-8434-4da1-b4cd-0822f05fbe35";
        public static string salt = "sehatitumahal";

        //[HttpGet, ValidateAntiForgeryToken]
        [HttpGet]
        public ActionResult CheckLogin(string Username, string Password, string signature)//usrename, pas, 
        {
           
            var Key = "";
            var serverDeploy = ConfigurationManager.AppSettings["ServerDeploy"];

            if (serverDeploy == "UAT")//Hanya Untuk UAT
            {
                Key = "ScNqI_YWcc9iAjpHcRAJfQyCflw";  
            }
            else
            {
                Key = getSignature(Username, Password);
            }

                
            if (Key == signature)
            {
                using (OnlineEntities on = new OnlineEntities())
                {
                    //System.Web.HttpContext.Current.Session["Time"] = DateTime.Now;
                    //System.Web.HttpContext.Current.Session.Timeout = 1; // In minutes.

                    LoginDataResult dataResult;
                    var onlineLogin = false;

                    if (serverDeploy == "UAT")//Hanya Untuk UAT
                    {
                        dataResult = obj.LoginOfflineUAT(Username);
                        onlineLogin = true;
                    }
                    else
                    {
                        try
                        {

                            dataResult = obj.LoginOnlie(Username, Password);
                            onlineLogin = true;
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                dataResult = obj.LoginOffline(Username, Generator.toPassword(Password));
                            }
                            catch (Exception exception)
                            {
                                TempData["msg"] = "Please check again your username or password!";
                                return RedirectToAction("Index");
                            }
                        }
                    }

                    if (dataResult == null || dataResult.result_code == LoginResultCode.AGENT_CODE_NOT_EXIST || dataResult.result_code == LoginResultCode.WRONG_PASSWORD)
                    {
                        TempData["msg"] = "Wrong Agent Code or Password";
                        return RedirectToAction("Index");
                    }
                    else if (dataResult.result_code == LoginResultCode.TERMINATE)
                    {
                        TempData["msg"] = "Agent is no more active";
                        return RedirectToAction("Index");
                    }

                    if (Username.Length > 10)
                        _session.AgentCode = Username.ToString().Replace(".", "").Substring(0, 10);
                    else
                        _session.AgentCode = Username;

                    _session.AgentName = dataResult.data.agent_name;
                    _session.AAJILicense = dataResult.data.license_aaji;
                    _session.JoinDate = dataResult.data.join_date;
                    _session.Email = dataResult.data.email;
                    _session.RoleUser = dataResult.data.roleuser;
                    _session.AgentType = (_session.AgentCode).Substring(0, 1) == "7" ? "Digital Agent" : "Standard Agent";
                    System.Web.HttpContext.Current.Session.Timeout = 1440;
                    try
                    {
                        obj.CreateLog(_session.AgentCode, dataResult.data.agent_name);
                    }
                    catch (Exception ex) { }

                    //var token = GenerateToken(Username, DateTime.UtcNow.AddMinutes(1));

                    //if (ValidateToken(token) != null)
                    //{
                        return RedirectToAction("Index", "Unitlink");

                    //}
                    //else
                    //{
                    //    return RedirectToAction("Index");
                    //}
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public static string ValidateToken(string token)
        {
            if (token == null)
                return null;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            try
            {
                SecurityToken validatedToken;
                tokenHandler.ValidateToken(token, new TokenValidationParameters { ValidateIssuerSigningKey = true, IssuerSigningKey = new SymmetricSecurityKey(key), ValidateIssuer = false, ValidateAudience = false, ClockSkew = TimeSpan.Zero }, out validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                var username = jwtToken.Claims.First(x => x.Type == "username").Value;
                return username;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private class ResponseToken
        {
            public string Message { get; set; }
            public string Status { get; set; }
            public string Token { get; set; }
            //public List<RiskClass> Data { get; set; }
        }

        public static string GenerateToken(string username, DateTime? expiry = null)
        {
            if (expiry == null)
            {
                expiry = DateTime.UtcNow.AddHours(1);
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor { Subject = new ClaimsIdentity(new[] { new Claim("username", username) }), Expires = expiry, SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature) };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string getSignature(string username, string pass)
        {
            UnicodeEncoding unicode = new UnicodeEncoding();

            SHA1 SH1 = new SHA1CryptoServiceProvider();

            var gabungan = username + pass + salt;
            var byteData = unicode.GetBytes(gabungan);
            var hash = SH1.ComputeHash(byteData);
            var base64 =  Base64UrlEncoder.Encode(hash);//Convert.ToBase64String(hash);

            return base64;
        }
            
        public class Param
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string key { get; set; }
        }
    }
}
