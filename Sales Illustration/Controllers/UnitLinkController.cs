﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales.Illustration.Web.ViewModel;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Extensions;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Services;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using System.Threading;
using System.Globalization;
using CrystalDecisions.Shared;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class UnitLinkController : Controller
    {
        Sales.Illustration.Web.Models.MainModel obj = new Models.MainModel();
        //
        // GET: /Product/

        public ActionResult Index()
        {
            _session.dataNasabah = null;
            _session.premiInvestmentData = null;
            _session.riderData = null;
            _session.addInsData = null;
            _session.topUpWithData = null;
            _session.insCostRatioData = null;
            _session.TransLogCode = null;

            return RedirectToAction("NasabahData");
        }

        public ActionResult NasabahData()
        {
            var model = new NasabahDataViewModel();

            if (_session.dataNasabah != null)
                model = _session.dataNasabah;

            ViewData["riskClass"] = obj.GetActiveRiskClass().OrderBy(x => x.RiskClassDesc).Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            ViewData["products"] = obj.GetProductByType("ul").OrderBy(x => x.OrderNo).Select(x => new SelectListItem { Text = x.ProductName, Value = x.ProductCode });

            return View("NasabahData", model);
        }

        [HttpPost]
        public ActionResult ValidateNasabahData(NasabahDataViewModel data)
        {
            if (data.TertanggungUtama == "Ya")
            {
                data.NamaTertanggungUtama = null;
                data.JenisKelaminTertanggungUtama = null;
                data.TanggalLahirTertanggungUtama = null;
                data.StatusPekerjaanTertanggungUtama = null;
                data.KelasPekerjaanTertanggungUtama = null;
                data.UmurTertanggungUtama = null;
                data.Relation = null;
            }else if(data.NamaProduk == "HLWORK" || data.Relation == "Karyawan" || data.Relation == "Pengurus Yayasan")
            {                
                data.JenisKelaminPemegangPolis = null;
                data.TanggalLahirPemegangPolis = null;
                data.StatusPekerjaanPemegangPolis = null;
                data.KelasPekerjaanPemegangPolis = null;
                data.UmurPemegangPolis = 0;
            }

            _session.dataNasabah = data;

            return RedirectToAction("PremiumAndInvestment");
        }

        public ActionResult BackToNasabahData()
        {
            if (_session.premiInvestmentData != null)
                _session.premiInvestmentData = null;

            return RedirectToAction("NasabahData");
        }

        public ActionResult PremiumAndInvestment()
        {
            var model = new PremiumAndInvestmentViewModel
            {
                Investments = obj.GetFundByProduct(_session.dataNasabah.NamaProduk).Select(x => new Domain.Investment() { InvestmentName = x.FundName, InvestmentCode = x.FundCode }).ToList()
            };

            if (_session.premiInvestmentData != null)
            {
                model = _session.premiInvestmentData;
                //string url = Request.UrlReferrer == null ? "PremiumAndInvestment" : Request.UrlReferrer.ToString();

                //if (!url.Contains("PremiumAndInvestment"))
                //{

                //    var caraBayar = model.CaraBayar == null || model.CaraBayar == "" ? 1 : Convert.ToInt32(model.CaraBayar);
                //    model.PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.PremiBerkala) / caraBayar);
                //    model.TopupBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.TopupBerkala) / caraBayar);
                //}
            }

            ViewData["currencies"] = obj.GetActiveCurrency().Select(m => new SelectListItem { Text = m.CurrName, Value = m.CurrCode });
            ViewData["paymentMethods"] = obj.GetActivePaymentMethod(_session.dataNasabah.NamaProduk).Select(m => new SelectListItem { Text = m.PMName, Value = m.PMCode.ToString() });
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductCategory"] = product.ProductCategory;
            ViewData["ProductCode"] = product.ProductCode;
            ViewData["ProductTitle"] = product.ProductName;
            
            if (_session.dataNasabah.TertanggungUtama == "Ya")
            {
                ViewData["Umur_tertanggung"] = _session.dataNasabah.UmurPemegangPolis;
                ViewData["gender_tertanggung"] = _session.dataNasabah.JenisKelaminPemegangPolis;
            }
            else
            { 
                ViewData["Umur_tertanggung"] = _session.dataNasabah.UmurTertanggungUtama;
                ViewData["gender_tertanggung"] = _session.dataNasabah.JenisKelaminTertanggungUtama;
            }
            return View("PremiumAndInvestment", model);
        }

        [HttpPost]
        public ActionResult ValidatePremiumAndInvestmentData(PremiumAndInvestmentViewModel model)
        {
            var caraBayar = model.CaraBayar == null ? 1 : Convert.ToInt32(model.CaraBayar);
            model.PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.PremiBerkala) * caraBayar);
            model.TopupBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.TopupBerkala) * caraBayar);
            _session.premiInvestmentData = model;

            SummaryViewModel data = new SummaryViewModel();
            data.Nasabah = _session.dataNasabah;
            data.Premi = model;

            var result = MainServices.CalculateFundBenefit(data);
            //var product = obj.GetProduct(_session.dataNasabah.NamaProduk);

            //if (product.ProductCategory.Equals("single"))
            //{
            //    if (!String.IsNullOrEmpty(model.TopupBerkala))
            //    {
            //        var topup = new TopUpWithdrawalViewModel();
            //        topup.TopupWithdrawals = new List<TopUpWithdrawal>();

            //        var itemTopup = new TopUpWithdrawal();
            //        itemTopup.IsTopUp = true;
            //        itemTopup.Year = 1;
            //        itemTopup.Amount = model.TopupBerkala;
            //        topup.TopupWithdrawals.Add(itemTopup);

            //        _session.topUpWithData = topup;
            //    }
            //}

            if (result.ErrorMsg != null && result.ErrorMsg.Length > 0)
            {
                TempData["errorMsg"] = result.ErrorMsg[0];
                return RedirectToAction("BackToPremiumAndInvestment");
            }

            return RedirectToAction("Rider");
        }

        public ActionResult BackToPremiumAndInvestment()
        {
            if (_session.riderData != null)
                _session.riderData = null;

            if (_session.addInsData != null)
                _session.addInsData = null;

            var caraBayar = _session.premiInvestmentData.CaraBayar == null || _session.premiInvestmentData.CaraBayar == "" ? 1 : Convert.ToInt32(_session.premiInvestmentData.CaraBayar);
            _session.premiInvestmentData.PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala) / caraBayar);
            _session.premiInvestmentData.TopupBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.TopupBerkala) / caraBayar);

            return RedirectToAction("PremiumAndInvestment");
        }

        public ActionResult Rider()
        {
            var model = new RiderViewModel();


            if (_session.riderData != null)
            {
                model = _session.riderData;
            }
            else
            {
                var riders = obj.GetRiderByProduct(_session.dataNasabah.NamaProduk).Where(x => x.RiderCode != "SP").GroupBy(x => x.RiderCode).Select(x => x.First());
                model.Riders = new List<Domain.RiderItem>();
                var totalBiayaAsuransi = 0;

                foreach (var rider in riders)
                {
                    List<Choices> choices = new List<Choices>();
                    var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);

                    var check = prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "WOP" || rider.RiderCode == "ADB") && _session.dataNasabah.UmurPemegangPolis < (rider.CoverTermWizer / 12);
                    var uangPertanggungan = check && prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ? (CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan) * 3) > rider.MaxAmountSumInsured ? Convert.ToDecimal(rider.MaxAmountSumInsured) : (CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan) * 3) : 0;
                    var allocationPercent = check && prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ? "300%" : "0%";
                    var biayaAsuransi = check && prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ?
                        (int)MainServices.CalculateCostOfRider(new SummaryViewModel(), uangPertanggungan, rider.RiderCode,
                        null, _session.dataNasabah.UmurPemegangPolis, Int32.Parse(_session.dataNasabah.KelasPekerjaanPemegangPolis), rider.Category, prod, null) : 0;

                    var riderTypeList = obj.GetRiderProduct(prod.ProductCode, rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                    foreach (var c in rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                    {
                        var choice = new Choices();
                        choice.Checked = c.TypeName == "CI" && prod.ProductCategory.Equals("wizer") && check;
                        choice.RiderType = c;

                        uangPertanggungan = choice.Checked ? c.TypeName == "CI" && prod.ProductCategory.Equals("wizer") ? CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.TopupBerkala.Replace(",", "")) + CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala.Replace(",", "")) : CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan.Replace(",", "")) : 0;
                        var dataInput = new SummaryViewModel();
                        var riderCode = rider.RiderCode;
                        var riderType = c.RiderTypeId;
                        var age1 = _session.dataNasabah.UmurPemegangPolis;
                        var riderCategory = rider.Category;
                        biayaAsuransi += choice.Checked ? (int)MainServices.CalculateCostOfRider(dataInput, uangPertanggungan, riderCode, riderType, age1, null, riderCategory, prod, null) : 0;

                        choices.Add(choice);
                    }

                    totalBiayaAsuransi += biayaAsuransi;
                    model.Riders.Add(new Domain.RiderItem() { Rider = rider, Checked = check, Choices = choices, BiayaAsuransi = string.Format("{0:n0}", biayaAsuransi).Replace(".", ","), UangPertanggungan = uangPertanggungan > 0 ? string.Format("{0:n0}", uangPertanggungan).Replace(".", ",") : "", AllocationPercent = allocationPercent });
                }

                model.BiayaAsuransiTambahan = string.Format("{0:n0}", totalBiayaAsuransi);
            }

            var age = 0;
            var riskClass = 0;

            if (_session.dataNasabah.TertanggungUtama == "Ya")
            {
                age = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                riskClass = Convert.ToInt32(_session.dataNasabah.KelasPekerjaanPemegangPolis);
            }
            else
            {
                age = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);
                riskClass = Convert.ToInt32(_session.dataNasabah.KelasPekerjaanTertanggungUtama);
            }

            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            var listAddIns = "";

            if (_session.addInsData != null)
            {
                if(_session.addInsData.Riders != null)
                {
                    foreach(var list in _session.addInsData.Riders.Where(x => x.Checked && x.Rider.RiderCode != "SP").GroupBy(x => x.Rider.RiderCode))
                    {
                        listAddIns += list.ElementAt(0).Rider.RiderCode + ";";
                    }
                }
            }

            listAddIns =  listAddIns.TrimEnd(';');

            ViewData["ProductCategory"] = product.ProductCategory;
            ViewData["ProductCode"] = product.ProductCode;
            ViewData["ProductTitle"] = product.ProductName;
            ViewData["MaxJouvenille"] = product.ChildMaxAge;
            ViewData["up"] = _session.premiInvestmentData.UangPertanggungan;
            ViewData["age"] = age;
            ViewData["riskClass"] = riskClass;
            ViewData["hasAddInsured"] = product.HasAdditionalInsured;
            ViewData["isTertanggungUtama"] = _session.dataNasabah.TertanggungUtama;
            ViewData["caraBayar"] = _session.premiInvestmentData.CaraBayar;
            ViewData["regularTopUp"] = _session.premiInvestmentData.TopupBerkala;
            ViewData["countAddIns"] = _session.addInsData != null ? _session.addInsData.TertanggungTambahan != null ?
                _session.addInsData.TertanggungTambahan.Count() <= 0 ? 0 : _session.addInsData.TertanggungTambahan.ElementAt(0).DateOfBirth != null ? _session.addInsData.TertanggungTambahan.Count() : 0 : 0 :0;
            model.NasabahData = _session.dataNasabah;
            ViewData["listAddIns"] = listAddIns;
            return View("Rider", model);
        }

        [HttpPost]
        public ActionResult ValidateRiderData(RiderViewModel data)
        {
            _session.riderData = data;

            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            dataInput.Rider = _session.riderData;

            if (_session.addInsData != null)
            {
                var model = _session.addInsData;
                if (_session.addInsData.TertanggungTambahan != null)
                {
                    for (int i = 0; i < model.TertanggungTambahan.Count; i++)
                    {
                        for (int y = i; y < model.Riders.Count; y = y + model.TertanggungTambahan.Count)
                        {
                            var rid = model.Riders[y];
                            var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);
                            var age = Convert.ToInt32(model.TertanggungTambahan[i].Age);
                            var mainRider = dataInput.Rider.Riders.Where(x => x.Rider.RiderCode == rid.Rider.RiderCode);

                            if (rid.Rider.Category == "Unit" || rid.Rider.Category == "Choice")
                            {
                                _session.addInsData.Riders[y].Unit = mainRider.First().Unit != null ? mainRider.First().Unit : null;
                                _session.addInsData.Riders[y].UnitName = mainRider.First().UnitName != null ? mainRider.First().UnitName : null;
                                _session.addInsData.Riders[y].Choice = mainRider.First().Choice != null ? mainRider.First().Choice : null;
                                var riderType = rid.Rider.Category == "Unit" ? rid.UnitName : rid.Rider.Category == "Choice" ? rid.Choice : null;
                                _session.addInsData.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) - CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                                _session.addInsData.Riders[y].BiayaAsuransi = rid.Checked ? CalculatorServices.DecimalToCurrency(MainServices.CalculateCostOfRider(new SummaryViewModel(), CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan), rid.Rider.RiderCode, Convert.ToInt32(riderType), age, Int32.Parse(_session.dataNasabah.KelasPekerjaanPemegangPolis), rid.Rider.Category, prod, Convert.ToInt32(rid.Unit))) : "0";
                                _session.addInsData.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) + CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                            }
                        }
                    }
                }
                dataInput.Additional = _session.addInsData;
            }
            else
                dataInput.Additional = new AdditionalInsuredViewModel();

            if (dataInput.Additional.Riders != null)
            {

                foreach (var AddInsRider in dataInput.Additional.Riders.Where(x => x.Rider.RiderCode != "SP"))
                {
                    var ttuRiderChecked = dataInput.Rider.Riders.Where(x => x.Rider.RiderCode == AddInsRider.Rider.RiderCode);
                    if (!AddInsRider.Checked)
                        continue;

                    if (ttuRiderChecked.Where(x => x.Checked).Count() <= 0)
                    {
                        TempData["errorMsg"] = "Tertanggung tambahan tidak dapat mengambil " + AddInsRider.Rider.RiderName;
                        return RedirectToAction("Rider");
                    }

                    var addinsUp = CalculatorServices.CurrencyToDecimal(AddInsRider.UangPertanggungan);
                    var ttuUp = CalculatorServices.CurrencyToDecimal(ttuRiderChecked.ElementAt(0).UangPertanggungan);

                    if ((AddInsRider.Rider.Category == "Basic" || AddInsRider.Rider.Category == "Risk") && addinsUp > ttuUp)
                    {
                        TempData["errorMsg"] = "Uang pertanggungan " + AddInsRider.Rider.RiderName + " tertanggung tambahan tidak boleh lebih besar dari tertanggung utama ";
                        return RedirectToAction("Rider");
                    }
                }
            }

            var result = MainServices.CalculateFundBenefitWithRider(dataInput);

            if (result.ErrorMsg != null && result.ErrorMsg.Length > 0)
            {
                TempData["errorMsg"] = result.ErrorMsg[0];
                return RedirectToAction("Rider");
            }

            return RedirectToAction("TopUpWithdrawal");
        }

        [HttpPost]
        public ActionResult AdditionalInsured(RiderViewModel riderData)
        {
            _session.riderData = riderData;
            return RedirectToAction("AdditionalInsured");
        }

        public ActionResult AdditionalInsured()
        {
            var riderData = new RiderViewModel();
            riderData = _session.riderData;
            var model = new AdditionalInsuredViewModel();
            var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);
            var check = _session.addInsData == null ? 0 : _session.addInsData.TertanggungTambahan == null ? 0 : _session.addInsData.TertanggungTambahan.Count();

            if (check > 0)
            {
                model = _session.addInsData;
                for (int i = 0; i < model.TertanggungTambahan.Count; i++)
                {
                    for (int y = i; y < model.Riders.Count; y = y + model.TertanggungTambahan.Count)
                    {
                        var age = Convert.ToInt32(model.TertanggungTambahan[i].Age);
                        var rid = model.Riders[y];

                        if (rid.Rider.Category == "Unit")
                        {
                            var mainRider = riderData.Riders.Where(x => x.Rider.RiderCode == rid.Rider.RiderCode);
                            rid.Unit = mainRider.First().Unit != null ? mainRider.First().Unit : null;
                            rid.UnitName = mainRider.First().UnitName != null ? mainRider.First().UnitName : null;
                            model.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) - CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                            rid.BiayaAsuransi = rid.Checked ? CalculatorServices.DecimalToCurrency(MainServices.CalculateCostOfRider(new SummaryViewModel(), CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan), rid.Rider.RiderCode, Convert.ToInt32(rid.UnitName), age, Int32.Parse(_session.dataNasabah.KelasPekerjaanPemegangPolis), rid.Rider.Category, prod, Convert.ToInt32(rid.Unit))) : "0";
                            model.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) + CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                        }else if (rid.Rider.Category == "Choice")
                        {
                            var mainRider = riderData.Riders.Where(x => x.Rider.RiderCode == rid.Rider.RiderCode);
                            rid.Choice = mainRider.First().Choice != null ? mainRider.First().Choice : null;
                            model.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) - CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                            rid.BiayaAsuransi = rid.Checked ? CalculatorServices.DecimalToCurrency(MainServices.CalculateCostOfRider(new SummaryViewModel(), CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan), rid.Rider.RiderCode, Convert.ToInt32(rid.Choice), age, Int32.Parse(_session.dataNasabah.KelasPekerjaanPemegangPolis), rid.Rider.Category, prod, null)) : "0";
                            model.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTertanggungTambahan) + CalculatorServices.CurrencyToDecimal(rid.BiayaAsuransi));
                        }
                    }
                }
            }
            else
            {
                model.TertanggungTambahan = new List<AdditionalInsured>();
                var insured = new AdditionalInsured();
                model.TertanggungTambahan.Add(insured);
                model.Riders = new List<RiderItem>();
            }
            model.RiderNoData = new List<RiderItem>();
            var riders = obj.GetRiderByProduct(_session.dataNasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(prod.ProductCode+"-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());

            foreach (var rider in riders)
            {

                var riderTypeList = obj.GetRiderProduct(prod.ProductCode, rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();
                List<Choices> choices = new List<Choices>();
                var riderItem = new RiderItem() { Rider = rider, Checked = false };
                foreach (var c in rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                {

                    var choice = new Choices();
                    choice.Checked = c.TypeName == "CI" && prod.ProductCategory.Equals("wizer");
                    choice.RiderType = c;


                    var dataInput = new SummaryViewModel();
                    var riderCode = rider.RiderCode;
                    var riderType = c.RiderTypeId;
                    var age1 = _session.dataNasabah.UmurPemegangPolis;
                    var riderCategory = rider.Category;
                    choices.Add(choice);
                }

                var mainRider = riderData.Riders.Where(x => x.Rider.RiderName == rider.RiderName);

                if (mainRider != null && mainRider.Count() > 0 && riderItem.Rider.Category.Equals("Unit"))
                {
                    riderItem.Unit = mainRider.First().Unit != null ? mainRider.First().Unit : null;
                    riderItem.UnitName = mainRider.First().UnitName != null ? mainRider.First().UnitName : null;
                }
                if (mainRider != null && mainRider.Count() > 0 && riderItem.Rider.Category.Equals("Choice"))
                    riderItem.Choice = mainRider.First().Choice != null ? mainRider.First().Choice : null;

                riderItem.Choices = choices;
                model.RiderNoData.Add(riderItem);
            }

            model.NasabahData = _session.dataNasabah;
            model.DataPremi = _session.premiInvestmentData;
            model.RidersTertanggungUtama = riderData.Riders;
            //ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.Class.ToString() });
            ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            var pekerjaanP = obj.GetRiskClass(Convert.ToInt32(model.NasabahData.StatusPekerjaanPemegangPolis));
            ViewData["pekerjaanP"] = pekerjaanP == null ? "" : pekerjaanP.RiskClassDesc;
            ViewData["prod"] = prod.ProductCode;
            var pekerjaanT = obj.GetRiskClass(Convert.ToInt32(model.NasabahData.StatusPekerjaanTertanggungUtama));
            ViewData["pekerjaanT"] = model.NasabahData.TertanggungUtama == "Ya" ? "" : pekerjaanT.RiskClassDesc;
            ViewData["isTertanggungUtama"] = model.NasabahData.TertanggungUtama;
            ViewData["up"] = _session.premiInvestmentData.UangPertanggungan;
            //var rel = StaticDropdownValues.GetRelation("worksite");

            if (model.NasabahData.Relation == "Karyawan")
            {
                ViewData["relation"] = StaticDropdownValues.GetRelation("worksite");
            }
            else if (model.NasabahData.Relation == "Pengurus Yayasan")
            {
                ViewData["relation"] = StaticDropdownValues.GetRelation("yayasan");
            }
            else
            {
                ViewData["relation"] = obj.GetRelationByProduct(prod.ProductCode, "additional").Select(x => new SelectListItem { Text = x.Relation.RelationText_ID, Value = x.RelationCode });
            }
            ViewData["maxAgeTTBasedOnRider"] = model.RiderNoData.Max(x => x.Rider.InsuredMaxAge).Value;
            return View("AdditionalInsured", model);
        }

        public ActionResult CheckAdditionalInsured(AdditionalInsuredViewModel model)
        {
            if(model.Riders != null)
            {
                foreach (var rid in model.Riders.Where(x => x.Rider.Category == "Choices"))
                {
                    model.Riders.Where(x => x.Rider.RiderCode == rid.Rider.RiderCode).FirstOrDefault().Checked = true;
                    decimal biayaAsuransi = 0;
                    if(rid.Choices != null)
                    {
                        foreach (var ridType in rid.Choices)
                        {
                            biayaAsuransi += CalculatorServices.CurrencyToDecimal(ridType.BiayaAsuransi);
                        }
                    }
                    model.Riders.Where(x => x.Rider.RiderCode == rid.Rider.RiderCode).FirstOrDefault().BiayaAsuransi = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                }
            }
            
            _session.addInsData = model;
            return RedirectToAction("Rider");
        }

        public ActionResult AddAdditionalInsuredRow()
        {
            var additional = new AdditionalInsured();
            additional.Riders = new List<RiderItem>();

            var riders = obj.GetRiderByProduct(_session.dataNasabah.NamaProduk);
            foreach (var rider in riders)
            {
                additional.Riders.Add(new RiderItem() { Rider = rider, Checked = false });
            }
            var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);
            //ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.Class.ToString() });
            ViewData["prod"] = prod.ProductCode;
            ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            ViewData["relation"] = obj.GetRelationByProduct(prod.ProductCode, "additional").Select(x => new SelectListItem { Text = x.Relation.RelationText_ID, Value = x.RelationCode });
            return PartialView("AdditionalInsuredRow", additional);
        }

        public string AddAdditionalInsuredRiderRow()
        {
            var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);
            var riders = obj.GetRiderByProduct(_session.dataNasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(prod.ProductCode + "-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());
            var partialView = new Dictionary<string, string>();
            int i = 0;
            foreach (var rider in riders)
            {
                var riderTypeList = obj.GetRiderProduct(prod.ProductCode, rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();
                List<Choices> choices = new List<Choices>();

                foreach (var c in rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                {
                    var choice = new Choices();
                    choice.Checked = c.TypeName == "CI" && prod.ProductCategory.Equals("wizer");
                    choice.RiderType = c;

                    var dataInput = new SummaryViewModel();
                    var riderCode = rider.RiderCode;
                    var riderType = c.RiderTypeId;
                    var age1 = _session.dataNasabah.UmurPemegangPolis;
                    var riderCategory = rider.Category;
                    choices.Add(choice);
                }
                RiderItem riderItem = new RiderItem() { Rider = rider, Checked = false };

                var riderData = _session.riderData;
                var mainRider = riderData.Riders.Where(x => x.Rider.RiderName == rider.RiderName);

                if (mainRider != null && mainRider.Count() > 0)
                {

                    riderItem.Unit = mainRider.First().Unit != null ? mainRider.First().Unit : null;
                    riderItem.UnitName = mainRider.First().UnitName != null ? mainRider.First().UnitName : null;
                    riderItem.Choice = mainRider.First().Choice != null ? mainRider.First().Choice : null;
                }
                riderItem.Choices = choices;
                var viewString = RenderRazorViewToString(this.ControllerContext, "AdditionalInsuredRiderRow", riderItem);
                partialView.Add(i + "", viewString);
                i = i + 1;
            }
            var jsonSetting = new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.All };
            return JsonConvert.SerializeObject(partialView);
        }

        public string RenderPartialView(Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData
                    .GetRequiredString("action");

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines
                    .FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext,
                    viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static String RenderRazorViewToString(ControllerContext controllerContext, String viewName, Object model)
        {
            controllerContext.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public ActionResult BackToRider()
        {
            if (_session.topUpWithData != null)
                _session.topUpWithData = null;

            return RedirectToAction("Rider");
        }

        public ActionResult TopUpWithdrawal()
        {
            var model = new TopUpWithdrawalViewModel();

            if (_session.topUpWithData != null)
            {
                model = _session.topUpWithData;
            }
            else
            {
                model.TopupWithdrawals = new List<TopUpWithdrawal>();
                TopUpWithdrawal topup = new TopUpWithdrawal();

                topup.Amount = "";
                topup.IsTopUp = true;
                model.TopupWithdrawals.Add(topup);
            }


            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);

            if (product.ProductCode == "HLINVEST" || product.ProductCode == "HLMAXPRO") 
            {

                var caraBayar = _session.premiInvestmentData.CaraBayar == null ? 1 : Convert.ToInt32(_session.premiInvestmentData.CaraBayar);
                var PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala) / caraBayar);
                ViewData["PremiBerkala"] = PremiBerkala;
            }

            ViewData["ProductTitle"] = product.ProductName;
            ViewData["ProductCode"] = product.ProductCode;
            ViewData["TopUpStartYear"] = product.TopUpStartYear;
            ViewData["WithdrawalStartYear"] = product.WithdrawalStartYear;




            return View("TopUpWithdrawal", model);
        }

        public ActionResult AddNewTopUp()
        {
            TopUpWithdrawal topup = new Domain.TopUpWithdrawal();
            topup.IsTopUp = true;
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["TopUpStartYear"] = product.TopUpStartYear;
            ViewData["WithdrawalStartYear"] = product.WithdrawalStartYear;
            return PartialView("TopUpWithdrawalRow", topup);
        }

        public ActionResult AddNewWithrdrawal()
        {
            TopUpWithdrawal topup = new Domain.TopUpWithdrawal();
            topup.IsTopUp = false;
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["TopUpStartYear"] = product.TopUpStartYear;
            ViewData["WithdrawalStartYear"] = product.WithdrawalStartYear;
            return PartialView("TopUpWithdrawalRow", topup);
        }

        [HttpPost]
        public ActionResult ValidateTopUpWithdrawal(TopUpWithdrawalViewModel model)
        {
            _session.topUpWithData = model;

            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            dataInput.Rider = _session.riderData;
            dataInput.TopUp = _session.topUpWithData;

            if (_session.addInsData != null)
                dataInput.Additional = _session.addInsData;
            else
                dataInput.Additional = new AdditionalInsuredViewModel();

            var result = MainServices.CalculateFundBenefitTopupWithdrawal(dataInput);

            if (result.ErrorMsg != null && result.ErrorMsg.Length > 0)
            {
                TempData["errorMsg"] = result.ErrorMsg[0];
                return RedirectToAction("TopUpWithdrawal");
            }

            return RedirectToAction("InsuranceCostAndRatio");
        }

        public ActionResult BackToTopUpWithdrawal()
        {
            if (_session.insCostRatioData != null)
                _session.insCostRatioData = null;

            return RedirectToAction("TopUpWithdrawal");
        }

        public ActionResult InsuranceCostAndRatio()
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            var model = new InsuranceCostAndRatioViewModel();

            if (_session.insCostRatioData != null)
                model = _session.insCostRatioData;

            if (_session.riderData.BiayaAsuransiTambahan != null)
                model.BiayaAsuransiTambahan = _session.riderData.BiayaAsuransiTambahan;
            else
                model.BiayaAsuransiTambahan = "0";

            if (_session.addInsData != null)
                model.BiayaAsuransiTambahan = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTambahan) + CalculatorServices.CurrencyToDecimal(_session.addInsData.BiayaAsuransiTertanggungTambahan));

            if (_session.premiInvestmentData.BiayaAsuransi != null && _session.premiInvestmentData.BiayaAsuransi != "0")
                model.Premi = _session.premiInvestmentData.BiayaAsuransi;
            else
            {
                var age = 0;

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    age = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    age = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                model.Premi = CalculatorServices.DecimalToCurrency(MainServices.CalculateCostOfInsurance(dataInput,age));
                _session.premiInvestmentData.BiayaAsuransi = model.Premi;
            }

            var coi = CalculatorServices.CurrencyToDecimal(model.Premi);
            var cor = CalculatorServices.CurrencyToDecimal(model.BiayaAsuransiTambahan);
            // based on onsite 10 nov 2016 by kyok (biaya asuransi dibagi premi tanpa topup berkala)*100%
            //var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            var ratio = ((coi + cor) / totalPremi) * 100;

            ViewData["language"] = obj.GetActiveLanguage().Select(x => new SelectListItem { Text = x.Name, Value = x.CultureId });
            model.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency((coi + cor));
            model.RatioPremiBerkala = Math.Round(ratio, 2, MidpointRounding.AwayFromZero) + "%";

            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductTitle"] = product.ProductName;

            return View("InsuranceCostAndRatio", model);
        }

        [HttpPost]
        public ActionResult ValidateInsuranceCostAndRatioData(InsuranceCostAndRatioViewModel data)
        {
            _session.insCostRatioData = data;

            return RedirectToAction("Summary");
        }

        public ActionResult Summary()
        {
            var model = new SummaryViewModel();

            if (_session.dataNasabah != null)
                model.Nasabah = _session.dataNasabah;
            if (_session.premiInvestmentData != null)
                model.Premi = _session.premiInvestmentData;
            if (_session.riderData != null)
                model.Rider = _session.riderData;
            if (_session.insCostRatioData != null)
                model.Insurance = _session.insCostRatioData;

            if (model.Nasabah.TertanggungUtama.Equals("Tidak"))
            {
                var pekerjaanT = obj.GetRiskClass(Convert.ToInt32(model.Nasabah.StatusPekerjaanTertanggungUtama));
                ViewData["pekerjaanT"] = pekerjaanT.RiskClassDesc;
            }

            var pekerjaanP = obj.GetRiskClass(Convert.ToInt32(model.Nasabah.StatusPekerjaanPemegangPolis));
            ViewData["pekerjaanP"] = pekerjaanP == null ? "" : pekerjaanP.RiskClassDesc;
            var produk = obj.GetProduct(model.Nasabah.NamaProduk);
            ViewData["produk"] = produk.ProductName;
            var lang = obj.GetLanguage(model.Insurance.Language);
            ViewData["language"] = lang.Name;
            model.Premi.TopupBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(model.Premi.TopupBerkala));

            return View("Summary", model);
        }

        public ActionResult Report()
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            dataInput.Rider = _session.riderData;
            dataInput.Additional = _session.addInsData;
            dataInput.TopUp = _session.topUpWithData;
            dataInput.Insurance = _session.insCostRatioData;

            if (_session.addInsData != null)
                dataInput.Additional = _session.addInsData;
            else
                dataInput.Additional = new AdditionalInsuredViewModel();
            
            dataInput.Additional.RidersTertanggungUtama = dataInput.Rider.Riders;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(dataInput.Insurance.Language);

            var result = MainServices.GenerateReport(dataInput, _session.AgentName, _session.AgentCode, _session.AppVersion);
            if (result.ErrorMsg != null && result.ErrorMsg.Length > 0)
            {
                TempData["errorMsg"] = result.ErrorMsg[0];
                return RedirectToAction("Summary");
            }

            if (String.IsNullOrEmpty(_session.TransLogCode))
            {
                var transLog = MainServices.CreateTransLog(dataInput, _session.AgentCode);
                if (!String.IsNullOrEmpty(transLog.TransCode))
                    _session.TransLogCode = transLog.TransCode;
            }
            else
            {
                MainServices.UpdateTransLog(dataInput, _session.AgentCode, _session.TransLogCode);
            }

            var rptH = new ReportDocument { FileName = Server.MapPath(string.Format("~/Report/{0}.rpt", result.ReportName)) };
            rptH.SetDataSource(result.IllustrationDataSet);

            var guid = _session.TransLogCode + ".pdf";
            var path = Path.Combine(Server.MapPath("~/Report/temp"), guid);
            rptH.ExportToDisk(ExportFormatType.PortableDocFormat, path);

            ViewData["path"] = Path.Combine("/Report/temp", guid);
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductTitle"] = product.ProductName;

            rptH.Close();
            rptH.Dispose();

            return View("Report");
        }

        [DeleteFile]
        public ActionResult ResetAllData()
        {
            return RedirectToAction("Index");
        }

        public ActionResult SendMailReport()
        {
            var model = new SendMailViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["productName"] = product.ProductName;

            if (_session.dataNasabah.TertanggungUtama.Equals("Ya"))
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaPemegangPolis;
            else
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaTertanggungUtama;

            model.Attachment = _session.TransLogCode + ".pdf";
            model.Source = product.ProductType;

            return View("SendMailReport", model);
        }

        public ActionResult SendApiInvitation()
        {
            var model = new SendApiInvitationViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["productName"] = product.ProductName;

            if (_session.dataNasabah.TertanggungUtama.Equals("Ya"))
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaPemegangPolis;
            else
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaTertanggungUtama;

            model.Attachment = _session.TransLogCode + ".pdf";
            model.Source = product.ProductType;

            return View("SendApiInvitation", model);
        }

    }
}
