﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using System.Configuration;
using Sales.Illustration.Web.ViewModel;
using System.Data.Entity;
using System.Text;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace Sales.Illustration.Web.Controllers
{
    public class LoginApiController : ApiController
    {
        Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
        public static string salt = "sehatitumahal";
        //public static string secret = "cc9d2f0b-8434-4da1-b4cd-0822f05fbe35";

        public static string getKey(string username, string pass)
        {
            UnicodeEncoding unicode = new UnicodeEncoding();

            SHA1 SH1 = new SHA1CryptoServiceProvider();

            var gabungan = username + pass + salt;
            var byteData = unicode.GetBytes(gabungan);
            var hash = SH1.ComputeHash(byteData);
            var base64 = Convert.ToBase64String(hash);

            return base64;
        }
        
        // GET api/<controller>/5
        public HttpResponseMessage Get(string username, string pass, string key)
        { 
			var serverDeploy = ConfigurationManager.AppSettings["ServerDeploy"];
			var Key ="";
            if (serverDeploy == "UAT")//Hanya Untuk UAT
            {
                Key = "ScNqI_YWcc9iAjpHcRAJfQyCflw";  
            }
            else
            {
				Key = getKey(username, pass);
            }
            
            //"vaF1/5fit2VSZSx9OOOZya8vJLk="

            if (Key == key)
            {

                LoginDataResult dataResult;
                var onlineLogin = false;
                if (serverDeploy == "UAT")//Hanya Untuk UAT
                {
                    dataResult = obj.LoginOfflineUAT(username);
                    onlineLogin = true;
                }
                else
                {
                    try
                    {

                        dataResult = obj.LoginOnlie(username, pass);
                        onlineLogin = true;
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            dataResult = obj.LoginOffline(username, Generator.toPassword(pass));
                        }
                        catch (Exception exception)
                        {
                            Response dat = new Response();
                            dat.Message = "Wrong Agent Code or Password";
                            dat.Status = "Login Failed";
                            //dat.Data = null;
                            //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                            return Request.CreateResponse(HttpStatusCode.NotFound, dat);
                        }
                    }
                }
                if (dataResult == null || dataResult.result_code == LoginResultCode.AGENT_CODE_NOT_EXIST || dataResult.result_code == LoginResultCode.WRONG_PASSWORD)
                {
                    Response dat2 = new Response();
                    dat2.Message = "Wrong Agent Code or Password";
                    dat2.Status = "Login Failed";
                    //dat2.Data = null;
                    //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                    return Request.CreateResponse(HttpStatusCode.NotFound, dat2);
                }
                else if (dataResult.result_code == LoginResultCode.TERMINATE)
                {
                    Response dat3 = new Response();
                    dat3.Message = "Wrong Agent Code or Password";
                    dat3.Status = "Login Failed";
                    //dat3.Data = null;
                    //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                    return Request.CreateResponse(HttpStatusCode.NotFound, dat3);
                }

                if (onlineLogin)
                {
                    try
                    {
                        Sales.Illustration.Web.Models.UserMembership user = new Sales.Illustration.Web.Models.UserMembership();

                        if (dataResult.data.agent_code.ToString().Length > 10)
                            user.AgentCode = dataResult.data.agent_code.ToString().Replace(".", "").Substring(0, 10);
                        else
                            user.AgentCode = dataResult.data.agent_code;

                        // for add or edit local authentication
                        user.AAJILicense = dataResult.data.license_aaji;
                        user.AgentName = dataResult.data.agent_name;
                        user.EncPass = Generator.toPassword(pass);
                        user.JoinDate = dataResult.data.join_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.join_date);
                        user.ExpiredDate = dataResult.data.license_expiry_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.license_expiry_date);
                        user.Email = dataResult.data.email;
                        user.RoleUser = dataResult.data.roleuser == null ? "user" : dataResult.data.roleuser;
                        user.IsActive = true;

                        obj.UserEditApi(user);
                    }
                    catch (Exception ex)
                    {
                        Response res2 = new Response();
                        res2.Message = "Wrong Agent Code or Password";
                        res2.Status = "Login Failed";
                        //res2.Data = null;

                        return Request.CreateResponse(HttpStatusCode.NotFound, res2);
                    }
                }

                try
                {
                    obj.CreateLog(dataResult.data.agent_code, dataResult.data.agent_name);
                }
                catch (Exception ex) { }

                Response res = new Response();
                res.Message = "Success";
                res.Status = "OK";
                //res.Data = null;
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                Response res = new Response();
                res.Message = "Invalid Key";
                res.Status = "Failed";
                //res.Data = null;
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, res);
            }
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] Param param)
        {
            var serverDeploy = ConfigurationManager.AppSettings["ServerDeploy"];
            var Key = "";
            if (serverDeploy == "UAT")//Hanya Untuk UAT
            {
                Key = "ScNqI_YWcc9iAjpHcRAJfQyCflw";
            }
            else
            {
                Key = getKey(param.username, param.pass);
                //"vaF1/5fit2VSZSx9OOOZya8vJLk="
            }
            if (Key == param.key)
            {

                LoginDataResult dataResult;
                var onlineLogin = false;
                
                if (serverDeploy == "UAT")//Hanya Untuk UAT
                {
                    dataResult = obj.LoginOfflineUAT(param.username);
                    onlineLogin = true;
                }
                else
                {
                    try
                    {

                        dataResult = obj.LoginOnlie(param.username, param.pass);
                        onlineLogin = true;
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            dataResult = obj.LoginOffline(param.username, Generator.toPassword(param.pass));
                        }
                        catch (Exception exception)
                        {
                            Response dat = new Response();
                            dat.Message = "Wrong Agent Code or Password";
                            dat.Status = "Login Failed";
                            //dat.Data = null;
                            //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                            return Request.CreateResponse(HttpStatusCode.NotFound, dat);
                        }
                    }
                }
                if (dataResult == null || dataResult.result_code == LoginResultCode.AGENT_CODE_NOT_EXIST || dataResult.result_code == LoginResultCode.WRONG_PASSWORD)
                {
                    Response dat2 = new Response();
                    dat2.Message = "Wrong Agent Code or Password";
                    dat2.Status = "Login Failed";
                    //dat2.Data = null;
                    //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                    return Request.CreateResponse(HttpStatusCode.NotFound, dat2);
                }
                else if (dataResult.result_code == LoginResultCode.TERMINATE)
                {
                    Response dat3 = new Response();
                    dat3.Message = "Wrong Agent Code or Password";
                    dat3.Status = "Login Failed";
                    //dat3.Data = null;
                    //return new string[] { "Message : Login Failed", "MessageDetail : Wrong Agent Code or Password" };
                    return Request.CreateResponse(HttpStatusCode.NotFound, dat3);
                }

                if (onlineLogin)
                {
                    try
                    {
                        Sales.Illustration.Web.Models.UserMembership user = new Sales.Illustration.Web.Models.UserMembership();

                        if (dataResult.data.agent_code.ToString().Length > 10)
                            user.AgentCode = dataResult.data.agent_code.ToString().Replace(".", "").Substring(0, 10);
                        else
                            user.AgentCode = dataResult.data.agent_code;

                        // for add or edit local authentication
                        user.AAJILicense = dataResult.data.license_aaji;
                        user.AgentName = dataResult.data.agent_name;
                        user.EncPass = Generator.toPassword(param.pass);
                        user.JoinDate = dataResult.data.join_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.join_date);
                        user.ExpiredDate = dataResult.data.license_expiry_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.license_expiry_date);
                        user.Email = dataResult.data.email;
                        user.RoleUser = dataResult.data.roleuser == null ? "user" : dataResult.data.roleuser;
                        user.IsActive = true;

                        obj.UserEditApi(user);
                    }
                    catch (Exception ex)
                    {
                        Response res2 = new Response();
                        res2.Message = "Wrong Agent Code or Password";
                        res2.Status = "Login Failed";
                        //res2.Data = null;

                        return Request.CreateResponse(HttpStatusCode.NotFound, res2);
                    }
                }

                try
                {
                    obj.CreateLog(dataResult.data.agent_code, dataResult.data.agent_name);
                }
                catch (Exception ex) { }

                var token = GenerateToken(param.username, DateTime.UtcNow.AddSeconds(30));

                ResponseToken res = new ResponseToken();
                res.Message = "Success";
                res.Status = "OK";
                res.Token = token;
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                Response res = new Response();
                res.Message = "Invalid Key";
                res.Status = "Failed";
                //res.Data = null;
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, res);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public static class Global
        {
            public const string secret = "cc9d2f0b-8434-4da1-b4cd-0822f05fbe35";
        }
        private class Response
        {
            public string Message { get; set; }
            public string Status { get; set; }
            //public List<RiskClass> Data { get; set; }
        }

        private class ResponseData
        {
            public string Message { get; set; }
            public string Status { get; set; }
            public List<RiskClass> Data { get; set; }
        }

        private class ResponseToken
        {
            public string Message { get; set; }
            public string Status { get; set; }
            public string Token { get; set; }
            //public List<RiskClass> Data { get; set; }
        }
        public class Param
        {
            public string username { get; set; }
            public string pass { get; set; }
            public string key { get; set; }
        }

        public static string GenerateToken(string username, DateTime? expiry = null)
        {
            if (expiry == null)
            {
                expiry = DateTime.UtcNow.AddHours(1);
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Global.secret);
            var tokenDescriptor = new SecurityTokenDescriptor { Subject = new ClaimsIdentity(new[] { new Claim("username", username) }), Expires = expiry, SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature) };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string ValidateToken(string token)
        {
            if (token == null)
                return null;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Global.secret);
            try
            {
                SecurityToken validatedToken;
                tokenHandler.ValidateToken(token, new TokenValidationParameters { ValidateIssuerSigningKey = true, IssuerSigningKey = new SymmetricSecurityKey(key), ValidateIssuer = false, ValidateAudience = false, ClockSkew = TimeSpan.Zero }, out validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                var username = jwtToken.Claims.First(x => x.Type == "username").Value;
                return username;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}