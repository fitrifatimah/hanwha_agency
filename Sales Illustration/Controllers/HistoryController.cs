﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class HistoryController : Controller
    {
        MainModel obj = new MainModel();

        public ActionResult Index()
        {
            ViewData["agentCode"] = _session.AgentCode;
            ViewBag.TypeAgent = _session.AgentType;

            return View();
        }

        public ActionResult LoadHistoryData(string id)
        {
            var transLog = obj.GetTransLog(id);
            var nasabahData = transLog.NasabahDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode);
            var premiData = transLog.PremiumDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode);
            var fundData = transLog.InvestmentDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var riderData = transLog.RiderDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var addInsData = transLog.AdditionalInsuredDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var topupData = transLog.TopUpWithdrawalDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var riderF = transLog.RiderDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode);
            

            var product = obj.GetProduct(nasabahData.ProductCode);
            var nasabah = _session.dataNasabah = new ViewModel.NasabahDataViewModel();
            var premi = _session.premiInvestmentData = new ViewModel.PremiumAndInvestmentViewModel(); ;
            var rider = _session.riderData = new ViewModel.RiderViewModel(); ;
            var topup = _session.topUpWithData = new ViewModel.TopUpWithdrawalViewModel();
            var ratio = _session.insCostRatioData = new ViewModel.InsuranceCostAndRatioViewModel();
            var dataInput = new ViewModel.SummaryViewModel();
            bool error = false; var ageTTg = 0; var genderTTg = ""; 
            var errMsg = "Terjadi Kesalahan. Silahkan buat ilustrasi baru!";
            var errMsgUsia = "Usia melebihi batas ketentuan yang ada. Silahkan buat Ilustrasi baru!";
            var errMsgUsiaAnak = "Usia Anak Kandung melebihi Usia Pemegang Polis. Silahkan buat Ilustrasi baru!";
            var errRiderF = "Usia melebihi batas ketentuan yang ada. Silakan buat ilustrasi baru!";

            _session.TransLogCode = transLog.TransCode;
            ViewBag.TypeAgent = _session.AgentType;

            #region Nasabah
            var risk = 0;
            DateTime tertanggungBirth;
            nasabah.NamaPemegangPolis = nasabahData.PolicyHolderName;

            
            if(product.ProductCode != "HLWORK" && nasabahData.Relation != "Karyawan" && nasabahData.Relation != "Pengurus Yayasan")
            {
                nasabah.TanggalLahirPemegangPolis = Convert.ToDateTime(nasabahData.PolicyHolderDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (nasabahData.PolicyHolderGender.Equals("F"))
                    nasabah.JenisKelaminPemegangPolis = "Wanita";
                else
                    nasabah.JenisKelaminPemegangPolis = "Pria";

                nasabah.StatusPekerjaanPemegangPolis = nasabahData.PolicyHolderRiskClassId.ToString();
                var riskPolicyHolder = obj.GetRiskClass((int)nasabahData.PolicyHolderRiskClassId);
                nasabah.KelasPekerjaanPemegangPolis = riskPolicyHolder.Class.ToString();
                // cek age HPHC
                if (product.ProductCode == "HPCP") {
                    nasabah.UmurPemegangPolis = CalculatorServices.CalculateAgeLatest(Convert.ToDateTime(nasabahData.PolicyHolderDOB, CultureInfo.InvariantCulture));
                }
                else
                {
                    nasabah.UmurPemegangPolis = CalculatorServices.CalculateAge(Convert.ToDateTime(nasabahData.PolicyHolderDOB, CultureInfo.InvariantCulture));
                }
            }

            if (nasabahData.IsMainInsured == true)
            {
                nasabah.TertanggungUtama = "Ya";
                ageTTg = nasabah.UmurPemegangPolis;genderTTg = nasabah.JenisKelaminPemegangPolis;
                tertanggungBirth = DateTime.ParseExact(nasabah.TanggalLahirPemegangPolis, "dd/MM/yyyy", null);
                risk = Int32.Parse(nasabah.KelasPekerjaanPemegangPolis);
                //ditambah 20191213
                if (riderF != null)
                {
                    if ((riderF.RiderCode == "ADB" || riderF.RiderCode == "CI") && (ageTTg > 69) && (product.ProductCode == "HLMAXPRO"))
                    {
                        error = true;
                        TempData["errorMsg"] = errRiderF;
                        return RedirectToAction("Index", "History");
                    }
                }
                //end tambah
                if (nasabah.UmurPemegangPolis < (product.PolicyHolderMinAge / 12) || nasabah.UmurPemegangPolis > (product.InsuredMaxAge / 12))
                {
                    error = true;
                    TempData["errorMsg"] = errMsgUsia;
                    return RedirectToAction("Index", "History");
                }
            }
            else
            {
                nasabah.TertanggungUtama = "Tidak";
                nasabah.NamaTertanggungUtama = nasabahData.InsuredName;
                nasabah.TanggalLahirTertanggungUtama = Convert.ToDateTime(nasabahData.InsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (nasabahData.InsuredGender.Equals("F"))
                    nasabah.JenisKelaminTertanggungUtama = "Wanita";
                else
                    nasabah.JenisKelaminTertanggungUtama = "Pria";

                nasabah.StatusPekerjaanTertanggungUtama = nasabahData.InsuredRiskClassId.ToString();
                var riskMainInsured = obj.GetRiskClass((int)nasabahData.InsuredRiskClassId);
                nasabah.KelasPekerjaanTertanggungUtama = riskMainInsured.Class.ToString();
                nasabah.UmurTertanggungUtama = CalculatorServices.CalculateAge(Convert.ToDateTime(nasabahData.InsuredDOB, CultureInfo.InvariantCulture));
                nasabah.Relation = nasabahData.Relation;
                risk = Int32.Parse(nasabah.KelasPekerjaanTertanggungUtama);
                tertanggungBirth = DateTime.ParseExact(nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", null);
                ageTTg = (int)nasabah.UmurTertanggungUtama;genderTTg = nasabah.JenisKelaminTertanggungUtama;
                //ditambah 20191213
                if (riderF != null)
                {
                    if ((riderF.RiderCode == "ADB" || riderF.RiderCode == "CI") && (ageTTg > 69) && (product.ProductCode == "HLMAXPRO"))
                    {
                        error = true;
                        TempData["errorMsg"] = errRiderF;
                        return RedirectToAction("Index", "History");
                    }
                }
                //end tambah
                if (nasabah.Relation != "Karyawan" && nasabah.Relation != "Pengurus Yayasan")
                {
                    if (nasabah.UmurPemegangPolis < (product.PolicyHolderMinAge / 12) || nasabah.UmurPemegangPolis > (product.PolicyHolderMaxAge / 12))
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsia;
                        return RedirectToAction("Index", "History");
                    }
                }
                if (nasabah.UmurTertanggungUtama > (product.InsuredMaxAge / 12))
                {
                    error = true;
                    TempData["errorMsg"] = errMsgUsia;
                    return RedirectToAction("Index", "History");
                }

                if (nasabah.Relation == "Anak Kandung" && nasabah.UmurTertanggungUtama >= nasabah.UmurPemegangPolis)
                {
                    error = true;
                    TempData["errorMsg"] = errMsgUsiaAnak;
                    return RedirectToAction("Index", "History");
                }
            }

            if (product.ProductType.Equals("tra"))
            {
                #region Anak
                if (!String.IsNullOrEmpty(nasabahData.ChildName))
                {
                    nasabah.NamaAnak = nasabahData.ChildName;
                    nasabah.TanggalLahirAnak = Convert.ToDateTime(nasabahData.ChildDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (nasabahData.ChildGender.Equals("F"))
                        nasabah.JenisKelaminAnak = "Wanita";
                    else
                        nasabah.JenisKelaminAnak = "Pria";

                    nasabah.UmurAnak = CalculatorServices.CalculateAge(Convert.ToDateTime(nasabahData.ChildDOB, CultureInfo.InvariantCulture));

                    if (nasabah.UmurAnak > (product.ChildMaxAge / 12))
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsia;
                        return RedirectToAction("Index", "History");
                    }
                }
                #endregion
            }

            nasabah.NamaProduk = nasabahData.ProductCode;
            ratio.Language = nasabahData.Language;
            dataInput.Nasabah = nasabah;
            #endregion

            #region Premi
            premi.MataUang = premiData.Currency == "IDR" ? "Rupiah" :  premiData.Currency;
            premi.TotalInvestment = 100;

            if (premiData.PaymentMethod != null)
                premi.CaraBayar = premiData.PaymentMethod.ToString();

            premi.PremiAngsuran = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium + (premiData.RegularTopUp != null ? (decimal)premiData.RegularTopUp : 0));
            premi.PremiBerkala = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);

            if (premiData.SumInsured != null)
                premi.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)premiData.SumInsured);

            if (product.ProductType.Equals("tra"))
            {
                if (premiData.PaymentPeriodOption > 0)
                    premi.PilihanMasaPembayaran = premiData.PaymentPeriodOption.ToString();

                if (!String.IsNullOrEmpty(premiData.PaymentMode))
                    premi.ModeBayarPremi = premiData.PaymentMode;

                if (premiData.InsurancePeriod > 0)
                    premi.MasaAsuransi = CalculatorServices.GetInsPeriod(nasabah.UmurAnak,product.ProductCode);

                if (premiData.PaymentPeriod > 0)
                    premi.RencanaMasaPembayaran = (int)premiData.PaymentPeriod;

                var insAge = 0;
                if (nasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(nasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(nasabah.UmurTertanggungUtama);

                if (product.CovAge != null && (insAge+premi.RencanaMasaPembayaran) > (product.CovAge/12))
                {
                    error = true;
                    TempData["errorMsg"] = errMsg;
                    return RedirectToAction("Index", "History");
                }
                //Add FAzmi
                if (!product.ProductCode.Equals("HLSAVING") && !product.ProductCode.Equals("HLHEALTH"))
                {
                    premi.PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.RoundUp((decimal)MainServices.CalculatePremiTraditional(premi.UangPertanggungan.ToString(), premi.ModeBayarPremi, insAge, premi.RencanaMasaPembayaran, product, premi.RencanaMasaPembayaran), -1));
                }
                
                string type = product.ProductCode == "HLHEALTH" ? premiData.ProductPlan : premi.ModeBayarPremi;
                if (product.ProductCode.Equals("HLHEALTH"))
                {
                    premi.PremiBerkala = CalculatorServices.DecimalToCurrency(CalculatorServices.RoundUp((decimal)MainServices.CalculatePremiTraditional(premi.UangPertanggungan, type, insAge, premi.RencanaMasaPembayaran, product, premi.RencanaMasaPembayaran), -1));
                    premi.JenisPlan = premiData.ProductPlan;
                }
                premi.UsiaMasukAnak = nasabah.UmurAnak;
                dataInput.Premi = premi;
            }
            else if (product.ProductType.Equals("ul"))
            {

                if (premiData.PaymentPeriod != null)
                {
                    premi.RencanaMasaPembayaran = (int)premiData.PaymentPeriod;
                    premi.RencanaMasaPembayaran = (ageTTg+premi.RencanaMasaPembayaran) > (product.CovAge/12) ? (product.CovAge.Value / 12) - ageTTg : premi.RencanaMasaPembayaran;
                }

                if (premiData.RegularTopUp != null)
                    premi.TopupBerkala = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularTopUp);

                premi.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)premiData.SumInsured);
                if (product.ProductCategory == "preferred" || product.ProductCategory == "invest")
                {
                    if (MainServices.ValidateUP(product, ageTTg, genderTTg, (decimal)premiData.SumInsured, premiData.RegularPremium))
                    {
                        error = true;
                        TempData["errorMsg"] = "Uang Pertanggungan melebihi batas ketentuan yang ada. Silahkan buat Ilustrasi baru!";
                        return RedirectToAction("Index", "History");
                    }
                }
                dataInput.Premi = premi;
                premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency(MainServices.CalculateCostOfInsurance(dataInput,ageTTg));

                if (premiData.UnitWizer != null)
                    premi.UnitWizer = premiData.UnitWizer;
            }
            else
            {
                dataInput.Premi = premi;
                premi.PremiInpatient = CalculatorServices.DecimalToCurrency((decimal)premiData.PremiInpatient);
                premi.PremiOutpatient = CalculatorServices.DecimalToCurrency((decimal)premiData.PremiOutpatient);
                premi.JenisPlan = Convert.ToString(premiData.PlanValue);
                premi.ManfaatRawatJalan = premiData.ProductPlan;
                premi.MasaAsuransi = 1; //default khusus HCPC

            }
            #endregion

            #region Rider
            var riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => x.RiderCode != "SP").GroupBy(x => x.RiderCode).Select(x => x.First());
            rider.Riders = new List<RiderItem>();
            decimal totalBiayaAsuransi = 0;

            foreach (var riderItem in riders)
            {
                List<Choices> choices = new List<Choices>();

                if (riderItem.Category.Equals("Choices"))
                {
                    var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                    foreach (var c in riderItem.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                    {
                        var choice = new Choices();
                        choice.RiderType = c;
                        choice.Checked = false;
                        choices.Add(choice);
                    }
                }

                rider.Riders.Add(new RiderItem()
                {
                    Rider = riderItem,
                    Choices = choices
                });
            }

            rider.ddlRiderCoverage = riderData != null && riderData.Count(x => x.RiderCode == "CI") != 0 && riderData.FirstOrDefault(x => x.RiderCode == "CI").ddlRiderCoverage != "" && riderData.FirstOrDefault(x => x.RiderCode == "CI").ddlRiderCoverage != null ? riderData.FirstOrDefault(x => x.RiderCode == "CI").ddlRiderCoverage : "1";

            foreach (var riderItem in rider.Riders)
            {
                var riderDataItem = riderData.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);
                
                if (riderDataItem != null)
                {
                    int age = 0;

                    if (riderItem.Rider.RiderCode.Equals("POP"))
                        age = Convert.ToInt32(nasabah.UmurPemegangPolis);
                    else
                    {
                        if (nasabah.TertanggungUtama.Equals("Ya"))
                            age = Convert.ToInt32(nasabah.UmurPemegangPolis);
                        else
                            age = Convert.ToInt32(nasabah.UmurTertanggungUtama);
                    }

                    if (!MainServices.IsCoveredByRider(riderItem.Rider, Convert.ToInt32(nasabah.UmurPemegangPolis), nasabah.Relation, "PH"))
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsia;
                        return RedirectToAction("Index", "History");
                        break;
                    }

                    var usia = 0;
                    if (nasabah.TertanggungUtama.Equals("Ya"))
                        usia = Convert.ToInt32(nasabah.UmurPemegangPolis);
                    else
                        usia = Convert.ToInt32(nasabah.UmurTertanggungUtama);


                    if (!MainServices.IsCoveredByRider(riderItem.Rider, usia, nasabah.Relation, "TU"))
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsia;
                        return RedirectToAction("Index", "History");
                        break;
                    }

                    riderItem.Checked = true;

                    if (!riderItem.Rider.Category.Equals("Choices"))
                    {
                        decimal COR = 0;
                        if (product.ProductType.Equals("tra") && riderItem.Rider.RiderCode == "CI")
                        {
                            var riderCov = premi.MasaAsuransi;
                            if (premi.ModeBayarPremi == "berkala")
                            {
                                var ddlRiderCov = riderDataItem.ddlRiderCoverage;
                                if (ddlRiderCov == "1")
                                    riderCov = premi.RencanaMasaPembayaran;
                                else
                                {
                                    if ((65 - age) < riderCov)
                                        riderCov = 65 - age;
                                }
                            }
                            else
                            {
                                if ((65 - age) < riderCov)
                                    riderCov = 65 - age;
                            }
                            COR = MainServices.CalculateTraditionalCOR(riderDataItem.SumInsured.ToString(), riderItem.Rider.RiderCode, null, premi.ModeBayarPremi == "sekaligus" ? premi.ModeBayarPremi : premi.ModeBayarPremi + riderDataItem.ddlRiderCoverage, riderCov, riderItem.Rider.Category);
                            COR = premi.ModeBayarPremi == "sekaligus" ? COR : COR * (decimal)obj.GetPaymentMethod(Convert.ToInt32(_session.premiInvestmentData.CaraBayar), _session.dataNasabah.NamaProduk).Factor / 100;
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Unit"))
                                COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, riderDataItem.RiderTypeId, age, risk, riderItem.Rider.Category, riderDataItem.Unit);
                            else if (riderItem.Rider.Category.Equals("Choice"))
                                COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, riderDataItem.RiderTypeId, age, risk, riderItem.Rider.Category, null);
                            else
                                COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, null, age, risk, riderItem.Rider.Category, null);
                        }

                        riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(COR);
                        totalBiayaAsuransi += (decimal)COR;
                    }

                    if (riderDataItem.SumInsured != null)
                        riderItem.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.SumInsured);

                    if (riderItem.Rider.RiderCode == "HWPCI" && riderDataItem.COR != null)
                    {
                        decimal COR = 0;
                        var mpp = premi.RencanaMasaPembayaran;
                        var premiBerkala = decimal.Parse(premi.PremiBerkala);
                        decimal carabayar = decimal.Parse(premi.CaraBayar);
                        var factor = (decimal)obj.GetPaymentMethod(Convert.ToInt32(_session.premiInvestmentData.CaraBayar), _session.dataNasabah.NamaProduk).Factor / 100;
                        var Rate = obj.GetRiderRate(riderItem.Rider.RiderCode, age, riderItem.Rider.Category, premi.RencanaMasaPembayaran).Rate;
                        COR = Math.Round((((decimal)Rate / 1000) * (mpp * premiBerkala * carabayar) * factor), 1);
                        riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(CalculatorServices.RoundUp(COR, -1));
                        //riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.COR);
                    }

                    if (riderDataItem.AllocationPerc != null)
                        riderItem.AllocationPercent = riderDataItem.AllocationPerc + "%";

                    if (riderDataItem.RiderTypeId != null)
                    {
                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            decimal choicesCOR = 0;
                            foreach (var choice in riderItem.Choices)
                            {
                                var choicheData = riderData.Where(x => x.RiderCode == riderDataItem.RiderCode).ToList();

                                foreach (var cho in choicheData)
                                {
                                    if (choice.RiderType.RiderTypeId == cho.RiderTypeId)
                                    {
                                        decimal COR = 0;
                                        if (product.ProductType.Equals("tra"))
                                            COR = MainServices.CalculateTraditionalCOR(riderDataItem.SumInsured.ToString(), riderItem.Rider.RiderCode, choice.RiderType.RiderTypeId, premi.ModeBayarPremi, dataInput.Premi.RencanaMasaPembayaran, riderItem.Rider.Category);
                                        else
                                            COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, choice.RiderType.RiderTypeId, age, null, riderItem.Rider.Category, null);

                                        choice.RiderType.RiderTypeId = (int)cho.RiderTypeId;
                                        choice.Checked = true;
                                        choicesCOR += (decimal)COR;
                                        totalBiayaAsuransi += (decimal)COR;
                                    }
                                }
                            }
                            riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(choicesCOR);
                        }
                        else if (riderItem.Rider.Category.Equals("Unit"))
                        {
                            riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                            foreach (var unit in riderItem.Rider.RiderTypes)
                            {
                                if (unit.RiderTypeId == riderDataItem.RiderTypeId)
                                {
                                    riderItem.UnitName = riderDataItem.RiderTypeId.ToString();
                                    riderItem.Unit = riderDataItem.Unit.ToString();
                                }
                            }
                        }
                        else if (riderItem.Rider.Category.Equals("Choice"))
                        {
                            riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                            foreach (var choice in riderItem.Rider.RiderTypes)
                            {
                                if (choice.RiderTypeId == riderDataItem.RiderTypeId)
                                {
                                    riderItem.Choice = riderDataItem.RiderTypeId.ToString();
                                }
                            }
                        }
                    }
                }
            }

            rider.BiayaAsuransiTambahan = product.ProductCode == "HSCI" && riderF != null ? CalculatorServices.DecimalToCurrency((decimal)riderF.COR) : CalculatorServices.DecimalToCurrency(totalBiayaAsuransi); 
            #endregion

            #region Additional Insured
            decimal totalBiayaAsuransiTambahan = 0;
            if (product.ProductType.Equals("tra"))
            {
                var addIns = _session.addInsTraditionalData = new ViewModel.AdditionalInsuredTraditionalViewModel();
                riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(product.ProductCode + "-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());
                addIns.TertanggungTambahan = new List<AdditionalInsured>();

                foreach (var ins in addInsData)
                {
                    addIns.TertanggungTambahan.Add(new AdditionalInsured()
                    {
                        Age = CalculatorServices.CalculateAge(Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture)),
                        Nama = ins.AdditionalInsuredName,
                        DateOfBirth = Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Gender = ins.AdditionalInsuredGender.Contains("F") ? "Wanita" : "Pria",
                        KelasPekerjaan = ins.AdditionalInsuredRiskClassId.ToString(),
                        Relationship = ins.AdditionalInsuredRelation,
                        RiskClass = obj.GetRiskClass(Convert.ToInt32(ins.AdditionalInsuredRiskClassId)).Class.ToString()
                    });
                }
            }
            else
            {
                var addIns = _session.addInsData = new ViewModel.AdditionalInsuredViewModel();
                dataInput.Additional = addIns;
                riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(product.ProductCode + "-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());
                addIns.TertanggungTambahan = new List<AdditionalInsured>();
                addIns.Riders = new List<RiderItem>();
                addIns.RiderNoData = new List<RiderItem>();

                foreach (var ins in addInsData)
                {
                    ins.AdditionalInsuredAge = CalculatorServices.CalculateAge(Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture));
                    if (ins.AdditionalInsuredAge > (product.InsuredMaxAge / 12))
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsia;
                        return RedirectToAction("Index", "History");
                    }

                    if (ins.AdditionalInsuredRelation == "Anak Kandung" && ins.AdditionalInsuredAge >= nasabah.UmurPemegangPolis)
                    {
                        error = true;
                        TempData["errorMsg"] = errMsgUsiaAnak;
                        return RedirectToAction("Index", "History");
                    }

                    addIns.TertanggungTambahan.Add(new AdditionalInsured()
                    {
                        //Age = CalculatorServices.CalculateAge(Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture)),
                        Age = ins.AdditionalInsuredAge,
                        Nama = ins.AdditionalInsuredName,
                        DateOfBirth = Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Gender = ins.AdditionalInsuredGender.Contains("F") ? "Wanita" : "Pria",
                        KelasPekerjaan = ins.AdditionalInsuredRiskClassId.ToString(),
                        Relationship = ins.AdditionalInsuredRelation,
                        RiskClass = obj.GetRiskClass(Convert.ToInt32(ins.AdditionalInsuredRiskClassId)).Class.ToString()
                    });
                }

                foreach (var riderItem in riders)
                {
                    List<Choices> choices = new List<Choices>();

                    if (riderItem.Category.Equals("Choices"))
                    {
                        var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                        foreach (var c in riderItem.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                        {
                            var choice = new Choices();
                            choice.RiderType = c;
                            choice.Checked = false;
                            choices.Add(choice);
                        }
                    }

                    addIns.RiderNoData.Add(new RiderItem()
                    {
                        Rider = riderItem,
                        Choices = choices
                    });
                }

                for (int i = 0; i < addIns.RiderNoData.Count; i++)
                {
                    for (int y = 0; y < addInsData.Count; y++)
                    {
                        var riderItem = addIns.RiderNoData[i];

                        List<Choices> choices = new List<Choices>();

                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.Rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                            foreach (var c in riderItem.Rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                            {
                                var choice = new Choices();
                                choice.RiderType = c;
                                choice.Checked = false;
                                choices.Add(choice);
                            }
                        }

                        addIns.Riders.Add(new RiderItem()
                        {
                            Rider = riderItem.Rider,
                            Choices = choices
                        });
                    }
                }

                for (int i = 0; i < addInsData.Count; i++)
                {
                    int age = CalculatorServices.CalculateAge(Convert.ToDateTime(addInsData[i].AdditionalInsuredDOB, CultureInfo.InvariantCulture));
                    int? riskTT = obj.GetRiskClass(Convert.ToInt32(addInsData[i].AdditionalInsuredRiskClassId)).Class;
                    for (int y = i; y < addIns.Riders.Count; y = y + addInsData.Count)
                    {
                        var addInsRiderData = addInsData[i].AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();
                        var riderItem = addIns.Riders[y];
                        var riderDataItems = addInsRiderData.Where(x => x.RiderCode == riderItem.Rider.RiderCode && x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();

                        if (riderDataItems != null)
                        {
                            decimal biayaAsuransi = 0;
                            foreach (var riderDataItem in riderDataItems)
                            {
                                if (!MainServices.IsCoveredByRider(riderItem.Rider, age, addInsData[i].AdditionalInsuredRelation, "TT"))
                                {
                                    error = true;
                                    TempData["errorMsg"] = errMsgUsia;
                                    return RedirectToAction("Index", "History");
                                    break;
                                }

                                decimal COR = 0;
                                if (riderItem.Rider.Category.Equals("Unit"))
                                    COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, riderDataItem.RiderTypeId, age, riskTT, riderItem.Rider.Category, riderDataItem.Unit);
                                else if (riderItem.Rider.Category.Equals("Choice"))
                                    COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, riderDataItem.RiderTypeId, age, riskTT, riderItem.Rider.Category, null);
                                else
                                    COR = MainServices.CalculateUnitLinkCOR((decimal)riderDataItem.SumInsured, riderItem.Rider.RiderCode, null, age, riskTT, riderItem.Rider.Category, null);

                                riderItem.Checked = true;
                                biayaAsuransi += (decimal)COR;
                                riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                                totalBiayaAsuransiTambahan += (decimal)COR;

                                if (riderDataItem.SumInsured != null)
                                    riderItem.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.SumInsured);

                                if (riderDataItem.AllocationPerc != null)
                                    riderItem.AllocationPercent = riderDataItem.AllocationPerc + "%";

                                if (riderDataItem.RiderTypeId != null)
                                {
                                    if (riderItem.Rider.Category.Equals("Choices"))
                                    {
                                        foreach (var choice in riderItem.Choices)
                                        {
                                            if (choice.RiderType.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                choice.RiderType.RiderTypeId = (int)riderDataItem.RiderTypeId;
                                                choice.Checked = true;
                                            }
                                        }
                                    }
                                    if (riderItem.Rider.Category.Equals("Choices"))
                                    {
                                        foreach (var choice in riderItem.Choices)
                                        {
                                            var choicheData = addInsRiderData.Where(x => x.RiderTypeId == riderDataItem.RiderTypeId && x.RiderCode == riderItem.Rider.RiderCode && x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();

                                            foreach (var cho in choicheData)
                                            {
                                                if (choice.RiderType.RiderTypeId == cho.RiderTypeId)
                                                {
                                                    var cor = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan), riderItem.Rider.RiderCode, choice.RiderType.RiderTypeId, age, null, riderItem.Rider.Category, null);
                                                    choice.RiderType.RiderTypeId = (int)cho.RiderTypeId;
                                                    choice.Checked = true;
                                                    choice.BiayaAsuransi = CalculatorServices.DecimalToCurrency(cor);
                                                    totalBiayaAsuransiTambahan += cor;
                                                }
                                            }
                                        }
                                    }
                                    else if (riderItem.Rider.Category.Equals("Unit"))
                                    {
                                        riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                                        foreach (var unit in riderItem.Rider.RiderTypes)
                                        {
                                            if (unit.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                riderItem.UnitName = riderDataItem.RiderTypeId.ToString();
                                                riderItem.Unit = riderDataItem.Unit.ToString();
                                            }
                                        }
                                    }
                                    else if (riderItem.Rider.Category.Equals("Choice"))
                                    {
                                        riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                                        foreach (var choice in riderItem.Rider.RiderTypes)
                                        {
                                            if (choice.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                riderItem.Choice = riderDataItem.RiderTypeId.ToString();
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                addIns.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(totalBiayaAsuransiTambahan);
            }
            #endregion

            #region Fund
            premi.Investments = obj.GetFundByProduct(nasabah.NamaProduk).Select(x => new Domain.Investment() { InvestmentName = x.FundName, InvestmentCode = x.FundCode }).ToList();

            foreach (var fundItem in premi.Investments)
            {
                var fund = fundData.FirstOrDefault(x => x.FundCode == fundItem.InvestmentCode);

                if (fund != null)
                {
                    fundItem.InvestmentCode = fund.FundCode;
                    fundItem.Percentage = (int)fund.FundValue;
                }
            }
            #endregion

            #region TopUpWithdrawal
            topup.TopupWithdrawals = topupData.Select(x => new Domain.TopUpWithdrawal() { IsTopUp = (bool)x.IsTopUp, Year = (int)x.TransYear, Amount = CalculatorServices.DecimalToCurrency((decimal)x.TransAmount) }).ToList();
            #endregion

            if(error)
            {
                return RedirectToAction("Index", "History");
            }

            if(CalculatorServices.CalculateNearestBirthday(tertanggungBirth).Success)
                TempData["errorMsg"] =  "Perhatian, polis ini sudah mendekati sebulan dari tanggal nearest birthday Tertanggung";

            
            if (product.ProductType.Equals("tra"))
            {
                if (product.ProductCode != "HSCI")
                {
                    premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);
                    ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(premi.PremiBerkala) + (decimal)totalBiayaAsuransi);

                }
                else
                {
                    
                    ratio.Premi = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);
                    ratio.BiayaAsuransiTambahan = "0,00";
                    if (riderF != null)
                    {
                        ratio.BiayaAsuransiTambahan = CalculatorServices.DecimalToCurrency((decimal)riderF.COR);
                    }
                    premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)premiData.CostOfInsurance);
                    ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(ratio.BiayaAsuransiTambahan));

                }

                return RedirectToAction("Summary", "Traditional");
            }
            if (product.ProductType.Equals("ul"))
            {
                ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(premi.BiayaAsuransi) + (decimal)totalBiayaAsuransi + totalBiayaAsuransiTambahan);
                ratio.RatioPremiBerkala = Math.Round(CalculatorServices.CurrencyToDecimal(ratio.BiayaAsuransiTotal) / CalculatorServices.CurrencyToDecimal(premi.PremiBerkala) * 100, 2, MidpointRounding.AwayFromZero) + "%";

                return RedirectToAction("Summary", "UnitLink");
            }
            else
            {
                premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);
                ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(premi.PremiBerkala) + (decimal)totalBiayaAsuransi);

                return RedirectToAction("Summary", "Kesehatan");
            }
        }
    }
}
