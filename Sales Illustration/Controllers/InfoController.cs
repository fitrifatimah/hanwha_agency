﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class InfoController : Controller
    {
        //
        // GET: /Info/

        public ActionResult Index()
        {
            ViewBag.TypeAgent = _session.AgentType;
            return View();
        }

    }
}
