﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

        public ActionResult Index()
        {
            Session.Clear();
            _session.AppMode = ConfigurationManager.AppSettings["ApplicationMode"];
            _session.AppVersion = ConfigurationManager.AppSettings["ApplicationVersion"];
            ViewData["AppVersion"] = ConfigurationManager.AppSettings["ApplicationVersion"];

            return View();
        }

        public ActionResult AddUser()
        {
            Sales.Illustration.Web.Models.UserMembership user = new Sales.Illustration.Web.Models.UserMembership();
            user.AgentCode = "gebi.anant";
            user.AgentName = "GEBI ANANTIASARI";
            user.EncPass = Generator.toPassword("januari2019");
            user.AAJILicense = "";
            user.JoinDate = DateTime.Now;
            user.IsActive = true;

            try
            {
                obj.UserAdd(user);
                TempData["msg"] = "success";
            }
            catch (Exception ex)
            {
                TempData["msg"] = "failed";
            }

            return RedirectToAction("Index");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult CheckLogin(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["msg"] = "Username or password cannot be empty!";
                return RedirectToAction("Index");
            }

            LoginDataResult dataResult;
            var onlineLogin = false;

            try
            {

                dataResult = obj.LoginOnlie(model.Username, model.Password);
                onlineLogin = true;
            }
            catch (Exception ex)
            {
                try
                {
                    dataResult = obj.LoginOffline(model.Username, Generator.toPassword(model.Password));
                }
                catch (Exception exception)
                {
                    TempData["msg"] = "Please check again your username or password!";
                    return RedirectToAction("Index");
                }
            }

            if (dataResult == null || dataResult.result_code == LoginResultCode.AGENT_CODE_NOT_EXIST || dataResult.result_code == LoginResultCode.WRONG_PASSWORD)
            {
                TempData["msg"] = "Wrong Agent Code or Password";
                return RedirectToAction("Index");
            } 
            else if (dataResult.result_code == LoginResultCode.TERMINATE)
            { 
                TempData["msg"] = "Agent is no more active";
                return RedirectToAction("Index");
            }

            if (onlineLogin)
            {
                try
                {
                    Sales.Illustration.Web.Models.UserMembership user = new Sales.Illustration.Web.Models.UserMembership();

                    if (dataResult.data.agent_code.ToString().Length > 10)
                        user.AgentCode = dataResult.data.agent_code.ToString().Replace(".", "").Substring(0, 10);
                    else
                        user.AgentCode = dataResult.data.agent_code;

                    // for add or edit local authentication
                    //user.AgentCode = dataResult.data.agent_code.ToString().Substring(0,10);
                    user.AAJILicense = dataResult.data.license_aaji;
                    user.AgentName = dataResult.data.agent_name;
                    user.EncPass = Generator.toPassword(model.Password);
                    user.JoinDate = dataResult.data.join_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.join_date);
                    user.ExpiredDate = dataResult.data.license_expiry_date == null ? (DateTime?)null : Convert.ToDateTime(dataResult.data.license_expiry_date);
                    user.Email = dataResult.data.email;
                    user.RoleUser = dataResult.data.roleuser == null ? "user" : dataResult.data.roleuser;
                    user.IsActive = true;

                    //AddUser();
                    obj.UserEdit(user);

                }
                catch (Exception ex)
                {

                    TempData["msg"] = ex.Message;
                    return RedirectToAction("Index");
                }
            }
            
           //_session.AgentCode = dataResult.data.agent_code;
            if (dataResult.data.agent_code.ToString().Length > 10)
                _session.AgentCode = dataResult.data.agent_code.ToString().Replace(".", "").Substring(0, 10);
            else
                _session.AgentCode = dataResult.data.agent_code;

            _session.AgentName = dataResult.data.agent_name;
            _session.AAJILicense = dataResult.data.license_aaji;
            _session.JoinDate = dataResult.data.join_date;
            _session.Email = dataResult.data.email;
            _session.RoleUser = dataResult.data.roleuser;
            _session.AgentType = (_session.AgentCode).Substring(0, 1) == "7" ? "Digital Agent" : "Standard Agent";
            
            try
            {
                obj.CreateLog(dataResult.data.agent_code, dataResult.data.agent_name);
            }
            catch (Exception ex) { }

            return RedirectToAction("Index", "Profile");
        }
    }
}
