﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.ViewModel;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        public ActionResult Index()
        {
            var model = new ProfileViewModel
            {
                AgentCode = _session.AgentCode,
                Name = _session.AgentName,
                LicenseNo = _session.AAJILicense,
                JoinDate = _session.JoinDate                
        };
            ViewBag.TypeAgent = _session.AgentType;

            return View(model);
        }
        

    }
}
