﻿using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    public class ClearDataController : Controller
    {
        //
        // GET: /ClearData/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClearUnitLinkNasabahData()
        {
            _session.dataNasabah = null;
            return RedirectToAction("NasabahData", "UnitLink");
        }

        public ActionResult ClearKesehatanNasabahData()
        {
            _session.dataNasabah = null;
            return RedirectToAction("NasabahData", "Kesehatan");
        }

        public ActionResult ClearUnitLinkPremiumAndInvestmentData()
        {
            _session.premiInvestmentData = null;
            return RedirectToAction("PremiumAndInvestment", "UnitLink");
        }

        public ActionResult ClearKesehatanPremiumAndInvestmentData()
        {
            _session.premiInvestmentData = null;
            return RedirectToAction("PremiumAndInvestment", "Kesehatan");
        }

        public ActionResult ClearUnitLinkRiderData()
        {
            _session.riderData = null;
            _session.addInsData = null;

            return RedirectToAction("Rider", "UnitLink");
        }

        public ActionResult ClearUnitLinkAdditionalRiderData()
        {
            _session.addInsData = null;

            return RedirectToAction("AdditionalInsured", "UnitLink");
        }

        public ActionResult ClearUnitLinkTopUpWithdrawalData()
        {
            _session.topUpWithData = null;
            return RedirectToAction("TopUpWithdrawal", "UnitLink");
        }

        public ActionResult ClearUnitLinkInsuranceCostAndRatioData()
        {
            _session.insCostRatioData = null;
            return RedirectToAction("InsuranceCostAndRatio", "UnitLink");
        }

        public ActionResult ClearKesehatanInsuranceCostAndRatioData()
        {
            _session.insCostRatioData = null;
            return RedirectToAction("InsuranceCostAndRatio", "Kesehatan");
        }

        public ActionResult ClearTraditionalNasabahData()
        {
            _session.dataNasabah = null;
            return RedirectToAction("NasabahData", "Traditional");
        }

        public ActionResult ClearTraditionalPremiumAndInvestmentData()
        {
            _session.premiInvestmentData = null;
            return RedirectToAction("PremiumAndInvestment", "Traditional");
        }

        public ActionResult ClearTraditionalRiderData()
        {
            _session.riderData = null;
            _session.addInsData = null;

            return RedirectToAction("Rider", "Traditional");
        }

        public ActionResult ClearTraditionalTopUpWithdrawalData()
        {
            _session.topUpWithData = null;
            return RedirectToAction("TopUpWithdrawal", "Traditional");
        }

        public ActionResult ClearTraditionalInsuranceCostAndRatioData()
        {
            _session.insCostRatioData = null;
            return RedirectToAction("InsuranceCostAndRatio", "Traditional");
        }

    }
}
