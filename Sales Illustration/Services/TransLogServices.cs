﻿using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public static class TransLogServices
    {
        public static string CreateTransLog(SummaryViewModel dataInput, string agentCode, out string transNo)
        {
            transNo = null;
            int age = 0; int riskClass;

            MainModel obj = new MainModel();
            TransLog log = new TransLog();
            NasabahData nasabahData = new NasabahData();
            PremiumData premiData = new PremiumData();
            InvestmentData fundData = new InvestmentData();
            RiderData riderData = new RiderData();
            AdditionalInsuredData addInsData = new AdditionalInsuredData();
            AdditionalInsuredRiderData addInsRiderData = new AdditionalInsuredRiderData();
            TopUpWithdrawalData topupData = new TopUpWithdrawalData();

            try
            {
                var dateNow = DateTime.Now;
                var transCode = agentCode + dataInput.Nasabah.NamaProduk + dateNow.ToString("yyMMddHHmmssfff");
                string gender = "M";
                var product = obj.GetProduct(dataInput.Nasabah.NamaProduk);

                #region Header
                log.TransCode = transCode;
                log.TransStatus = 1;
                log.TransDate = dateNow;
                log.AgentCode = agentCode;

                obj.AddTransLog(log);
                #endregion

                #region NasabahData
                nasabahData.PolicyHolderName = dataInput.Nasabah.NamaPemegangPolis;
                    if(product.ProductCode != "HLWORK" && dataInput.Nasabah.Relation != "Karyawan" && dataInput.Nasabah.Relation != "Pengurus Yayasan")
                    {
                    nasabahData.PolicyHolderDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirPemegangPolis, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (dataInput.Nasabah.JenisKelaminPemegangPolis == "Wanita")
                        gender = "F";

                    nasabahData.PolicyHolderGender = gender;
                    nasabahData.PolicyHolderRiskClassId = Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis);
                    nasabahData.PolicyHolderAge = dataInput.Nasabah.UmurPemegangPolis;
                    nasabahData.IsMainInsured = true;

                    age = dataInput.Nasabah.UmurPemegangPolis;
                    riskClass = Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis);
                }

                if (dataInput.Nasabah.TertanggungUtama == "Tidak")
                {
                    age = (int)dataInput.Nasabah.UmurTertanggungUtama;
                    riskClass = Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama);

                    nasabahData.InsuredName = dataInput.Nasabah.NamaTertanggungUtama;
                    nasabahData.InsuredDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    gender = dataInput.Nasabah.JenisKelaminTertanggungUtama == "Wanita" ? "F" : "M";

                    nasabahData.InsuredGender = gender;
                    nasabahData.InsuredRiskClassId = Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama);
                    nasabahData.InsuredAge = dataInput.Nasabah.UmurTertanggungUtama;
                    nasabahData.Relation = dataInput.Nasabah.Relation;
                    nasabahData.IsMainInsured = false;
                }

                if (!String.IsNullOrEmpty(dataInput.Nasabah.NamaAnak))
                {
                    nasabahData.ChildName = dataInput.Nasabah.NamaAnak;
                    nasabahData.ChildDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirAnak, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    gender = dataInput.Nasabah.JenisKelaminAnak == "Wanita" ? "F" : "M";

                    nasabahData.ChildGender = gender;
                    nasabahData.ChildAge = dataInput.Nasabah.UmurAnak;
                }

                nasabahData.ProductCode = dataInput.Nasabah.NamaProduk;
                nasabahData.Language = dataInput.Insurance.Language;
                nasabahData.TransCode = transCode;

                try
                {
                    obj.AddNasabahData(nasabahData);
                }
                catch (Exception)
                {

                }
                #endregion

                #region PremiumData
                premiData.Currency = dataInput.Premi.MataUang == "Rupiah" ? "IDR" : dataInput.Premi.MataUang;

                if (!String.IsNullOrEmpty(dataInput.Insurance.RatioPremiBerkala))
                    premiData.RatioRegularPremium = Double.Parse(dataInput.Insurance.RatioPremiBerkala.Replace("%", ""));

                if (!String.IsNullOrEmpty(dataInput.Premi.CaraBayar))
                    premiData.PaymentMethod = Int32.Parse(dataInput.Premi.CaraBayar);

                if (dataInput.Premi.RencanaMasaPembayaran > 0)
                    premiData.PaymentPeriod = dataInput.Premi.RencanaMasaPembayaran;

                if (!String.IsNullOrEmpty(dataInput.Premi.PremiBerkala))
                premiData.RegularPremium = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);

                if (!String.IsNullOrEmpty(dataInput.Premi.TopupBerkala))
                    premiData.RegularTopUp = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);

                if (!String.IsNullOrEmpty(dataInput.Premi.UangPertanggungan))
                    premiData.SumInsured = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);


                if (dataInput.Nasabah.NamaProduk == "HPCP")
                {
                    if (!String.IsNullOrEmpty(dataInput.Premi.ManfaatRawatJalan))
                        premiData.ProductPlan = dataInput.Premi.ManfaatRawatJalan;

                    if (!String.IsNullOrEmpty(dataInput.Premi.JenisPlan))
                        premiData.PlanValue = Convert.ToInt32(dataInput.Premi.JenisPlan);

                    //if (!String.IsNullOrEmpty(dataInput.Premi.PremiInpatient))
                        premiData.PremiInpatient = String.IsNullOrEmpty(dataInput.Premi.PremiInpatient) == null? 0 : CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiInpatient);

                    //if (!String.IsNullOrEmpty(dataInput.Premi.PremiOutpatient))
                        premiData.PremiOutpatient = !String.IsNullOrEmpty(dataInput.Premi.PremiOutpatient) == null? 0 : CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiOutpatient);
                }
                else
                {
                    if (!String.IsNullOrEmpty(dataInput.Premi.JenisPlan))
                        premiData.ProductPlan = dataInput.Premi.JenisPlan;

                }

                premiData.CostOfInsurance = CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi);

                if (!String.IsNullOrEmpty(dataInput.Premi.PilihanMasaPembayaran))
                    premiData.PaymentPeriodOption = Int32.Parse(dataInput.Premi.PilihanMasaPembayaran);

                if (!String.IsNullOrEmpty(dataInput.Premi.ModeBayarPremi))
                    premiData.PaymentMode = dataInput.Premi.ModeBayarPremi;

                if (dataInput.Premi.MasaAsuransi > 0)
                    premiData.InsurancePeriod = dataInput.Premi.MasaAsuransi;

                if (dataInput.Premi.UnitWizer > 0)
                    premiData.UnitWizer = dataInput.Premi.UnitWizer;
                if (!String.IsNullOrEmpty(dataInput.Insurance.BiayaAsuransiTotal) && product.ProductCode == "HLLIFE")
                    premiData.RegularPremium = CalculatorServices.CurrencyToDecimal(dataInput.Insurance.BiayaAsuransiTotal);

                premiData.TransCode = transCode;

                try
                {
                    obj.AddPremiumData(premiData);
                }
                catch (Exception)
                {

                }
                #endregion

                #region FundData
                if (dataInput.Premi.Investments != null)
                {
                    foreach (var fund in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                    {
                        fundData.FundCode = fund.InvestmentCode;
                        fundData.FundValue = fund.Percentage;
                        fundData.TransCode = transCode;

                        try
                        {
                            obj.AddInvestmentData(fundData);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                #endregion

                #region RiderData
                if(dataInput.Rider != null)
                {
                foreach (var rider in dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    riderData.AllocationPerc = null;
                    riderData.Unit = null;
                    riderData.RiderTypeId = null;

                    riderData.RiderCode = rider.Rider.RiderCode;
                    riderData.SumInsured = CalculatorServices.CurrencyToDecimal(rider.UangPertanggungan);
                    riderData.TransCode = transCode;
                    riderData.ddlRiderCoverage = rider.Rider.RiderCode == "CI" ? dataInput.Rider.ddlRiderCoverage : "";

                    if (!String.IsNullOrEmpty(rider.AllocationPercent))
                        riderData.AllocationPerc = Double.Parse(rider.AllocationPercent.Replace("%", ""));

                    riderData.COR = CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi);

                    if (rider.Rider.Category.Equals("Unit"))
                    {
                        if (!String.IsNullOrEmpty(rider.UnitName))
                        {
                            riderData.RiderTypeId = Int32.Parse(rider.UnitName);
                            riderData.Unit = Int32.Parse(rider.Unit);

                            obj.AddRiderData(riderData);
                        }
                    }
                    else if (rider.Rider.Category.Equals("Choices"))
                    {
                        if (rider.Choices != null)
                        {
                            foreach (var riderTypeItem in rider.Choices.Where(x => x.Checked))
                            {
                                decimal biayaAsuransi;
                                var ageChoices = rider.Rider.RiderCode == "POP" ? dataInput.Nasabah.UmurPemegangPolis : age ;
                                riderData.RiderTypeId = riderTypeItem.RiderType.RiderTypeId;

                                if (product.ProductType.Equals("tra"))
                                {
                                    biayaAsuransi = MainServices.CalculateTraditionalCOR(rider.UangPertanggungan, rider.Rider.RiderCode, riderData.RiderTypeId, premiData.PaymentMode, premiData.PaymentPeriod, rider.Rider.Category);
                                    // biayaAsuransi = CalculatorServices.Round(biayaAsuransi * Convert.ToDecimal((MainServices.GetFactorPaymentMethod(Convert.ToInt32(dataInput.Premi.CaraBayar)) / 100)), -1);
                                }
                                else
                                {
                                    biayaAsuransi = MainServices.CalculateUnitLinkCOR((decimal)riderData.SumInsured, rider.Rider.RiderCode, riderData.RiderTypeId, ageChoices, null, rider.Rider.Category, null);
                                }

                                riderData.COR = biayaAsuransi;

                                obj.AddRiderData(riderData);
                            }
                        }
                        else
                        {

                            obj.AddRiderData(riderData);
                        }
                        }
                    else if (rider.Rider.Category.Equals("Choice"))
                    {
                        if (!String.IsNullOrEmpty(rider.Choice))
                        {
                            riderData.RiderTypeId = Int32.Parse(rider.Choice);

                            obj.AddRiderData(riderData);
                        }
                    }
                    else
                    {
                        obj.AddRiderData(riderData);
                    }
                    }
                }
                #endregion

                #region AdditionalInsuredData
                if (dataInput.Additional != null)
                {
                    if (dataInput.Additional.TertanggungTambahan != null)
                    {
                        for (int y = 0; y < dataInput.Additional.TertanggungTambahan.Count; y++)
                        {
                            var addIns = dataInput.Additional.TertanggungTambahan[y];
                            gender = "M";
                            addInsData = new AdditionalInsuredData();

                            addInsData.TransCode = transCode;
                            addInsData.AdditionalInsuredName = addIns.Nama;
                            addInsData.AdditionalInsuredDOB = DateTime.ParseExact(addIns.DateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            if (addIns.Gender == "Wanita")
                                gender = "F";

                            addInsData.AdditionalInsuredGender = gender;
                            addInsData.AdditionalInsuredRiskClassId = Convert.ToInt32(addIns.KelasPekerjaan);
                            addInsData.AdditionalInsuredAge = addIns.Age;
                            addInsData.AdditionalInsuredRelation = addIns.Relationship;

                            obj.AddAdditionalInsuredData(addInsData);

                            for (int i = y; i < dataInput.Additional.Riders.Count; )
                            {
                                var addInsDataId = obj.GetAdditionalInsuredData(y,transCode, addInsData.AdditionalInsuredName, addInsData.AdditionalInsuredDOB, addInsData.AdditionalInsuredGender, addInsData.AdditionalInsuredRiskClassId, addInsData.AdditionalInsuredAge, addInsData.AdditionalInsuredRelation).AdditionalInsuredDataId;
                                var rider = dataInput.Additional.Riders[i];

                                if (rider.Checked || CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi) > 0 || rider.Rider.Category.Equals("Choices"))
                                {
                                    addInsRiderData.AdditionalInsuredDataId = addInsDataId;
                                    addInsRiderData.AllocationPerc = null;
                                    addInsRiderData.Unit = null;
                                    addInsRiderData.RiderTypeId = null;

                                    addInsRiderData.RiderCode = rider.Rider.RiderCode;
                                    addInsRiderData.SumInsured = CalculatorServices.CurrencyToDecimal(rider.UangPertanggungan);

                                    if (!String.IsNullOrEmpty(rider.AllocationPercent))
                                        addInsRiderData.AllocationPerc = Double.Parse(rider.AllocationPercent.Replace("%", ""));

                                    addInsRiderData.COR = CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi);

                                    if (rider.Rider.Category.Equals("Unit"))
                                    {
                                        if (!String.IsNullOrEmpty(rider.UnitName))
                                        {
                                            addInsRiderData.RiderTypeId = Int32.Parse(rider.UnitName);
                                            addInsRiderData.Unit = Int32.Parse(rider.Unit);

                                            obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                        }
                                    }
                                    else if (rider.Rider.Category.Equals("Choices"))
                                    {
                                        if (rider.Choices != null)
                                        {
                                            foreach (var riderTypeItem in rider.Choices.Where(x => x.Checked))
                                            {
                                                decimal biayaAsuransi;
                                                addInsRiderData.RiderTypeId = riderTypeItem.RiderType.RiderTypeId;
                                                age = (int)addIns.Age;

                                                if (product.ProductType.Equals("tra"))
                                                {
                                                    biayaAsuransi = MainServices.CalculateTraditionalCOR(rider.UangPertanggungan, rider.Rider.RiderCode, addInsRiderData.RiderTypeId, premiData.PaymentMode, premiData.PaymentPeriod, rider.Rider.Category);
                                                }
                                                else
                                                {
                                                    biayaAsuransi = MainServices.CalculateUnitLinkCOR((decimal)addInsRiderData.SumInsured, rider.Rider.RiderCode, addInsRiderData.RiderTypeId, age, null, rider.Rider.Category, null);
                                                }

                                                addInsRiderData.COR = biayaAsuransi;

                                                obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                            }
                                        }
                                    }
                                    else if (rider.Rider.Category.Equals("Choice"))
                                    {
                                        if (!String.IsNullOrEmpty(rider.Choice))
                                        {
                                            addInsRiderData.RiderTypeId = Int32.Parse(rider.Choice);

                                            obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                        }
                                    }
                                    else
                                    {
                                        obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                    }
                                }

                                if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                    i = i + dataInput.Additional.TertanggungTambahan.Count;
                                else
                                    i++;
                            }
                        }
                    }
                }
                #endregion

                #region AdditionalInsuredDataTraditional
                if (dataInput.Premi.additionalInsured != null && dataInput.Premi.additionalInsured.TertanggungTambahan != null)
                {
                    for (int y = 0; y < dataInput.Premi.additionalInsured.TertanggungTambahan.Count; y++)
                    {
                        var addIns = dataInput.Premi.additionalInsured.TertanggungTambahan[y];
                        gender = "M";
                        addInsData = new AdditionalInsuredData();

                        addInsData.TransCode = transCode;
                        addInsData.AdditionalInsuredName = addIns.Nama;
                        addInsData.AdditionalInsuredDOB = DateTime.ParseExact(addIns.DateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (addIns.Gender == "Wanita")
                            gender = "F";

                        addInsData.AdditionalInsuredGender = gender;
                        addInsData.AdditionalInsuredRiskClassId = Convert.ToInt32(addIns.KelasPekerjaan);
                        addInsData.AdditionalInsuredAge = addIns.Age;
                        addInsData.AdditionalInsuredRelation = addIns.Relationship;

                        obj.AddAdditionalInsuredData(addInsData);
                    }
                }
                #endregion
                #region TopUpWithdrawalData
                if (dataInput.TopUp != null)
                {
                    foreach (var topup in dataInput.TopUp.TopupWithdrawals.Where(x => x.Amount != null))
                    {
                        topupData.IsTopUp = topup.IsTopUp;
                        topupData.TransYear = topup.Year;
                        topupData.TransAmount = CalculatorServices.CurrencyToDecimal(topup.Amount);
                        topupData.TransCode = transCode;

                        try
                        {
                            obj.AddTopUpWithdrawalData(topupData);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                #endregion

                transNo = transCode;
            }
            catch (Exception)
            {

            }

            return transNo;
        }

        public static void UpdateTransLog(SummaryViewModel dataInput, string agentCode, string transNo)
        {
            MainModel obj = new MainModel();
            TransLog log = new TransLog();
            NasabahData nasabahData = new NasabahData();
            PremiumData premiData = new PremiumData();
            InvestmentData fundData = new InvestmentData();
            RiderData riderData = new RiderData();
            AdditionalInsuredData addInsData = new AdditionalInsuredData();
            AdditionalInsuredRiderData addInsRiderData = new AdditionalInsuredRiderData();
            TopUpWithdrawalData topupData = new TopUpWithdrawalData();

            try
            {
                var transLog = obj.GetTransLog(transNo);
                var dateNow = DateTime.Now;
                var transCode = transNo;
                string gender = "M";
                int age = 0; int riskClass;
                var product = obj.GetProduct(dataInput.Nasabah.NamaProduk);

                #region Header
                log.TransCode = transCode;
                log.TransStatus = 1;
                log.TransDate = dateNow;
                log.AgentCode = agentCode;

                obj.EditTransLog(log);
                #endregion

                #region NasabahData
                nasabahData.NasabahDataId = transLog.NasabahDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode).NasabahDataId;
                nasabahData.PolicyHolderName = dataInput.Nasabah.NamaPemegangPolis;

                if (product.ProductCode != "HLWORK" && dataInput.Nasabah.Relation != "Karyawan" && dataInput.Nasabah.Relation != "Pengurus Yayasan")
                {
                    nasabahData.PolicyHolderDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirPemegangPolis, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (dataInput.Nasabah.JenisKelaminPemegangPolis == "Wanita")
                        gender = "F";

                    nasabahData.PolicyHolderGender = gender;
                    nasabahData.PolicyHolderRiskClassId = Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis);
                    nasabahData.PolicyHolderAge = dataInput.Nasabah.UmurPemegangPolis;
                    nasabahData.IsMainInsured = true;

                    age = dataInput.Nasabah.UmurPemegangPolis;
                    riskClass = Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis);
                }

                if (dataInput.Nasabah.TertanggungUtama == "Tidak")
                {
                    age = (int)dataInput.Nasabah.UmurTertanggungUtama;
                    riskClass = Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama);

                    nasabahData.InsuredName = dataInput.Nasabah.NamaTertanggungUtama;
                    nasabahData.InsuredDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    gender = dataInput.Nasabah.JenisKelaminTertanggungUtama == "Wanita" ? "F" : "M";

                    nasabahData.InsuredGender = gender;
                    nasabahData.InsuredRiskClassId = Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama);
                    nasabahData.InsuredAge = dataInput.Nasabah.UmurTertanggungUtama;
                    nasabahData.Relation = dataInput.Nasabah.Relation;
                    nasabahData.IsMainInsured = false;
                }

                if (!String.IsNullOrEmpty(dataInput.Nasabah.NamaAnak))
                {
                    nasabahData.ChildName = dataInput.Nasabah.NamaAnak;
                    nasabahData.ChildDOB = DateTime.ParseExact(dataInput.Nasabah.TanggalLahirAnak, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    gender = dataInput.Nasabah.JenisKelaminAnak == "Wanita" ? "F" : "M";

                    nasabahData.ChildGender = gender;
                    nasabahData.ChildAge = dataInput.Nasabah.UmurAnak;
                }

                nasabahData.ProductCode = dataInput.Nasabah.NamaProduk;
                nasabahData.Language = dataInput.Insurance.Language;
                nasabahData.TransCode = transCode;

                try
                {
                    obj.EditNasabahData(nasabahData);
                }
                catch (Exception)
                {

                }
                #endregion

                #region PremiumData
                premiData.Currency = dataInput.Premi.MataUang == "Rupiah" ? "IDR" : dataInput.Premi.MataUang;

                if (!String.IsNullOrEmpty(dataInput.Insurance.RatioPremiBerkala))
                    premiData.RatioRegularPremium = Double.Parse(dataInput.Insurance.RatioPremiBerkala.Replace("%", ""));

                if (!String.IsNullOrEmpty(dataInput.Premi.CaraBayar))
                    premiData.PaymentMethod = Int32.Parse(dataInput.Premi.CaraBayar);

                if (dataInput.Premi.RencanaMasaPembayaran > 0)
                    premiData.PaymentPeriod = dataInput.Premi.RencanaMasaPembayaran;

                premiData.RegularPremium = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);

                if (!String.IsNullOrEmpty(dataInput.Premi.TopupBerkala))
                    premiData.RegularTopUp = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);

                if (!String.IsNullOrEmpty(dataInput.Premi.UangPertanggungan))
                    premiData.SumInsured = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

                if (dataInput.Nasabah.NamaProduk == "HPCP")
                {
                    if (!String.IsNullOrEmpty(dataInput.Premi.ManfaatRawatJalan))
                        premiData.ProductPlan = dataInput.Premi.ManfaatRawatJalan;

                    if (!String.IsNullOrEmpty(dataInput.Premi.JenisPlan))
                        premiData.PlanValue = Convert.ToInt32(dataInput.Premi.JenisPlan);

                    if (!String.IsNullOrEmpty(dataInput.Premi.PremiInpatient))
                        premiData.PremiInpatient = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiInpatient);

                    if (!String.IsNullOrEmpty(dataInput.Premi.PremiOutpatient))
                        premiData.PremiOutpatient = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiOutpatient);
                }
                else
                {
                    if (!String.IsNullOrEmpty(dataInput.Premi.JenisPlan))
                        premiData.ProductPlan = dataInput.Premi.JenisPlan;
                }

                premiData.CostOfInsurance = CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi);

                if (!String.IsNullOrEmpty(dataInput.Premi.PilihanMasaPembayaran))
                    premiData.PaymentPeriodOption = Int32.Parse(dataInput.Premi.PilihanMasaPembayaran);

                if (!String.IsNullOrEmpty(dataInput.Premi.ModeBayarPremi))
                    premiData.PaymentMode = dataInput.Premi.ModeBayarPremi;

                if (dataInput.Premi.MasaAsuransi > 0)
                    premiData.InsurancePeriod = dataInput.Premi.MasaAsuransi;

                if (dataInput.Premi.UnitWizer > 0)
                    premiData.UnitWizer = dataInput.Premi.UnitWizer;
                if (!String.IsNullOrEmpty(dataInput.Insurance.BiayaAsuransiTotal) && product.ProductCode == "HLLIFE")
                    premiData.RegularPremium = CalculatorServices.CurrencyToDecimal(dataInput.Insurance.BiayaAsuransiTotal);

                premiData.TransCode = transCode;
                premiData.PremiumDataId = transLog.PremiumDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode).PremiumDataId;

                try
                {
                    obj.EditPremiumData(premiData);
                }
                catch (Exception)
                {

                }
                #endregion

                #region FundData
                if (dataInput.Premi.Investments != null)
                {
                    var delFund = true;

                    foreach (var fund in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                    {
                        fundData.FundCode = fund.InvestmentCode;
                        fundData.FundValue = fund.Percentage;
                        fundData.TransCode = transCode;

                        try
                        {
                            if (delFund)
                            {
                                obj.DeleteInvestmentData(fundData);
                                delFund = false;
                            }
                                
                            obj.AddInvestmentData(fundData);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                #endregion

                #region RiderData
                var delRider = true;
                try
                {

                    obj.DeleteRiderData(transCode);
                }catch (Exception ex) { }

                if(dataInput.Rider != null)
                {
                foreach (var rider in dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    riderData.AllocationPerc = null;
                    riderData.Unit = null;
                    riderData.RiderTypeId = null;

                    riderData.RiderCode = rider.Rider.RiderCode;
                    riderData.SumInsured = CalculatorServices.CurrencyToDecimal(rider.UangPertanggungan);
                    riderData.TransCode = transCode;
                    riderData.ddlRiderCoverage = rider.Rider.RiderCode == "CI" ? dataInput.Rider.ddlRiderCoverage : "";

                    if (!String.IsNullOrEmpty(rider.AllocationPercent))
                        riderData.AllocationPerc = Double.Parse(rider.AllocationPercent.Replace("%", ""));

                    riderData.COR = CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi);

                        //if (delRider)
                        //{
                        //    obj.DeleteRiderData(riderData);
                        //    delRider = false;
                        //}

                        if (rider.Rider.Category.Equals("Unit"))
                        {
                            if (!String.IsNullOrEmpty(rider.UnitName))
                            {
                                riderData.RiderTypeId = Int32.Parse(rider.UnitName);
                                riderData.Unit = Int32.Parse(rider.Unit);

                                obj.AddRiderData(riderData);
                            }
                        }
                        else if (rider.Rider.Category.Equals("Choices"))
                        {
                            if (rider.Choices != null)
                            {
                                var riderType = rider.Choices.Where(x => x.Checked).Count();
                                if (riderType != 0)
                                {
                                    foreach (var riderTypeItem in rider.Choices.Where(x => x.Checked))
                                    {
                                        decimal biayaAsuransi;
                                        var ageChoices = rider.Rider.RiderCode == "POP" ? dataInput.Nasabah.UmurPemegangPolis : age;
                                        riderData.RiderTypeId = riderTypeItem.RiderType.RiderTypeId;

                                        if (product.ProductType.Equals("tra"))
                                        {
                                            biayaAsuransi = MainServices.CalculateTraditionalCOR(rider.UangPertanggungan, rider.Rider.RiderCode, riderData.RiderTypeId, premiData.PaymentMode, premiData.PaymentPeriod, rider.Rider.Category);
                                            //biayaAsuransi = CalculatorServices.Round(biayaAsuransi * Convert.ToDecimal((MainServices.GetFactorPaymentMethod(Convert.ToInt32(dataInput.Premi.CaraBayar)) / 100)), -1);
                                        }
                                        else
                                        {
                                            biayaAsuransi = MainServices.CalculateUnitLinkCOR((decimal)riderData.SumInsured, rider.Rider.RiderCode, riderData.RiderTypeId, ageChoices, null, rider.Rider.Category, null);
                                        }

                                        riderData.COR = biayaAsuransi;

                                        obj.AddRiderData(riderData);
                                    }
                                }
                                else
                                {
                                    obj.AddRiderData(riderData);
                                }
                            }
                            else
                            {
                                obj.AddRiderData(riderData);
                            }
                        }
                      
                        else if (rider.Rider.Category.Equals("Choice"))
                        {
                            if (!String.IsNullOrEmpty(rider.Choice))
                            {
                                riderData.RiderTypeId = Int32.Parse(rider.Choice);

                                obj.AddRiderData(riderData);
                            }
                        }
                        else
                        {
                            obj.AddRiderData(riderData);
                            }
                        }
                }
                #endregion

                #region AdditionalInsuredData
                try
                {

                    obj.DeleteAdditionalInsuredData(transCode);
                }
                catch (Exception ex) { }
                if (dataInput.Additional != null)
                {
                    if (dataInput.Additional.TertanggungTambahan != null)
                    {

                        for (int y = 0; y < dataInput.Additional.TertanggungTambahan.Count; y++)
                        {
                            var addIns = dataInput.Additional.TertanggungTambahan[y];
                            gender = "M";
                            addInsData = new AdditionalInsuredData();

                            addInsData.TransCode = transCode;
                            addInsData.AdditionalInsuredName = addIns.Nama;
                            addInsData.AdditionalInsuredDOB = DateTime.ParseExact(addIns.DateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            if (addIns.Gender == "Wanita")
                                gender = "F";

                            addInsData.AdditionalInsuredGender = gender;
                            addInsData.AdditionalInsuredRiskClassId = Convert.ToInt32(addIns.KelasPekerjaan);
                            addInsData.AdditionalInsuredAge = addIns.Age;
                            addInsData.AdditionalInsuredRelation = addIns.Relationship;
                            

                            obj.AddAdditionalInsuredData(addInsData);
                            
                            for (int i = y; i < dataInput.Additional.Riders.Count; )
                            {
                                var addInsDataId = obj.GetAdditionalInsuredData(y,transCode, addInsData.AdditionalInsuredName, addInsData.AdditionalInsuredDOB, addInsData.AdditionalInsuredGender, addInsData.AdditionalInsuredRiskClassId, addInsData.AdditionalInsuredAge, addInsData.AdditionalInsuredRelation).AdditionalInsuredDataId;
                                var rider = dataInput.Additional.Riders[i];

                                if (rider.Checked || CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi) > 0 || rider.Rider.Category.Equals("Choices"))
                                {
                                    addInsRiderData.AdditionalInsuredDataId = addInsDataId;
                                    addInsRiderData.AllocationPerc = null;
                                    addInsRiderData.Unit = null;
                                    addInsRiderData.RiderTypeId = null;

                                    addInsRiderData.RiderCode = rider.Rider.RiderCode;
                                    addInsRiderData.SumInsured = CalculatorServices.CurrencyToDecimal(rider.UangPertanggungan);

                                    if (!String.IsNullOrEmpty(rider.AllocationPercent))
                                        addInsRiderData.AllocationPerc = Double.Parse(rider.AllocationPercent.Replace("%", ""));

                                    addInsRiderData.COR = CalculatorServices.CurrencyToDecimal(rider.BiayaAsuransi);
                                    

                                    if (rider.Rider.Category.Equals("Unit"))
                                    {
                                        if (!String.IsNullOrEmpty(rider.UnitName))
                                        {
                                            addInsRiderData.RiderTypeId = Int32.Parse(rider.UnitName);
                                            addInsRiderData.Unit = Int32.Parse(rider.Unit);

                                            obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                        }
                                    }
                                    else if (rider.Rider.Category.Equals("Choices"))
                                    {
                                        if (rider.Choices != null)
                                        {
                                            foreach (var riderTypeItem in rider.Choices.Where(x => x.Checked))
                                            {
                                                decimal biayaAsuransi;
                                                addInsRiderData.RiderTypeId = riderTypeItem.RiderType.RiderTypeId;
                                                age = (int)addIns.Age;

                                                if (product.ProductType.Equals("tra"))
                                                {
                                                    biayaAsuransi = MainServices.CalculateTraditionalCOR(rider.UangPertanggungan, rider.Rider.RiderCode, addInsRiderData.RiderTypeId, premiData.PaymentMode, premiData.PaymentPeriod, rider.Rider.Category);
                                                }
                                                else
                                                {
                                                    biayaAsuransi = MainServices.CalculateUnitLinkCOR((decimal)addInsRiderData.SumInsured, rider.Rider.RiderCode, addInsRiderData.RiderTypeId, age, null, rider.Rider.Category, null);
                                                }

                                                addInsRiderData.COR = biayaAsuransi;

                                                obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                            }
                                        }
                                    }
                                    else if (rider.Rider.Category.Equals("Choice"))
                                    {
                                        if (!String.IsNullOrEmpty(rider.Choice))
                                        {
                                            addInsRiderData.RiderTypeId = Int32.Parse(rider.Choice);

                                            obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                        }
                                    }
                                    else
                                    {
                                        obj.AddAdditionalInsuredRiderData(addInsRiderData);
                                    }
                                }

                                if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                    i = i + dataInput.Additional.TertanggungTambahan.Count;
                                else
                                    i++;
                            }
                        }
                    }
                }
                #endregion

                #region AdditionalInsuredTraditionalData
                if(product.ProductCode.Equals("HLLIFE"))
                {
                    try
                    {

                        obj.DeleteAdditionalInsuredData(transCode);
                    }
                    catch (Exception ex) { }
                    if (dataInput.Premi.additionalInsured != null)
                    {
                        if (dataInput.Premi.additionalInsured != null && dataInput.Premi.additionalInsured.TertanggungTambahan != null)
                        {

                            for (int y = 0; y < dataInput.Premi.additionalInsured.TertanggungTambahan.Count; y++)
                            {
                                var addIns = dataInput.Premi.additionalInsured.TertanggungTambahan[y];
                                gender = "M";
                                addInsData = new AdditionalInsuredData();

                                addInsData.TransCode = transCode;
                                addInsData.AdditionalInsuredName = addIns.Nama;
                                addInsData.AdditionalInsuredDOB = DateTime.ParseExact(addIns.DateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                if (addIns.Gender == "Wanita")
                                    gender = "F";

                                addInsData.AdditionalInsuredGender = gender;
                                addInsData.AdditionalInsuredRiskClassId = Convert.ToInt32(addIns.KelasPekerjaan);
                                addInsData.AdditionalInsuredAge = addIns.Age;
                                addInsData.AdditionalInsuredRelation = addIns.Relationship;

                                obj.AddAdditionalInsuredData(addInsData);
                            }
                        }
                    }
                }
                #endregion

                #region TopUpWithdrawalData
                if (dataInput.TopUp != null)
                {
                    obj.DeleteTopUpWithdrawalData(transCode);
                    foreach (var topup in dataInput.TopUp.TopupWithdrawals.Where(x => x.Amount != null))
                    {
                        topupData.IsTopUp = topup.IsTopUp;
                        topupData.TransYear = topup.Year;
                        topupData.TransAmount = CalculatorServices.CurrencyToDecimal(topup.Amount);
                        topupData.TransCode = transCode;
                        
                        obj.AddTopUpWithdrawalData(topupData);
                    }
                }
                #endregion

                transNo = transCode;
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}