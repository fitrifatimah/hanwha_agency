﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public static class MainServices
    {
        public static ReportDataOutput GenerateReport(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion)
        {
            string[] errorMsg;
            var errorMsgList = new List<string>();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var result = new ReportDataOutput();
            int age;
            DataTable data;
            result.IllustrationDataSet = new DataSet { DataSetName = "ilustrasi" };

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 || (x.Choices != null ? x.Choices.Where(y => y.Checked).Count() > 0 : true)).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var ridersAddIns = riderCodeListAddIns == null ? null : on.Riders.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var ridersAll = riders;
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar).ToList();

                if (ridersAddIns != null)
                    ridersAll.AddRange(ridersAddIns);

                if (product.ProductCategory.Equals("single"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                }
                else if (product.ProductCategory.Equals("invest"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                }
                else
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput, dataInput.Insurance.Language == "en-US"));

                if (!product.ProductCategory.Equals("single"))
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

                if (product.ProductCategory.Equals("preferred") || product.ProductCategory.Equals("maxpro"))
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRate(loyaltyandsupplementary, dataInput,product, dataInput.Insurance.Language == "en-US"));

                    if (product.ProductCategory.Equals("preferred"))
                        result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                }
                else if (product.ProductCategory.Equals("invest"))
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRateInvest(data, funds, loyaltyandsupplementary, dataInput, dataInput.Insurance.Language == "en-US"));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                }

                result.ReportName = template.ReportName;
            }
            else
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var ridersAddIns = riderCodeListAddIns == null ? null : on.Riders.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var ridersAll = riders;
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar).ToList();

                if (ridersAddIns != null)
                    ridersAll.AddRange(ridersAddIns);

                if (product.ProductCategory.Equals("single"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                }
                else if (product.ProductCategory.Equals("invest"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                }
                else
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product,dataInput.Insurance.Language == "en-US"));
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput, dataInput.Insurance.Language == "en-US"));

                if (!product.ProductCategory.Equals("single"))
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

                if (product.ProductCategory.Equals("preferred") || product.ProductCategory.Equals("maxpro"))
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRate(loyaltyandsupplementary, dataInput,product, dataInput.Insurance.Language == "en-US"));

                    if (product.ProductCategory.Equals("preferred"))
                        result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                }
                else if (product.ProductCategory.Equals("invest"))
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRateInvest(data, funds, loyaltyandsupplementary, dataInput, dataInput.Insurance.Language == "en-US"));
                }

                result.ReportName = template.ReportName;
            }

            if (errorMsg != null && errorMsg.Length > 0)
            {
                errorMsgList.Add(errorMsg[0] + ". Silahkan Buat Ilustrasi Baru!");
            }
            
            if (dataInput.Premi.Investments == null || result.IllustrationDataSet.Tables["FundData"] == null)
            {
                errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            }
            else
            {
                if (dataInput.Premi.Investments.Count <= 0 || result.IllustrationDataSet.Tables["FundData"].Rows.Count <= 0)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

                if (dataInput.Premi.Investments.Where(x => x.Percentage > 0).Count() <= 0)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

                if (dataInput.Premi.Investments.Sum(x => x.Percentage) < 100)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            }

            result.ErrorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return result;
        }

        public static ReportDataOutput GenerateReportKesehatan(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion)
        {
            string[] errorMsg;
            var errorMsgList = new List<string>();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var result = new ReportDataOutput();
            int age;
            DataTable data;
            result.IllustrationDataSet = new DataSet { DataSetName = "ilustrasi" };

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);


            if (_session.AppMode == "offline")
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                //var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                //var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                //var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                //var ridersAll = riders;
                //var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar).ToList();

                //if (ridersAddIns != null)
                //    ridersAll.AddRange(ridersAddIns);

                //if (product.ProductCategory.Equals("single"))
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                //}
                //else if (product.ProductCategory.Equals("invest"))
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //}
                //else
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //}

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput, dataInput.Insurance.Language == "en-US"));

                //if (!product.ProductCategory.Equals("single"))
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

                //if (product.ProductCategory.Equals("preferred") || product.ProductCategory.Equals("maxpro"))
                //{
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRate(loyaltyandsupplementary, dataInput, product, dataInput.Insurance.Language == "en-US"));

                //    if (product.ProductCategory.Equals("preferred"))
                //        result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                //}
                //else if (product.ProductCategory.Equals("invest"))
                //{
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRateInvest(data, funds, loyaltyandsupplementary, dataInput, dataInput.Insurance.Language == "en-US"));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                //}

                result.ReportName = template.ReportName;
            }
            else
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                //var riders = on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                //var ridersAddIns = riderCodeListAddIns == null ? null : on.Riders.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                //var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                //var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                //var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                //var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                //var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                //var ridersAll = riders;
                //var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar).ToList();

                //if (ridersAddIns != null)
                //    ridersAll.AddRange(ridersAddIns);

                //if (product.ProductCategory.Equals("single"))
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                //}
                //else if (product.ProductCategory.Equals("invest"))
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //}
                //else
                //{
                //    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //}

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                //result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput, dataInput.Insurance.Language == "en-US"));

                //if (!product.ProductCategory.Equals("single"))
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

                //if (product.ProductCategory.Equals("preferred") || product.ProductCategory.Equals("maxpro"))
                //{
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRate(loyaltyandsupplementary, dataInput, product, dataInput.Insurance.Language == "en-US"));

                //    if (product.ProductCategory.Equals("preferred"))
                //        result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                //}
                //else if (product.ProductCategory.Equals("invest"))
                //{
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateSurrenderRegular(data, dataInput, productAllocationRates));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                //    result.IllustrationDataSet.Tables.Add(ReportDataServices.LoyaltyRateInvest(data, funds, loyaltyandsupplementary, dataInput, dataInput.Insurance.Language == "en-US"));
                //}

                result.ReportName = template.ReportName;
            }

            if (errorMsg != null && errorMsg.Length > 0)
            {
                errorMsgList.Add(errorMsg[0] + ". Silahkan Buat Ilustrasi Baru!");
            }

            if (dataInput.Premi.PremiBerkala == null || dataInput.Premi.PremiBerkala == "0")
            {
                errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            }
            //else
            //{
            //    if (dataInput.Premi.Investments.Count <= 0 || result.IllustrationDataSet.Tables["FundData"].Rows.Count <= 0)
            //        errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

            //    if (dataInput.Premi.Investments.Where(x => x.Percentage > 0).Count() <= 0)
            //        errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

            //    if (dataInput.Premi.Investments.Sum(x => x.Percentage) < 100)
            //        errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            //}

            result.ErrorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return result;
        }



        public static ReportDataOutput GenerateReportTraditional(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion)
        {
            string[] errorMsg;
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var result = new ReportDataOutput();
            DataTable data;
            List<ProductTahapanRate> tahapansRate = new List<ProductTahapanRate>();
            result.IllustrationDataSet = new DataSet { DataSetName = "ilustrasi" };

            if (dataInput.Rider == null)
            {
                dataInput.Rider = new RiderViewModel();
                dataInput.Rider.BiayaAsuransiTambahan = "0";
            }

            var riderCodeList = dataInput.Rider.Riders != null ? dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList() :null;
            var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);

            if (_session.AppMode == "offline")
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                if (dataInput.Nasabah.NamaProduk == "HSCI" && dataInput.Rider.Riders[0].Checked == true)
                {
                    template.ReportName = dataInput.Insurance.Language != "id-ID" ? "HWPCI_EN" : "HWPCI_ID";
                }
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar).ToList();
                //
                var cashValues = on.CashValueRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var tahapanRates = on.ProductTahapanRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var riders = riderCodeList == null ? null : on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var tahapans = on.ProductTahapanRates.Where(x => x.ProductCode == product.ProductCode && (product.ProductCode == "HLKIDS" ? x.ChildAge == dataInput.Nasabah.UmurAnak : true)).ToList();

                if (tahapans != null && product.ProductCode == "HLEDU")
                {
                    var awalTahunTahapan = dataInput.Premi.ModeBayarPremi == "sekaligus" && (5 + dataInput.Nasabah.UmurAnak) > 18 ? (5 + dataInput.Nasabah.UmurAnak) : 18;
                    foreach (var tahapan in tahapans)
                    {
                        var inputTahapan = new ProductTahapanRate();
                        inputTahapan.ProductTahapanRateId = tahapan.ProductTahapanRateId;
                        inputTahapan.ProductCode = tahapan.ProductCode;
                        inputTahapan.Year = tahapan.Year + awalTahunTahapan;
                        inputTahapan.RateTahapan = tahapan.RateTahapan;
                        inputTahapan.IsActive = tahapan.IsActive;
                        tahapansRate.Add(inputTahapan);
                    }
                }
                else
                    tahapansRate = tahapans;

                if (dataInput.Nasabah.NamaProduk == "HLEDU")
                    data = ReportDataServices.CalculateBenefitEducation(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if(dataInput.Nasabah.NamaProduk == "HLKIDS")
                    data = ReportDataServices.CalculateBenefitKidsPlan(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLFUTURE")
                    data = ReportDataServices.CalculateBenefitFuturePro(dataInput, productAllocationRates, cashValues, out errorMsg);
                //--alif
                else if (dataInput.Nasabah.NamaProduk == "HLSAVING")
                    data = ReportDataServices.CalculateBenefitSavingPro(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLLIFE")
				    data = ReportDataServices.CalculateBenefitLifePro(dataInput, productAllocationRates, cashValues, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HSCI")
                    data = ReportDataServices.CalculateBenefitSmartCI(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);

                else
                {
                    data = null;
                    errorMsg = null;
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                //add paymentmethod alif
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                //
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, null ,dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, riders));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DanaTahapan(dataInput,tahapansRate));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

                if (dataInput.Nasabah.NamaProduk == "HSCI")
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DetailSmartCI(dataInput, cashValues, out errorMsg));
                }

                result.ReportName = template.ReportName;
            }
            else
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                if (dataInput.Nasabah.NamaProduk == "HSCI" && dataInput.Rider.Riders[0].Checked == true)
                {
                    template.ReportName = dataInput.Insurance.Language != "id-ID" ? "HWPCI_EN" : "HWPCI_ID";
                }

                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var cashValues = on.CashValueRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //--alif
                var paymenmethod = on.PaymentMethods.Where(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar && x.PM_ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                //
                var tahapanRates = on.ProductTahapanRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var riders = riderCodeList == null ? null : on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var tahapans = on.ProductTahapanRates.Where(x => x.ProductCode == product.ProductCode && (product.ProductCode == "HLKIDS" ? x.ChildAge == dataInput.Nasabah.UmurAnak : true)).ToList();

                if (tahapans != null && product.ProductCode == "HLEDU")
                {
                    var awalTahunTahapan = dataInput.Premi.ModeBayarPremi == "sekaligus" && (5 + dataInput.Nasabah.UmurAnak) > 18 ? (5 + dataInput.Nasabah.UmurAnak) : 18;
                    foreach (var tahapan in tahapans)
                    {
                        var inputTahapan = new ProductTahapanRate();
                        inputTahapan.ProductTahapanRateId = tahapan.ProductTahapanRateId;
                        inputTahapan.ProductCode = tahapan.ProductCode;
                        inputTahapan.Year = tahapan.Year + awalTahunTahapan;
                        inputTahapan.RateTahapan = tahapan.RateTahapan;
                        inputTahapan.IsActive = tahapan.IsActive;
                        tahapansRate.Add(inputTahapan);
                    }
                }
                else
                    tahapansRate = tahapans;

                if (dataInput.Nasabah.NamaProduk == "HLEDU")
                    data = ReportDataServices.CalculateBenefitEducation(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLKIDS")
                    data = ReportDataServices.CalculateBenefitKidsPlan(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLFUTURE")
                    data = ReportDataServices.CalculateBenefitFuturePro(dataInput, productAllocationRates, cashValues, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLSAVING")
                    data = ReportDataServices.CalculateBenefitSavingPro(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                    data = ReportDataServices.CalculateBenefitLifePro(dataInput, productAllocationRates, cashValues, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLGOLDENA")
                    data = ReportDataServices.CalculateBenefitGoldenLifeA(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLGOLDENB")
                    data = ReportDataServices.CalculateBenefitGoldenLifeB(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HSCI")
                    data = ReportDataServices.CalculateBenefitSmartCI(dataInput, productAllocationRates, cashValues, paymenmethod, out errorMsg);

                else
                {
                    data = null;
                    errorMsg = null;
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product, dataInput.Insurance.Language == "en-US"));
                //add alif
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product, paymenmethod, out errorMsg));
                //
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders,null ,dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, riders));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DanaTahapan(dataInput, tahapansRate));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));

                if (dataInput.Nasabah.NamaProduk == "HSCI")
                {
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DetailSmartCI(dataInput, cashValues, out errorMsg));
                }

                result.ReportName = template.ReportName;
            }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateTraditionalHeader(dataInput));
                if (dataInput.Nasabah.NamaProduk != "HLGOLDENA" && dataInput.Nasabah.NamaProduk != "HLGOLDENB" && dataInput.Nasabah.NamaProduk != "HSCI")
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanTraditional(data, product));
                else if (dataInput.Nasabah.NamaProduk == "HSCI")
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanTraditionalSmartCI(data, product));
                else
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanTraditionalGoldenLife(data, product));

            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataAnakNasabah(dataInput, dataInput.Insurance.Language == "en-US"));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput, dataInput.Insurance.Language == "en-US"));
            if (dataInput.Nasabah.NamaProduk != "HLHEALTH")
            {
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataInput1(dataInput, dataInput.Insurance.Language == "en-US"));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataInput2(dataInput));
            }
            if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput, dataInput.Insurance.Language == "en-US", product));

            if (errorMsg != null && errorMsg.Length > 0)
            {
                result.ErrorMsg = !errorMsg.Any() ? null : errorMsg.ToArray();
            }

            return result; 
        }

        public static ReportDataOutput CalculateFundBenefit(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitSingle(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("regular") || product.ProductCategory.Equals("worksite"))
                    data = ReportDataServices.CalculateFundBenefitRegular(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("preferred"))
                    data = ReportDataServices.CalculateFundBenefitPrefferedMaxPro(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg,loyaltyandsupplementary);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitInvest(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg,loyaltyandsupplementary);
                else if (product.ProductCategory.Equals("maxpro"))
                    data = ReportDataServices.CalculateFundBenefitPrefferedMaxPro(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg, loyaltyandsupplementary);
                else //if (product.ProductCategory.Equals("wizer"))
                    data = ReportDataServices.CalculateFundBenefitWizer(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitSingle(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("regular") || product.ProductCategory.Equals("worksite"))
                    data = ReportDataServices.CalculateFundBenefitRegular(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("preferred"))
                    data = ReportDataServices.CalculateFundBenefitPrefferedMaxPro(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg, loyaltyandsupplementary);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitInvest(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg, loyaltyandsupplementary);
                else if (product.ProductCategory.Equals("maxpro"))
                    data = ReportDataServices.CalculateFundBenefitPrefferedMaxPro(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg, loyaltyandsupplementary);
                else //if (product.ProductCategory.Equals("wizer"))
                    data = ReportDataServices.CalculateFundBenefitWizer(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static ReportDataOutput CalculateFundBenefitWithRider(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = off.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : off.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static ReportDataOutput CalculateFundBenefitTopupWithdrawal(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = off.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : off.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else if (product.ProductCategory.Equals("invest"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static TransLogOutput CreateTransLog(SummaryViewModel dataInput, string agentCode)
        {
            string transCode;
            var result = new TransLogOutput();
            result.TransCode = TransLogServices.CreateTransLog(dataInput, agentCode, out transCode);

            return result;
        }

        public static void UpdateTransLog(SummaryViewModel dataInput, string agentCode, string transCode)
        {   
            TransLogServices.UpdateTransLog(dataInput, agentCode, transCode);
        }

        public static decimal CalculateCostOfRider(SummaryViewModel dataInput, decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, Product product, int? unit)
        {
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            decimal cor = 0;
            var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;

            if (_session.AppMode == "offline")
            {
                var riderRates = on.RiderRates.Where(x => x.RiderCode == riderCode).ToList();
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();

                cor = CalculatorServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, riderRates, annuityFactors, unit);
            }
            else
            {
                var riderRates = on.RiderRates.Where(x => x.RiderCode == riderCode).ToList();
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();

                cor = CalculatorServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, riderRates, annuityFactors, unit);
            }

            return cor;
        }

        public static decimal CalculateCostOfInsurance(SummaryViewModel dataInput, int age)
        {
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            decimal coi = 0;
            var riskClass = 0;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
            else
                riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

            if (_session.AppMode == "offline")
            {
                var upRates = on.RiderRates.FirstOrDefault(x => x.Age == age && x.RiderCode == null && x.Category == null);
                coi = CalculatorServices.CalculateCostOfInsurance(dataInput.Premi.UangPertanggungan, upRates.Rate);
            }
            else
            {
                var upRates = on.RiderRates.FirstOrDefault(x => x.Age == age && x.RiderCode == null && x.Category == null);
                coi = CalculatorServices.CalculateCostOfInsurance(dataInput.Premi.UangPertanggungan, upRates.Rate);
            }

            if(dataInput.Nasabah.NamaProduk == "HLINVEST")
            {
                Product product = obj.GetProduct(dataInput.Nasabah.NamaProduk);
                Rider rider = obj.GetActiveRider().Where(x => x.RiderCode == "ADB").FirstOrDefault();
                var up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
                up = up > 500000000 ? 500000000 : up;
                var corADB = (age+1) > (rider.CoverTerm/12) ? 0 : MainServices.CalculateCostOfRider(dataInput, up , rider.RiderCode, null, age, riskClass, rider.Category, product, null);
                coi += corADB;
            }

            return coi;
        }

        public static double CalculatePremiTraditional(string up, string type, int insAge, int basicTerm, Product product, int? mpp)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            double? rate;
            int premiValue = 0;
            int? umurAnak = product.ProductCode == "HLFUTURE" /*|| product.ProductCode == "HLSAVING"*/ ? (int?)null : _session.dataNasabah.UmurAnak;
            mpp = product.ProductCode == "HLFUTURE" || product.ProductCode == "HSCI" || product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB" ? mpp : (int?)null;

            if (product.ProductCode == "HLEDU")
                rate = obj.GetTraditionalRate(type, _session.dataNasabah.UmurAnak, insAge, basicTerm, Convert.ToInt32(_session.premiInvestmentData.MasaAsuransi)).Rate;
            else if (product.ProductCode == "HLHEALTH")
                rate = Convert.ToDouble(obj.GetTarifPremi(product.ProductCode, type, insAge, mpp, premiValue).Tarif);
            else
                rate = obj.GetProductRate(product.ProductCode, umurAnak, insAge, mpp).Rate;

            var paymentMethod = product.ProductCode == "HLEDU" && type == "sekaligus" ? 1 : GetFactorPaymentMethod(Convert.ToInt32(_session.premiInvestmentData.CaraBayar), _session.dataNasabah.NamaProduk) / 100;
            var premi = product.ProductCode == "HLHEALTH" ? rate : (rate / 1000) * Convert.ToDouble(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.UangPertanggungan)) * paymentMethod;

            return premi.Value;
        }

        public static decimal CalculateTraditionalCOR(string up, string code, int? ridertype, string type, int? riderCov, string category)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            if (category == "Basic")
            {
                int basicCov = Convert.ToInt32(_session.premiInvestmentData.MasaAsuransi);
                int insAge = 0;
                int basicTerm = 1;
                int riderTerm = 1;

                if (type != "sekaligus")
                {
                    basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                    riderTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                }

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, null, type, insAge, basicCov, basicTerm, riderCov, riderTerm);
                var cor = (rate.Rate / 1000) * Convert.ToDouble(up);
                return CalculatorServices.Round((decimal)cor, -1);
            }
            else //if (category == "Choices")
            {
                int insAge = 0;
                var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
                int basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, ridertype, type, insAge, basicTerm, basicTerm, null, null);
                var premi = CalculatePremiTraditional(up, type, insAge, basicTerm, product,basicTerm);
                var factor = Convert.ToDouble(_session.premiInvestmentData.CaraBayar);
                var annFactor = obj.GetAnnuityFactor(basicTerm, null, product.ProductType);
                var paymentMethod = GetFactorPaymentMethod(Convert.ToInt32(_session.premiInvestmentData.CaraBayar), _session.dataNasabah.NamaProduk) / 100;
                var upR = premi * factor;
                var cor = (rate.Rate / 1000) * upR * annFactor.Rate * paymentMethod;
                return  CalculatorServices.Round((decimal)cor,-1);
            }
        }

        public static decimal CalculateUnitLinkCOR(decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, int? unit)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            SummaryViewModel dataInput = new SummaryViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            if (riderCategory.Equals("Choices"))
            {
                up = CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala) + CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.TopupBerkala);

                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
            else if (riderCategory.Equals("Unit"))
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
            else
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
        }

        public static InsuranceCostTraditionalViewModel CalculatePremiAllTertanggung(string productCode, string santunanduka, string up, int phAge, int? insuredAge,double? paymentMethod,int mpp, bool isTTu)
        {
            var ret = new InsuranceCostTraditionalViewModel();
            List<ListTertanggung> ListTertanggung = new List<ListTertanggung>();
            decimal premi;
            var umurTertanggung = _session.dataNasabah.TertanggungUtama == "Ya" ? _session.dataNasabah.UmurPemegangPolis : _session.dataNasabah.UmurTertanggungUtama.Value;
            decimal TotalPremi = 0;

            premi = CalculatorServices.CalculatePremiLifePro(productCode, santunanduka, _session.dataNasabah.UmurPemegangPolis, _session.premiInvestmentData.RencanaMasaPembayaran, "PA", paymentMethod);
            TotalPremi += premi;
            ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Pemegang Polis", Premi = CalculatorServices.DecimalToCurrency(premi) });

            premi = CalculatorServices.CalculatePremiLifePro(productCode, _session.premiInvestmentData.UangPertanggungan, umurTertanggung, _session.premiInvestmentData.RencanaMasaPembayaran, "NON_PA", paymentMethod);
            TotalPremi += premi;
            ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Tertanggung Utama", Premi = CalculatorServices.DecimalToCurrency(premi) });

            var no = 1;
            foreach (var ttgTambahan in _session.addInsTraditionalData.TertanggungTambahan)
            {
                premi = CalculatorServices.CalculatePremiLifePro(productCode, santunanduka, ttgTambahan.Age.Value, _session.premiInvestmentData.RencanaMasaPembayaran, "PA", paymentMethod);
                TotalPremi += premi;
                ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Tertanggung Tambahan " + no, Premi = CalculatorServices.DecimalToCurrency(premi) });
                no = no + 1;
            }
            ret.ListTertanggung = ListTertanggung;
            ret.TotalPremi = CalculatorServices.DecimalToCurrency(TotalPremi);

            return ret;
        }
        public static double? GetFactorPaymentMethod(int code, string productcode)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            var factor = obj.GetPaymentMethod(code, productcode);

            return factor.Factor;
        }

        public static string GetRelationshipText(string code)
        {
            var ret = "";

            if (code == "Pasangan")
                ret = App_Data.Text.Pasangan;
            else if (code == "Anak Angkat")
                ret = App_Data.Text.AnakAngkat;
            else if (code == "Anak Kandung")
                ret = App_Data.Text.AnakKandung;
            else if (code == "Orang Tua Angkat")
                ret = App_Data.Text.OrangTuaAngkat;
            else if (code == "Orang Tua Kandung")
                ret = App_Data.Text.OrangTuaKandung;
            else if (code == "Kakak Kandung")
                ret = App_Data.Text.KakakKandung;
            else if (code == "Adik Kandung")
                ret = App_Data.Text.AdikKandung;
            else if (code == "Paman/bibi")
                ret = App_Data.Text.Paman;
            else if (code == "Cucu")
                ret = App_Data.Text.Cucu;
            else if (code == "Keponakan")
                ret = App_Data.Text.Keponakan;
            else if (code == "" || code == null)
                ret = App_Data.Text.DiriSendiri;
            else
                ret = code;

            return ret;
        }

        public static bool IsCoveredByRider(Rider rider, int age, string relation, string type)
        {
            int? ridermaxAge = type == "PH" ? rider.PolicyHolderMaxAge : rider.InsuredMaxAge;
            int? riderminAge = type == "PH" ? rider.PolicyHolderMinAge : rider.InsuredMinAge;
            int? maxAge = ridermaxAge / 12;
            int? minAge = riderminAge / 12;
            int? childMaxAge = rider.ChildInsuredMaxAge / 12;
            int? childMinAge = rider.ChildInsuredMinAge / 12;
            var productName = _session.dataNasabah.NamaProduk;
            int? coverTerm = rider.CoverTermWizer / 12;
            var covered = true;
            if (type != "PH" && rider.ChildInsuredMaxAge != null && rider.ChildInsuredMinAge != null && (relation == "Anak Angkat" || relation == "Anak Kandung"))
                covered = age <= childMaxAge && age >= childMinAge;
            else if (riderminAge != null && ridermaxAge != null)
                covered = age <= maxAge && age >= minAge;

            if (productName == "HLWIZ")
                covered = age < coverTerm;

            return covered;
        }

        public static bool ValidateUP(Product product, int age,string gender, decimal up,decimal? premi)
        {
            var minAmtUP = product.MinAmountSumInsured;
            var minPercUP = product.MinPercSumInsured;
            var maxAgeJouvenille = product.ChildMaxAge / 12;
            var maxJouvenille = product.MaxAmountSumInsuredChild;
            //decimal? upAfter = 0; 
            var ret = false;
                            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
                            var sa_mult = obj.GetSAMultiplier(product.ProductCode, age, product.ProductCode == "HLINVEST" ? null : gender == "Wanita" ? "F" : "M").Multiplier;
                            sa_mult = sa_mult == null ? 0 : sa_mult;
                            var maxSA = sa_mult * premi ;
                            var maxUP = age < maxAgeJouvenille ? maxJouvenille < maxSA ? maxJouvenille : maxSA : maxSA;
                            var maxPremi = maxUP / (minPercUP / 100);

                            if (up > maxUP)
                                ret = true;              

                        return ret;
        }

        public static string GetKeteranganMedis(SummaryViewModel dataInput, string type, int insAge, int? ttgNo)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var ret = "";
            decimal upDeath = type == "utama" ? CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) : 0;
            decimal upCI = 0;
            Product product = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal basicUP = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if(product.ProductType == "ul" || product.ProductCategory == "education")
            {
                var on = new OnlineEntities();
                var prodType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == prodType && x.Year == insAge && x.Factor == 6).FirstOrDefault();
                var annFactor = annuityFactors != null ? (decimal)annuityFactors.Rate : 1;

                if (type == "utama" || type == "pemegangpolis")
                {
                    if (dataInput.Rider != null)
                    {
                        if (dataInput.Rider.Riders != null)
                        {
                            var dataRider = dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0);
                            if (type == "utama")
                                dataRider = dataRider.Where(x => x.Rider.RiderCode != "POP");

                            foreach (var riderUtama in dataRider)
                            {
                                if (riderUtama.Rider.Category == "Choices")
                                {
                                    if (riderUtama.Choices != null && riderUtama.Choices.Count > 0)
                                    {
                                        foreach (var riderTypeItem in riderUtama.Choices.Where(x => x.Checked))
                                        {
                                            if (riderTypeItem.RiderType.TypeName.Contains("CI"))
                                                upCI += CalculatorServices.CurrencyToDecimal(riderUtama.UangPertanggungan) * annFactor;
                                            else if (riderTypeItem.RiderType.TypeName.Contains("Death"))
                                                upDeath += CalculatorServices.CurrencyToDecimal(riderUtama.UangPertanggungan) * annFactor;
                                        }
                                    }
                                }
                                else if (riderUtama.Rider.RiderCode == "TR")
                                    upDeath += CalculatorServices.CurrencyToDecimal(riderUtama.UangPertanggungan);
                                else
                                {
                                    if (riderUtama.Rider.RiderName.Contains("CI"))
                                        upCI += CalculatorServices.CurrencyToDecimal(riderUtama.UangPertanggungan);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (dataInput.Additional != null)
                    {
                        if (dataInput.Additional.Riders != null)
                        {
                            for (int y = (ttgNo.Value - 1); y < (dataInput.Additional.Riders.Count); )
                            {
                                var riderTT = dataInput.Additional.Riders[y];
                                if (riderTT.Checked)
                                {
                                    if (riderTT.Rider.Category == "Choices")
                                    {
                                        if (riderTT.Choices != null && riderTT.Choices.Count > 0)
                                        {
                                            foreach (var riderTypeItem in riderTT.Choices.Where(x => x.Checked))
                                            {
                                                if (riderTypeItem.RiderType.TypeName.Contains("CI"))
                                                    upCI += CalculatorServices.CurrencyToDecimal(riderTT.UangPertanggungan) * annFactor;
                                                else if (riderTypeItem.RiderType.TypeName.Contains("Death"))
                                                    upDeath += CalculatorServices.CurrencyToDecimal(riderTT.UangPertanggungan) * annFactor;
                                            }
                                        }
                                    }
                                    else if (riderTT.Rider.RiderCode == "TR")
                                            upDeath += CalculatorServices.CurrencyToDecimal(riderTT.UangPertanggungan);
                                    else
                                    {
                                        if (riderTT.Rider.RiderName.Contains("CI"))
                                            upCI += CalculatorServices.CurrencyToDecimal(riderTT.UangPertanggungan);
                                    }
                                }

                                y = y + dataInput.Additional.TertanggungTambahan.Count;
                            }
                        }
                    }
                }
            }
            else if (product.ProductCategory == "kids")
            {
                if(type == "anak")
                    upDeath = (decimal)0.1 * basicUP;
                else
                {
                    upDeath = basicUP + (CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) * Convert.ToInt32(dataInput.Premi.CaraBayar) * dataInput.Premi.RencanaMasaPembayaran);
                }
            }
            else if (product.ProductCategory == "futurepro")
            {
                var trigerUP = obj.GetTrigerSumInsured(insAge, product.ProductCode);
                decimal upAkhir = CalculatorServices.Round(basicUP * (decimal)Math.Pow((1 + ((double)(decimal)product.RateUP.Value / 100)), dataInput.Premi.RencanaMasaPembayaran - 1),0);

                if (trigerUP != null)
                    upDeath = basicUP > trigerUP.SumInsured ? (basicUP + upAkhir) / 2 : basicUP;
                else
                    upDeath = (basicUP + upAkhir) / 2;
            }

            var up = upCI > upDeath ? upCI : upDeath;
            if (product.ProductCategory == "goldenA" || product.ProductCategory == "goldenB")
            {

                var ketMedis = obj.GetInfoMedical(insAge, up, product.ProductCategory);
                if (ketMedis != null)
                    if (dataInput.Insurance.Language == "en-US")
                    {
                        if (ketMedis.TipeMedis == "Medis")
                            ketMedis.TipeMedis = "Medical";
                        else if (ketMedis.TipeMedis == "Non Medis")
                            ketMedis.TipeMedis = "Non Medical";
                        ret = ketMedis.TipeMedis;
                    }
                    else
                    {
                        ret = ketMedis.TipeMedis;
                    }
                else
                    if (dataInput.Insurance.Language == "en-US") { ret = "Medical"; }
                    else { ret = "Medis"; }
            }
            else if (product.ProductCategory == "prehealth")
            {
                up = Convert.ToDecimal(dataInput.Premi.JenisPlan);
                var ketMedis = obj.GetInfoMedical(insAge, up, product.ProductCategory);
                if (ketMedis != null)
                    if (dataInput.Insurance.Language == "en-US")
                    {
                        if (ketMedis.TipeMedis == "Medis")
                            ketMedis.TipeMedis = "Medical";
                        else if (ketMedis.TipeMedis == "Non Medis")
                            ketMedis.TipeMedis = "Non Medical";
                        ret = ketMedis.TipeMedis;
                    }
                    else
                    {
                        ret = ketMedis.TipeMedis;
                    }
                else
                    if (dataInput.Insurance.Language == "en-US") { ret = "Medical"; }
                    else { ret = "Medis"; }
            }
            else
            {
                if (product.ProductCategory != "savingpro")
                {
                    var ketMedis = obj.GetInfoMedical(insAge, up, dataInput.Nasabah.NamaProduk == "HLWIZ" ? "wizer" : null);
                    if (ketMedis != null)
                        if (dataInput.Insurance.Language == "en-US")
                        {
                            if (ketMedis.TipeMedis == "Medis")
                                ketMedis.TipeMedis = "Medical";
                            else if (ketMedis.TipeMedis == "Non Medis")
                                ketMedis.TipeMedis = "Non Medical";
                            ret = ketMedis.TipeMedis;
                        }
                        else
                        {
                            ret = ketMedis.TipeMedis;
                        }
                    else
                        if (dataInput.Insurance.Language == "en-US") { ret = "Medical"; }
                        else { ret = "Medis"; }
                }
                else
                {
                    var ketMedis = obj.GetInfoMedical(insAge, up, dataInput.Nasabah.NamaProduk == "HLSAVING" ? "savingpro" : null);
                    if (ketMedis != null)
                        if (dataInput.Insurance.Language == "en-US")
                        {
                            if (ketMedis.TipeMedis == "Medis")
                                ketMedis.TipeMedis = "Medical";
                            else if (ketMedis.TipeMedis == "Non Medis")
                                ketMedis.TipeMedis = "Non Medical";
                            ret = ketMedis.TipeMedis;
                        }
                        else
                        {
                            ret = ketMedis.TipeMedis;
                        }
                    else
                        if (dataInput.Insurance.Language == "en-US") { ret = "Medical"; }
                        else { ret = "Medis"; }
                }
            }
            return ret;
        }
    }
}