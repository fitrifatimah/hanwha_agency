﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public class InvestmentFundRow
    {
        public int Tahun { get; set; }
        public decimal Low { get; set; }
        public decimal Med { get; set; }
        public decimal High { get; set; }
    }

    public static class ReportDataServices
    {
        public static DataTable General(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion, Product product)
        {
            var dtGeneral = new DataTable { TableName = "General" };

            dtGeneral.Columns.Add("Version");
            dtGeneral.Columns.Add("AgentCode");
            dtGeneral.Columns.Add("AgentName");
            dtGeneral.Columns.Add("HasAsuransiTambahan");
            dtGeneral.Columns.Add("BiayaAsuransi");
            dtGeneral.Columns.Add("BiayaAsuransiUtama");
            dtGeneral.Columns.Add("BiayaAsuransiTambahan");
            dtGeneral.Columns.Add("TotalBiayaAsuransi");
            dtGeneral.Columns.Add("DueDate");
            dtGeneral.Columns.Add("NearestBirthday");
            dtGeneral.Columns.Add("Insured");
            dtGeneral.Columns.Add("InsuredAge");
            dtGeneral.Columns.Add("MPP");

            decimal biayaAsuransi = 0;
            decimal biayaAsuransiUtama = 0;
            if (product.ProductType != "kes")
            {
                biayaAsuransi = product.ProductType.Equals("tra") ? CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) : Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi) / 12, 0);
                biayaAsuransiUtama = product.ProductType.Equals("tra") ? CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan) : CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);

            }

            decimal biayaAsuransiTambahan = 0;

            if (product.HasAdditionalInsured == true)
                biayaAsuransiTambahan = dataInput.Additional.TertanggungTambahan != null || (dataInput.Additional.BiayaAsuransiTertanggungTambahan != null && dataInput.Additional.BiayaAsuransiTertanggungTambahan != "0") ? CalculatorServices.CurrencyToDecimal(dataInput.Additional.BiayaAsuransiTertanggungTambahan) : 0;

            if (dataInput.Rider != null)
            {
                if (dataInput.Rider.Riders != null)
                {
                    if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                    {
                        RiderItem dataPoP = dataInput.Rider.Riders.Where(x => x.Rider.RiderCode == "POP").FirstOrDefault();
                        biayaAsuransiTambahan = biayaAsuransiTambahan + CalculatorServices.CurrencyToDecimal(dataPoP.BiayaAsuransi);
                        biayaAsuransiUtama = biayaAsuransiUtama - CalculatorServices.CurrencyToDecimal(dataPoP.BiayaAsuransi);
                    }
                }
            }

            if (!product.ProductType.Equals("tra"))
            {
                biayaAsuransiUtama = Math.Round(biayaAsuransiUtama / 12, 0);
                biayaAsuransiTambahan = Math.Round(biayaAsuransiTambahan / 12, 0);
            }

            var totalBiayaAsuransi = biayaAsuransi + biayaAsuransiUtama + biayaAsuransiTambahan;

            var newRow = dtGeneral.NewRow();
            newRow["Version"] = appVersion;
            newRow["AgentCode"] = agentCode;
            newRow["AgentName"] = agentName;
            newRow["HasAsuransiTambahan"] = product.HasAdditionalInsured;
            newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
            newRow["BiayaAsuransiUtama"] = CalculatorServices.DecimalToCurrency(biayaAsuransiUtama);
            newRow["BiayaAsuransiTambahan"] = CalculatorServices.DecimalToCurrency(biayaAsuransiTambahan);
            newRow["TotalBiayaAsuransi"] = CalculatorServices.DecimalToCurrency(totalBiayaAsuransi);

            var dateNow = DateTime.Now.Date;
            var dueDate = dateNow.AddDays(60);

            newRow["DueDate"] = dueDate.ToString("dd MMM yyyy");

            if (dataInput.Nasabah.NamaProduk != "HPCP")
            {
                if (dataInput.Nasabah.TertanggungUtama == "Ya")
                {
                    if (dataInput.Rider != null)
                    {
                        if (dataInput.Rider.Riders != null)
                        {
                            if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                            {
                                if (dataInput.Nasabah.NamaProduk == "HSCI")
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd MMM yyyy", null);
                                }
                                else 
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                                }
                            }
                            else
                            {
                                if (dataInput.Nasabah.NamaProduk == "HSCI")
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd MMM yyyy", null);
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                                }
                            }
                        }
                        else
                        {
                            if (dataInput.Nasabah.NamaProduk == "HSCI")
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd MMM yyyy", null);
                            }
                            else
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                            }
                        }
                    }
                    else
                    {
                        if (dataInput.Nasabah.NamaProduk == "HSCI")
                        {
                            newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd MMM yyyy", null);
                        }
                        else
                        {
                            newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                        }
                    }
                }
                else
                {
                    if (dataInput.Rider != null)
                    {
                        if (dataInput.Rider.Riders != null)
                        {
                            if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                            {
                                if (dataInput.Nasabah.NamaProduk == "HSCI")
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd MMM yyyy", null);
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                                }

                            }
                            else
                            {
                                if (dataInput.Nasabah.NamaProduk == "HSCI")
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd MMM yyyy", null);
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                                }

                            }
                        }
                        else
                        {
                            if (dataInput.Nasabah.NamaProduk == "HSCI")
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd MMM yyyy", null);
                            }
                            else
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                            }

                        }
                    }
                    else
                    {
                        if (dataInput.Nasabah.NamaProduk == "HSCI")
                        {
                            newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd MMM yyyy", null);
                        }
                        else
                        {
                            newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                        }
                    }
                }

            }
            else
            {
                    if (dataInput.Nasabah.TertanggungUtama == "Ya")
                    {
                        newRow["NearestBirthday"] = dataInput.latestBirthday.ToString("dd/MM/yyyy", null);
                    }
                    else
                    {
                        newRow["NearestBirthday"] = dataInput.latestBirthdayTTU.ToString("dd/MM/yyyy", null);
                    }

            }

            newRow["Insured"] = dataInput.Nasabah.TertanggungUtama == "Ya" ? dataInput.Nasabah.NamaPemegangPolis : dataInput.Nasabah.NamaTertanggungUtama;
            newRow["InsuredAge"] = dataInput.Nasabah.TertanggungUtama == "Ya" ? dataInput.Nasabah.UmurPemegangPolis : dataInput.Nasabah.UmurTertanggungUtama;
            newRow["MPP"] = product.ProductCode == "HSCI"? dataInput.Premi.RencanaMasaPembayaran : 0;

            dtGeneral.Rows.Add(newRow);

            return dtGeneral;
        }

        

        public static DataTable NasabahData(SummaryViewModel dataInput, Product product, bool isEnglish)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtNasabah = new DataTable { TableName = "NasabahData" };
            var relation = dataInput.Nasabah.Relation;
            dtNasabah.Columns.Add("DataType");
            dtNasabah.Columns.Add("Data");


            var newRow = dtNasabah.NewRow();
            newRow["DataType"] = product.ProductCode == "HLWORK" || relation == "Karyawan" ? App_Data.Text.NamaPerusahaan : App_Data.Text.NamaPemegangPolis;
            newRow["Data"] = dataInput.Nasabah.NamaPemegangPolis;
            dtNasabah.Rows.Add(newRow);

            if (!(product.ProductCode == "HLWORK") && relation != "Karyawan" && relation != "Pengurus Yayasan")
            {
                RiskClass pekerjaan = obj.GetRiskClass(Convert.ToInt32(dataInput.Nasabah.StatusPekerjaanPemegangPolis));

                newRow = dtNasabah.NewRow();
                newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
                newRow["Data"] = dataInput.Nasabah.UmurPemegangPolis + App_Data.Text.TahunUmurSpasi + " / " + (dataInput.Nasabah.JenisKelaminPemegangPolis == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtNasabah.Rows.Add(newRow);

                if (product.ProductCode == "HSCI" || product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB" || product.ProductCode == "HPCP" || product.ProductCode == "HLSAVING")
                {
                    newRow = dtNasabah.NewRow();
                    newRow["DataType"] = App_Data.Text.PekerjaanGolden;
                    newRow["Data"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                    dtNasabah.Rows.Add(newRow);
                }
                else
                {
                    newRow = dtNasabah.NewRow();
                    newRow["DataType"] = App_Data.Text.Pekerjaan;
                    newRow["Data"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                    dtNasabah.Rows.Add(newRow);
                }
            }

            if (dataInput.Additional != null)
            {
                if (dataInput.Additional.RidersTertanggungUtama.Where(x => x.Rider.RiderCode == "POP" && x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Count() > 0)
                {
                    newRow = dtNasabah.NewRow();
                    newRow["DataType"] = App_Data.Text.KetMedis;
                    newRow["Data"] = MainServices.GetKeteranganMedis(dataInput, "pemegangpolis", dataInput.Nasabah.UmurPemegangPolis, null);
                    dtNasabah.Rows.Add(newRow);
                }
            }

            return dtNasabah;
        }

        public static DataTable DataTertanggungUtama(SummaryViewModel dataInput, bool isEnglish)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtTTU = new DataTable { TableName = "DataTertanggungUtama" };
            dtTTU.Columns.Add("DataType");
            dtTTU.Columns.Add("Data");
            string namaTTU; int? umurTTU; string jkTTU; string dobTTU; RiskClass pekerjaanTTU; string hubungan;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
            {
                namaTTU = dataInput.Nasabah.NamaPemegangPolis;
                umurTTU = dataInput.Nasabah.UmurPemegangPolis;
                jkTTU = dataInput.Nasabah.JenisKelaminPemegangPolis;
                dobTTU = dataInput.Nasabah.TanggalLahirPemegangPolis;
                pekerjaanTTU = obj.GetRiskClass(Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis));
                hubungan = "Diri Sendiri";
            }
            else
            {
                namaTTU = dataInput.Nasabah.NamaTertanggungUtama;
                umurTTU = dataInput.Nasabah.UmurTertanggungUtama;
                jkTTU = dataInput.Nasabah.JenisKelaminTertanggungUtama;
                dobTTU = dataInput.Nasabah.TanggalLahirTertanggungUtama;
                pekerjaanTTU = obj.GetRiskClass(Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama));
                hubungan = dataInput.Nasabah.Relation;
            }

            var newRow = dtTTU.NewRow();
            newRow["DataType"] = App_Data.Text.NamaTertanggung;
            newRow["Data"] = namaTTU;
            dtTTU.Rows.Add(newRow);

            newRow = dtTTU.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = umurTTU + App_Data.Text.TahunUmurSpasi + " / " + (jkTTU == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtTTU.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk == "HSCI" || dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLGOLDENB" || dataInput.Nasabah.NamaProduk == "HPCP" || dataInput.Nasabah.NamaProduk == "HLSAVING")
            {
                newRow = dtTTU.NewRow();
                newRow["DataType"] = App_Data.Text.PekerjaanGolden;
                newRow["Data"] = isEnglish ? pekerjaanTTU.RiskClassDesc_EN : pekerjaanTTU.RiskClassDesc;
                dtTTU.Rows.Add(newRow);

                newRow = dtTTU.NewRow();
                newRow["DataType"] = App_Data.Text.KetMedisGolden;
                newRow["Data"] = MainServices.GetKeteranganMedis(dataInput, "utama", umurTTU.Value, null);
                dtTTU.Rows.Add(newRow);

            }
            else
            {
                newRow = dtTTU.NewRow();
                newRow["DataType"] = App_Data.Text.Pekerjaan;
                newRow["Data"] = isEnglish ? pekerjaanTTU.RiskClassDesc_EN : pekerjaanTTU.RiskClassDesc;
                dtTTU.Rows.Add(newRow);

                if (!dataInput.Nasabah.NamaProduk.Equals("HLHEALTH"))
                {
                    newRow = dtTTU.NewRow();
                    newRow["DataType"] = App_Data.Text.KetMedis;
                    newRow["Data"] = MainServices.GetKeteranganMedis(dataInput, "utama", umurTTU.Value, null);
                    dtTTU.Rows.Add(newRow);
                }
            }

            if (!dataInput.Nasabah.NamaProduk.Equals("HSCI"))
            {
                newRow = dtTTU.NewRow();
                newRow["DataType"] = App_Data.Text.HubunganDgPH;
                newRow["Data"] = isEnglish ? obj.GetRelation(hubungan).RelationText_EN : obj.GetRelation(hubungan).RelationText_ID;
                dtTTU.Rows.Add(newRow);
            }
            else {
                if (dataInput.Nasabah.TertanggungUtama == "Ya")
                {
                    newRow = dtTTU.NewRow();
                    newRow["DataType"] = App_Data.Text.HubunganDgPH;
                    newRow["Data"] = App_Data.Text.DiriSendiri;
                    dtTTU.Rows.Add(newRow);
                }
                else
                {
                    newRow = dtTTU.NewRow();
                    newRow["DataType"] = App_Data.Text.HubunganDgPH;
                    newRow["Data"] = isEnglish ? obj.GetRelation(hubungan).RelationText_EN : obj.GetRelation(hubungan).RelationText_ID;
                    dtTTU.Rows.Add(newRow);
                }
            }
            return dtTTU;
        }
        public static DataTable DataTertanggungTambahan(SummaryViewModel dataInput, bool isEnglish, Product product)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtTT = new DataTable { TableName = "DataTertanggungTambahan" };
            dtTT.Columns.Add("No");
            dtTT.Columns.Add("Nama");
            dtTT.Columns.Add("Usia");
            dtTT.Columns.Add("Medis");
            dtTT.Columns.Add("JenisKelamin");
            dtTT.Columns.Add("Pekerjaan");
            dtTT.Columns.Add("Hubungan");
            dtTT.Columns.Add("NearestBirthday");

            var no = 1;

            if (dataInput.Additional.TertanggungTambahan != null)
            {
                if (dataInput.Additional.TertanggungTambahan.Count != 0)
                {
                    foreach (var ttg in dataInput.Additional.TertanggungTambahan)
                    {
                        var relation = obj.GetRelation(ttg.Relationship);
                        var pekerjaan = obj.GetRiskClass(Convert.ToInt32(ttg.KelasPekerjaan));
                        var riderAll = dataInput.Rider == null ? null : dataInput.Rider.Riders == null ? null : dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode.Equals("POP")).ToList();

                        var newRow = dtTT.NewRow();
                        newRow["No"] = "TT" + no;
                        newRow["Nama"] = ttg.Nama;
                        newRow["Usia"] = ttg.Age;
                        newRow["JenisKelamin"] = (ttg.Gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita); ;
                        newRow["Pekerjaan"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                        newRow["Hubungan"] = relation == null ? "" : isEnglish ? relation.RelationText_EN : relation.RelationText_ID;
                        newRow["Medis"] = MainServices.GetKeteranganMedis(dataInput, "tambahan", Convert.ToInt32(ttg.Age), no);

                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                        {
                            if (dataInput.Rider != null)
                            {
                                if (dataInput.Rider.Riders != null)
                                {
                                    if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                                    {
                                        newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                                    }
                                    else
                                    {
                                        newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                                    }
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                                }
                            }
                        }
                        else
                        {
                            if (dataInput.Rider != null)
                            {
                                if (dataInput.Rider.Riders != null)
                                {
                                    if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                                    {
                                        newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);

                                    }
                                    else
                                    {
                                        newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);

                                    }
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);

                                }
                            }
                        }
                        dtTT.Rows.Add(newRow);
                        no = no + 1;
                    }
                }
                else
                {
                    var newRow = dtTT.NewRow();
                    if (dataInput.Nasabah.TertanggungUtama == "Ya")
                    {
                        newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                    }
                    else
                    {
                        if (dataInput.Rider != null)
                        {
                            if (dataInput.Rider.Riders != null)
                            {
                                if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                                }
                                else
                                {
                                    newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                                }
                            }
                        }
                    }
                    dtTT.Rows.Add(newRow);
                }
            }
            else
            {
                var newRow = dtTT.NewRow();
                if (dataInput.Nasabah.TertanggungUtama == "Ya")
                {
                    newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                }
                else
                {
                    if (dataInput.Rider != null)
                    {
                        if (dataInput.Rider.Riders != null)
                        {
                            if (dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == "POP").Count() > 0)
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthday.ToString("dd/MM/yyyy", null);
                            }
                            else
                            {
                                newRow["NearestBirthday"] = dataInput.nearestBirthdayTTU.ToString("dd/MM/yyyy", null);
                            }
                        }
                    }
                }
                dtTT.Rows.Add(newRow);

            }
            return dtTT;
        }

        public static DataTable DataAnakNasabah(SummaryViewModel dataInput, bool isEnglish)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtAnakNasabah = new DataTable { TableName = "DataAnakNasabah" };
            dtAnakNasabah.Columns.Add("DataType");
            dtAnakNasabah.Columns.Add("Data");
            var hubungan = "Anak Kandung";

            var newRow = dtAnakNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.NamaAnak;
            newRow["Data"] = dataInput.Nasabah.NamaAnak;
            dtAnakNasabah.Rows.Add(newRow);

            newRow = dtAnakNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = dataInput.Nasabah.UmurAnak + App_Data.Text.TahunUmurSpasi + " / " + (dataInput.Nasabah.JenisKelaminAnak == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtAnakNasabah.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk == "HLKIDS")
            {
                newRow = dtAnakNasabah.NewRow();
                newRow["DataType"] = App_Data.Text.KetMedis;
                newRow["Data"] = MainServices.GetKeteranganMedis(dataInput, "anak", dataInput.Nasabah.UmurAnak, null);
                dtAnakNasabah.Rows.Add(newRow);
            }

            newRow = dtAnakNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.HubunganDgPH;
            newRow["Data"] = isEnglish ? obj.GetRelation(hubungan).RelationText_EN : obj.GetRelation(hubungan).RelationText_ID;
            dtAnakNasabah.Rows.Add(newRow);

            return dtAnakNasabah;
        }

        public static DataTable PremiData(SummaryViewModel dataInput, Product product, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            var dtPremi = new DataTable { TableName = "PremiData" };

            dtPremi.Columns.Add("DataType");
            dtPremi.Columns.Add("Data");

            var newRow = dtPremi.NewRow();

            string caraBayar, berkala;

            if (dataInput.Premi.CaraBayar == "1")
            {
                caraBayar = product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB" ? App_Data.Text.TahunanGolden : App_Data.Text.Tahunan;
                berkala = App_Data.Text.TahunSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "2")
            {
                caraBayar = App_Data.Text.Semesteran;
                berkala = App_Data.Text.SemeterSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "4")
            {
                caraBayar = App_Data.Text.Kuartalan;
                berkala = App_Data.Text.KuartalSpasi;
            }
            else
            {
                caraBayar = App_Data.Text.Bulanan;
                berkala = App_Data.Text.BulanSpasi;
            }



            if (product.ProductType.Equals("ul"))
            {

                newRow = dtPremi.NewRow();
                newRow["DataType"] = App_Data.Text.MataUang;
                newRow["Data"] = App_Data.Text.Rupiah;
                dtPremi.Rows.Add(newRow);

                var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);

                if (product.ProductCategory.Equals("single"))
                {
                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                    newRow["Data"] = App_Data.Text.Tunggal;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.CaraPembayaran;
                    newRow["Data"] = App_Data.Text.Tunggal;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiTunggal;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiTopUpSekaligus;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.TopupBerkala;
                    dtPremi.Rows.Add(newRow);
                }
                else
                {
                    TopUpWithdrawal firstTopup = new TopUpWithdrawal();

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                    newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunJamak;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.CaraPembayaran;
                    newRow["Data"] = caraBayar;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiBerkala;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency((CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala)) / Convert.ToInt32(dataInput.Premi.CaraBayar)) + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TopUpBerkala;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala) / Convert.ToInt32(dataInput.Premi.CaraBayar)) + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiAngsuran;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(totalPremi / Convert.ToInt32(dataInput.Premi.CaraBayar)) + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TotalPremiTahunan;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(totalPremi);
                    dtPremi.Rows.Add(newRow);

                    if (dataInput.TopUp.TopupWithdrawals != null)
                        firstTopup = dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp && x.Year == 1 && CalculatorServices.CurrencyToDecimal(x.Amount) > 0).FirstOrDefault();

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiTopupTglThnKe1;
                    newRow["Data"] = firstTopup == null ? "-" : dataInput.StrCurrency + firstTopup.Amount;
                    dtPremi.Rows.Add(newRow);
                }

                if (product.ProductCode.Equals("HLINVEST"))
                {
                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiBerkalaTahunPertama;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala;
                    dtPremi.Rows.Add(newRow);
                }
            }
            if (product.ProductType.Equals("tra"))
            {
                if (!product.ProductCode.Equals("HSCI"))
                {
                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.MataUang;
                    newRow["Data"] = App_Data.Text.Rupiah;
                    dtPremi.Rows.Add(newRow);

                    if (product.ProductCategory.Equals("education") && dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                    {
                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                        newRow["Data"] = App_Data.Text.Sekaligus;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.CaraPembayaran;
                        newRow["Data"] = App_Data.Text.Sekaligus;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.PremiAsuransiDasar;
                        newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.PremiAsuransiTambahan;
                        newRow["Data"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan;
                        dtPremi.Rows.Add(newRow);

                        var total = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.TotalPremi;
                        newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total);
                        dtPremi.Rows.Add(newRow);
                    }
                    else
                    {
                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                        newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunJamak;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.CaraPembayaran;
                        newRow["Data"] = caraBayar;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        var premidasar = product.ProductCode.Equals("HLLIFE") ? dataInput.Insurance.BiayaAsuransiTotal : dataInput.Premi.PremiBerkala;
                        newRow["DataType"] = product.ProductCode.Equals("HLGOLDENA") || product.ProductCode.Equals("HLGOLDENB") ? App_Data.Text.PremiAsuransi : App_Data.Text.PremiAsuransiDasar;
                        newRow["Data"] = dataInput.StrCurrency + premidasar + " /" + berkala;
                        dtPremi.Rows.Add(newRow);

                        if (product.ProductCategory.Equals("education"))
                        {
                            newRow = dtPremi.NewRow();
                            newRow["DataType"] = App_Data.Text.PremiAsuransiTambahan;
                            newRow["Data"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan + " /" + berkala;
                            dtPremi.Rows.Add(newRow);
                            var total = CalculatorServices.CurrencyToDecimal(premidasar) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);

                            newRow = dtPremi.NewRow();
                            newRow["DataType"] = App_Data.Text.TotalPremi;
                            newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) + " /" + berkala;
                            dtPremi.Rows.Add(newRow);
                        }
                    }
                    //--alif
                    if (product.ProductCode.Equals("HLSAVING"))
                    {
                        decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
                        //PaymentMethod Factor = new PaymentMethod();
                        var Factor = PaymentMethodList.FirstOrDefault(x => x.PMCode.ToString() == dataInput.Premi.CaraBayar && x.PM_ProductCode == product.ProductCode);
                        var bagi = Convert.ToDecimal(Factor.Factor);
                        var PremiTahunanawal = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
                        var PremiTahunan = PremiTahunanawal / (bagi / 100);
                        //decimal? premitahunan = CalculatorServices.DecimalToCurrency(PremiTahunanawal);
                        //CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala), -1);
                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = App_Data.Text.PremiTahunanSaving;
                        newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(PremiTahunan);
                        dtPremi.Rows.Add(newRow);
                    }
                }
                else
                {
                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "A." + "  " + App_Data.Text.AsuransiDasar;
                    newRow["Data"] = product.ProductName;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.JenisAsuransi;
                    newRow["Data"] = App_Data.Text.Kesehatan;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.MataUang;
                    newRow["Data"] = App_Data.Text.Rupiah;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.UangPertanggungan;
                    newRow["Data"] = "Rp" + dataInput.Premi.UangPertanggungan;
                    dtPremi.Rows.Add(newRow);

                    //masa asuransi
                    int masa = 0;
                    if (dataInput.Nasabah.TertanggungUtama == "Ya")
                    {
                        masa = 80 - Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                    }
                    else
                    {
                        masa = 80 - Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);
                    }

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.MasaAsuransi;
                    newRow["Data"] = masa + " " + App_Data.Text.TahunJamak;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.MasaPembayaranPremi;
                    newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + " " + App_Data.Text.TahunJamak;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.CaraPembayaran2;
                    newRow["Data"] = caraBayar;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "      " + App_Data.Text.PremiAsuransiBintang;
                    newRow["Data"] = "Rp" + dataInput.Premi.PremiBerkala + " / " + berkala;
                    dtPremi.Rows.Add(newRow);

                    var rider = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Count();

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "B." + "  " + App_Data.Text.AsuransiTambahan;
                    newRow["Data"] = rider == 0 ? App_Data.Text.TidakAda : "(Rider) Hanwha Waiver of Premium CI";
                    dtPremi.Rows.Add(newRow);

                    if (rider != 0)
                    {
                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.JenisAsuransi;
                        newRow["Data"] = App_Data.Text.Kesehatan;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.MataUang;
                        newRow["Data"] = App_Data.Text.Rupiah;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.UangPertanggungan;
                        newRow["Data"] = App_Data.Text.PembebasanPremiLanjutanAsuransiDasar;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.MasaAsuransi;
                        newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + " " + App_Data.Text.TahunJamak;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.MasaPembayaranPremi;
                        newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + " " + App_Data.Text.TahunJamak;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.Cara_Pembayaran;
                        newRow["Data"] = caraBayar;
                        dtPremi.Rows.Add(newRow);

                        newRow = dtPremi.NewRow();
                        newRow["DataType"] = "      " + App_Data.Text.PremiAsuransiBintang;
                        newRow["Data"] = "Rp" + dataInput.Insurance.BiayaAsuransiTambahan + " / " + berkala;
                        dtPremi.Rows.Add(newRow);
                    }
                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = "C. " + App_Data.Text.TotalPremi;
                    newRow["Data"] = "Rp" + dataInput.Insurance.BiayaAsuransiTotal + " / " + berkala;
                    dtPremi.Rows.Add(newRow);

                }
                
            }
            if (product.ProductType.Equals("kes"))
            {

                newRow = dtPremi.NewRow();
                newRow["DataType"] = App_Data.Text.PilihanPlan;
                newRow["Data"] = "";
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = "        " + App_Data.Text.PlanRawatInap;
                newRow["Data"] = "Plan " + dataInput.Premi.JenisPlan;
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = "        " + App_Data.Text.PlanRawatJalan;
                if (dataInput.Premi.JenisPlan == "2000" && dataInput.Premi.ManfaatRawatJalan == "Outpatient")
                    newRow["Data"] = "Plan 10000";
                else if (dataInput.Premi.JenisPlan == "5000" && dataInput.Premi.ManfaatRawatJalan == "Outpatient")
                    newRow["Data"] = "Plan 20000";
                else
                    newRow["Data"] = "-";
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = App_Data.Text.MasaAsuransi;
                newRow["Data"] = "1 " + App_Data.Text.TahunJamak + "*";
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = App_Data.Text.CaraPembayaran;
                newRow["Data"] = caraBayar;
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = App_Data.Text.Premi;
                newRow["Data"] = "";
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = "        " + App_Data.Text.RawatInap;
                newRow["Data"] = dataInput.Premi.PremiInpatient + " / " + berkala;
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = "        " + App_Data.Text.RawatJalan;
                newRow["Data"] = dataInput.Premi.PremiOutpatient + " / " + berkala;
                dtPremi.Rows.Add(newRow);

                newRow = dtPremi.NewRow();
                newRow["DataType"] = "        " + App_Data.Text.TotalPremi;
                newRow["Data"] = dataInput.Premi.PremiBerkala + " / " + berkala;
                dtPremi.Rows.Add(newRow);
            }
            return dtPremi;
        }

        public static DataTable PremiDataKesehatan(SummaryViewModel dataInput, Product product, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            var dtPremi = new DataTable { TableName = "PremiDataKesehatan" };
            dtPremi.Columns.Add("DataType");
            dtPremi.Columns.Add("Data");

            var newRow = dtPremi.NewRow();

            string caraBayar, berkala;

            if (dataInput.Premi.CaraBayar == "1")
            {
                caraBayar = product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB" ? App_Data.Text.TahunanGolden : App_Data.Text.Tahunan;
                berkala = App_Data.Text.TahunSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "2")
            {
                caraBayar = App_Data.Text.Semesteran;
                berkala = App_Data.Text.SemeterSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "4")
            {
                caraBayar = App_Data.Text.Kuartalan;
                berkala = App_Data.Text.KuartalSpasi;
            }
            else
            {
                caraBayar = App_Data.Text.Bulanan;
                berkala = App_Data.Text.BulanSpasi;
            }

            //newRow = dtPremi.NewRow();
            //newRow["DataType"] = App_Data.Text.MataUang;
            //newRow["Data"] = App_Data.Text.Rupiah;
            //dtPremi.Rows.Add(newRow);

            //newRow = dtPremi.NewRow();
            //newRow["DataType"] = App_Data.Text.CaraPembayaran;
            //newRow["Data"] = App_Data.Text.Sekaligus;
            //dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.PlanRawatInap;
            newRow["Data"] = "Plan " + dataInput.Premi.JenisPlan;
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.PlanRawatJalan;
            if (dataInput.Premi.JenisPlan == "2000")
                newRow["Data"] = "Plan 10000";
            else if (dataInput.Premi.JenisPlan == "5000")
                newRow["Data"] = "Plan 20000";
            else
                newRow["Data"] = "-";
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.MasaAsuransi;
            newRow["Data"] = "1 " + App_Data.Text.TahunJamak;
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.CaraPembayaran;
            newRow["Data"] = caraBayar;
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.RawatInap;
            newRow["Data"] = dataInput.Premi.PremiInpatient + " / " + berkala;
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.RawatJalan;
            newRow["Data"] = dataInput.Premi.PremiOutpatient + " / " + berkala;
            dtPremi.Rows.Add(newRow);

            newRow = dtPremi.NewRow();
            newRow["DataType"] = App_Data.Text.TotalPremi;
            newRow["Data"] = dataInput.Premi.PremiBerkala + " / " + berkala;
            dtPremi.Rows.Add(newRow);

            return dtPremi;
        }

        public static DataTable FundData(SummaryViewModel dataInput)
        {
            var dtFund = new DataTable { TableName = "FundData" };
            dtFund.Columns.AddRange(new[] { new DataColumn("Name"), new DataColumn("Percentage") });

            foreach (var t in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
            {
                var fund = dtFund.NewRow();
                fund["Name"] = t.InvestmentName;
                fund["Percentage"] = t.Percentage + "%";
                dtFund.Rows.Add(fund);
            }

            return dtFund;
        }

        public static DataTable RincianAsuransiDasar(SummaryViewModel dataInput, Product product)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtAsuransi = new DataTable { TableName = "RincianAsuransiDasar" };

            dtAsuransi.Columns.Add("NamaProduk");
            dtAsuransi.Columns.Add("MasaPertanggungan");
            dtAsuransi.Columns.Add("UangPertanggungan");
            dtAsuransi.Columns.Add("BiayaAsuransi");

            var insAge = 0;
            var period = product.ProductCode == "HLFUTURE" || product.ProductCode == "HLHEALTH" || product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB" ? dataInput.Premi.RencanaMasaPembayaran : dataInput.Premi.MasaAsuransi;
            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var newRow = dtAsuransi.NewRow();
            newRow["NamaProduk"] = product.ProductName;
            if (product.ProductCode == "HLSAVING")
            {
                newRow["MasaPertanggungan"] = (insAge + 7);
            }
            else if (product.ProductCode == "HLGOLDENA" || product.ProductCode == "HLGOLDENB")
            {
                newRow["MasaPertanggungan"] = 100;
            }
            else
            {
                newRow["MasaPertanggungan"] = product.ProductType.Equals("tra") ? (period + insAge) : product.CovAge / 12;
            }

            newRow["UangPertanggungan"] = product.ProductCode == "HLHEALTH" ? obj.GetProductPlanbyProductCode(product.ProductCode, dataInput.Premi.JenisPlan).PlanDesc : dataInput.Premi.UangPertanggungan;


            if (product.ProductType.Equals("ul"))
                newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi) / 12, 0));
            else if (product.ProductCode.Equals("HLLIFE"))
                newRow["BiayaAsuransi"] = dataInput.Insurance.BiayaAsuransiTotal;
            else
                newRow["BiayaAsuransi"] = dataInput.Premi.PremiBerkala;

            dtAsuransi.Rows.Add(newRow);

            return dtAsuransi;
        }

        public static DataTable RiderDesc(SummaryViewModel dataInput, List<Rider> riderList, List<Rider> riderListAddins, bool isEnglish, Product product)
        {
            var dtRider = new DataTable { TableName = "RiderDesc" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("RiderDesc");

            if (riderList != null)
            {
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                    if (riderItem.Rider.Category.Equals("Choices"))
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                            {
                                var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                var newRow = dtRider.NewRow();
                                newRow["RiderName"] = rider.RiderName + " - " + riderType.TypeName;

                                if (product.ProductType.Equals("tra"))
                                {
                                    newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                }
                                else if (product.ProductCode.Equals("HLWIZ"))
                                {
                                    newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                }
                                else
                                {
                                    newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN : riderType.Description;
                                }

                                dtRider.Rows.Add(newRow);
                            }
                        }
                    }
                    else
                    {
                        var newRow = dtRider.NewRow();
                        newRow["RiderName"] = rider.RiderName;

                        if (product.ProductType.Equals("tra"))
                        {
                            newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN.Replace("70 y", "65 y") : rider.Description.Replace("70 t", "65 t");
                        }
                        else if (product.ProductCode.Equals("HLWIZ"))
                        {
                            newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN.Replace("70 y", "65 y") : rider.Description.Replace("70 t", "65 t");
                        }
                        //else if (product.ProductCode.Equals("HLPREF") && rider.RiderCode == "HHP")
                        //{
                        //    newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN.Replace("75 y", "70 y") : rider.Description.Replace("75 t", "70 t");
                        //}
                        else
                        {
                            newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN : rider.Description;
                        }

                        dtRider.Rows.Add(newRow);
                    }
                }
            }


            if (riderListAddins != null)
            {
                foreach (var riderItem in dataInput.Additional.Riders.Where(x => x.Checked || (x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0) || x.Rider.Category.Equals("Choices")))
                {
                    var rider = riderListAddins.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                    if (dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == riderItem.Rider.RiderCode).Count() <= 0)
                    {
                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                            {
                                foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = rider.RiderName + " - " + riderType.TypeName;

                                    if (product.ProductType.Equals("tra"))
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                    }
                                    else if (product.ProductCode.Equals("HLWIZ"))
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                    }
                                    else
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN : riderType.Description;
                                    }

                                    dtRider.Rows.Add(newRow);
                                }
                            }
                        }
                    }
                }
            }
            return dtRider;
        }

        public static DataTable RiderWithDesc(SummaryViewModel dataInput, List<Rider> riderList)
        {
            var dtRiderWithDesc = new DataTable { TableName = "RiderWithDesc" };
            var riderAll = dataInput.Rider == null ? null : dataInput.Rider.Riders == null ? null : dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).ToList();
            var addIns = dataInput.Additional == null ? null : dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || x.Rider.Category.Equals("Choices")).ToList();

            if (addIns != null)
                riderAll.AddRange(addIns);

            if (riderAll != null)
            {
                foreach (var riderItem in riderAll)
                {
                    var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                    {
                        foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                        {
                            var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                            if (riderType.TypeName.Equals("CI"))
                            {
                                if (dtRiderWithDesc.Rows.Count > 0)
                                {
                                    dtRiderWithDesc.Rows[0][riderType.TypeName] = riderType.TypeName;
                                }
                                else
                                {
                                    dtRiderWithDesc.Columns.Add("CI");
                                    dtRiderWithDesc.Columns.Add("CI Accelerated");
                                    dtRiderWithDesc.Columns.Add("HCP");
                                    dtRiderWithDesc.Columns.Add("AHCP");
                                    dtRiderWithDesc.Columns.Add("Medi Guard");
                                    dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                                    var row = dtRiderWithDesc.NewRow();
                                    row["CI"] = "NULL";
                                    row["CI Accelerated"] = "NULL";
                                    row["HCP"] = "NULL";
                                    row["AHCP"] = "NULL";
                                    row["Medi Guard"] = "NULL";
                                    row["Hanwha Health Protection"] = "NULL";
                                    row[riderType.TypeName] = riderType.TypeName;
                                    dtRiderWithDesc.Rows.Add(row);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (rider.RiderName.Equals("CI") || rider.RiderName.Equals("CI Accelerated") || rider.RiderName.Equals("HCP") || rider.RiderName.Equals("AHCP") || rider.RiderName.Equals("Medi Guard") || rider.RiderName.Equals("Hanwha Health Protection"))
                        {
                            var riderChoice = rider.RiderCode == "HHP" ? rider.RiderTypes.Where(x => x.RiderTypeId.ToString() == riderItem.Choice).FirstOrDefault().TypeName : rider.RiderName;
                            ;
                            if (dtRiderWithDesc.Rows.Count > 0)
                            {
                                dtRiderWithDesc.Rows[0][rider.RiderName] = rider.RiderCode == "HHP" ? riderChoice : rider.RiderName;
                            }
                            else
                            {
                                dtRiderWithDesc.Columns.Add("CI");
                                dtRiderWithDesc.Columns.Add("CI Accelerated");
                                dtRiderWithDesc.Columns.Add("HCP");
                                dtRiderWithDesc.Columns.Add("AHCP");
                                dtRiderWithDesc.Columns.Add("Medi Guard");
                                dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                                var row = dtRiderWithDesc.NewRow();
                                row["CI"] = "NULL";
                                row["CI Accelerated"] = "NULL";
                                row["HCP"] = "NULL";
                                row["AHCP"] = "NULL";
                                row["Medi Guard"] = "NULL";
                                row["Hanwha Health Protection"] = "NULL";
                                row[rider.RiderName] = riderChoice;
                                dtRiderWithDesc.Rows.Add(row);
                            }
                        }
                    }
                }
            }

            if (dtRiderWithDesc.Rows.Count < 1)
            {
                dtRiderWithDesc.Columns.Add("CI");
                dtRiderWithDesc.Columns.Add("CI Accelerated");
                dtRiderWithDesc.Columns.Add("HCP");
                dtRiderWithDesc.Columns.Add("AHCP");
                dtRiderWithDesc.Columns.Add("Medi Guard");
                dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                var row = dtRiderWithDesc.NewRow();
                row["CI"] = "NULL";
                row["CI Accelerated"] = "NULL";
                row["HCP"] = "NULL";
                row["AHCP"] = "NULL";
                row["Medi Guard"] = "NULL";
                row["Hanwha Health Protection"] = "NULL";
                dtRiderWithDesc.Rows.Add(row);
            }

            return dtRiderWithDesc;
        }

        public static DataTable RincianRider(SummaryViewModel dataInput, List<Rider> riderList, Product product)
        {
            var dtRider = new DataTable { TableName = "RincianRider" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("MasaPertanggungan");
            dtRider.Columns.Add("UangPertanggungan");
            dtRider.Columns.Add("BiayaAsuransi");

            if (riderList != null)
            {
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode != "POP"))
                {
                    var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                    // Traditional cov term 65 year
                    if (product.ProductType.Equals("tra"))
                        rider.CoverTerm = 780;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                    else if (product.ProductCode.Equals("HLPREF"))
                    {
                        rider.CoverTerm = riderItem.Rider.CoverTermPreferred;
                        rider.RiderTypes = rider.RiderTypes.Select(x => { x.CoverTerm = riderItem.Rider.CoverTermPreferred; return x; }).ToList();
                    }

                    if (riderItem.Rider.Category.Equals("Choices"))
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                            {
                                var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                var newRow = dtRider.NewRow();
                                newRow["RiderName"] = riderItem.Rider.RiderName + " - " + riderType.TypeName;
                                newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                                newRow["UangPertanggungan"] = "";
                                decimal biayaAsuransi;

                                if (product.ProductType.Equals("tra"))
                                {
                                    biayaAsuransi = MainServices.CalculateTraditionalCOR(riderItem.UangPertanggungan, riderItem.Rider.RiderCode, riderType.RiderTypeId, dataInput.Premi.ModeBayarPremi, dataInput.Premi.RencanaMasaPembayaran, riderItem.Rider.Category);
                                    //biayaAsuransi = CalculatorServices.Round(biayaAsuransi * Convert.ToDecimal((MainServices.GetFactorPaymentMethod(Convert.ToInt32(dataInput.Premi.CaraBayar)) / 100)), -1);
                                }
                                else
                                {
                                    int age = 0;

                                    if (riderItem.Rider.RiderCode.Equals("POP"))
                                        age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                                    else
                                    {
                                        if (dataInput.Nasabah.TertanggungUtama.Equals("Ya"))
                                            age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                                        else
                                            age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);
                                    }

                                    biayaAsuransi = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan), riderItem.Rider.RiderCode, riderType.RiderTypeId, age, null, riderItem.Rider.Category, null);
                                }

                                newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                                dtRider.Rows.Add(newRow);
                            }
                        }
                    }
                    else if (riderItem.Rider.Category.Equals("Unit"))
                    {
                        if (!String.IsNullOrEmpty(riderItem.UnitName))
                        {
                            var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.UnitName));

                            var newRow = dtRider.NewRow();
                            newRow["RiderName"] = riderItem.Unit + " X " + riderType.TypeName;
                            newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                            newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                            newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                            dtRider.Rows.Add(newRow);
                        }
                    }
                    else if (riderItem.Rider.Category.Equals("Choice"))
                    {
                        if (!String.IsNullOrEmpty(riderItem.Choice))
                        {
                            var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.Choice));
                            var riderName = riderItem.Rider.RiderCode == "HHP" ? riderItem.Rider.RiderCode : riderItem.Rider.RiderName;

                            var newRow = dtRider.NewRow();
                            newRow["RiderName"] = riderName + " - " + riderType.TypeName;
                            newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                            newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                            newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                            dtRider.Rows.Add(newRow);
                        }
                    }
                    else
                    {
                        var newRow = dtRider.NewRow();
                        newRow["RiderName"] = riderItem.Rider.RiderName;
                        newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                        newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                        newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                        dtRider.Rows.Add(newRow);
                    }
                }
            }

            return dtRider;
        }

        public static DataTable RincianRiderTambahan(SummaryViewModel dataInput, List<Rider> riderList, Product product)
        {
            var dtRider = new DataTable { TableName = "RincianRiderTambahan" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("MasaPertanggungan");
            dtRider.Columns.Add("UangPertanggungan");
            dtRider.Columns.Add("BiayaAsuransi");

            if (dataInput.Additional.RidersTertanggungUtama != null)
            {
                foreach (var dataPop in dataInput.Additional.RidersTertanggungUtama.Where(x => x.Rider.RiderCode == "POP"))
                {
                    if (dataPop.Choices != null && dataPop.Choices.Count > 0)
                    {
                        foreach (var riderTypeItem in dataPop.Choices.Where(x => x.Checked))
                        {
                            //var riderType = dataPop.Rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);
                            var riderType = dataPop.Choices.FirstOrDefault(x => x.RiderType.RiderTypeId == riderTypeItem.RiderType.RiderTypeId).RiderType;

                            var newRow = dtRider.NewRow();
                            newRow["RiderName"] = dataPop.Rider.RiderName + " - " + riderType.TypeName;
                            newRow["MasaPertanggungan"] = dataPop.Rider.CoverTerm / 12;
                            newRow["UangPertanggungan"] = "";

                            decimal biayaAsuransi;
                            int age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis); ;

                            biayaAsuransi = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(dataPop.UangPertanggungan), dataPop.Rider.RiderCode, riderType.RiderTypeId, age, null, dataPop.Rider.Category, null);

                            newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                            dtRider.Rows.Add(newRow);
                        }
                    }
                }
            }

            if (dataInput.Additional.TertanggungTambahan != null)
            {
                for (var i = 0; i < dataInput.Additional.TertanggungTambahan.Count; i++)
                {
                    for (int y = i; y < dataInput.Additional.Riders.Count;)
                    {
                        var riderItem = dataInput.Additional.Riders[y];
                        var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                        if (riderItem.Checked || CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0 || riderItem.Rider.Category.Equals("Choices"))
                        {

                            if (product.ProductCode.Equals("HLPREF"))
                            {
                                rider.CoverTerm = riderItem.Rider.CoverTermPreferred;
                                rider.RiderTypes = rider.RiderTypes.Select(x => { x.CoverTerm = riderItem.Rider.CoverTermPreferred; return x; }).ToList();
                            }

                            if (riderItem.Rider.Category.Equals("Choices"))
                            {
                                if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                {
                                    foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                                    {
                                        var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                        var newRow = dtRider.NewRow();
                                        newRow["RiderName"] = riderItem.Rider.RiderName + " - " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                        newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                                        newRow["UangPertanggungan"] = "";

                                        decimal biayaAsuransi;

                                        if (product.ProductType.Equals("tra"))
                                        {
                                            biayaAsuransi = MainServices.CalculateTraditionalCOR(riderItem.UangPertanggungan, riderItem.Rider.RiderCode, riderType.RiderTypeId, dataInput.Premi.ModeBayarPremi, dataInput.Premi.RencanaMasaPembayaran, riderItem.Rider.Category);
                                        }
                                        else
                                        {
                                            int age = (int)dataInput.Additional.TertanggungTambahan[i].Age;

                                            biayaAsuransi = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan), riderItem.Rider.RiderCode, riderType.RiderTypeId, age, null, riderItem.Rider.Category, null);
                                        }

                                        newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                                        dtRider.Rows.Add(newRow);
                                    }
                                }
                            }
                            else if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                if (!String.IsNullOrEmpty(riderItem.UnitName))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.UnitName));

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = riderItem.Unit + " X " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                    newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                                    newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                    newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                    dtRider.Rows.Add(newRow);
                                }
                            }
                            else if (riderItem.Rider.Category.Equals("Choice"))
                            {
                                if (!String.IsNullOrEmpty(riderItem.Choice))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.Choice));
                                    var riderName = riderItem.Rider.RiderCode == "HHP" ? riderItem.Rider.RiderCode : riderItem.Rider.RiderName;

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = riderName + " - " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                    newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                                    newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                    newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                    dtRider.Rows.Add(newRow);
                                }
                            }
                            else
                            {
                                var newRow = dtRider.NewRow();
                                newRow["RiderName"] = riderItem.Rider.RiderName + " (TT" + (i + 1) + ")";
                                newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                                newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                dtRider.Rows.Add(newRow);
                            }
                        }

                        if (dataInput.Additional.TertanggungTambahan.Count > 1)
                            y = y + dataInput.Additional.TertanggungTambahan.Count;
                        else
                            y++;
                    }
                }
            }

            dtRider.DefaultView.Sort = "RiderName";

            return dtRider;
        }

        public static DataTable FundInvestment(SummaryViewModel dataInput, List<Fund> fundList)
        {
            var dtFund = new DataTable { TableName = "FundInvestment" };

            dtFund.Columns.Add("FundName");
            dtFund.Columns.Add("Komposisi");
            dtFund.Columns.Add("Rendah");
            dtFund.Columns.Add("Sedang");
            dtFund.Columns.Add("Tinggi");

            foreach (var fundItem in dataInput.Premi.Investments.Where(x => x.Percentage != 0))
            {
                var fund = fundList.FirstOrDefault(x => x.FundCode == fundItem.InvestmentCode);

                var newRow = dtFund.NewRow();
                newRow["FundName"] = fund.FundName;
                newRow["Komposisi"] = fundItem.Percentage + "%";
                newRow["Rendah"] = fund.LowRate + "%";
                newRow["Sedang"] = fund.MediumRate + "%";
                newRow["Tinggi"] = fund.HighRate + "%";
                dtFund.Rows.Add(newRow);
            }

            return dtFund;
        }

        public static DataTable AllocationRate(List<ProductAllocationRate> productAllocationRates, Product product, bool isEnglish)
        {
            var dtAllocation = new DataTable { TableName = "AllocationRate" };

            dtAllocation.Columns.Add("Tahun");
            dtAllocation.Columns.Add("PremiInvestasi");
            dtAllocation.Columns.Add("PremiAkuisisi");
            dtAllocation.Columns.Add("TopUpInvestasi");
            dtAllocation.Columns.Add("TopUpAkuisisi");
            dtAllocation.Columns.Add("TopUpTunggalInvestasi");
            double? ratePremi = 0;
            double? rateTopup = 0;
            var lastYear = productAllocationRates.Max(x => x.Year).Value;

            //var year = product.ProductCategory == "preferred" ? 2 : 6;
            foreach (var rate in productAllocationRates)
            {
                if ((rate.RatePremi != ratePremi) || (rate.RateSingleTopUp != rateTopup))
                {
                    var newRow = dtAllocation.NewRow();

                    if (rate.Year == lastYear)
                        newRow["Tahun"] = rate.Year + (isEnglish ? " onward" : " dst");
                    else
                        newRow["Tahun"] = rate.Year;

                    newRow["PremiInvestasi"] = rate.RatePremi + "%";
                    newRow["PremiAkuisisi"] = (100 - rate.RatePremi) < 0 ? "0%" : (100 - rate.RatePremi) + "%";
                    newRow["TopUpInvestasi"] = product.RegularTopUpRate + "%";
                    newRow["TopUpAkuisisi"] = (100 - product.RegularTopUpRate) + "%";
                    newRow["TopUpTunggalInvestasi"] = rate.RateSingleTopUp + "%";

                    dtAllocation.Rows.Add(newRow);

                    ratePremi = rate.RatePremi;
                    rateTopup = rate.RateSingleTopUp;
                }
                else
                {
                    var drLast = dtAllocation.Rows[dtAllocation.Rows.Count - 1]["Tahun"].ToString().Split(new[] { " - " }, StringSplitOptions.None);
                    dtAllocation.Rows[dtAllocation.Rows.Count - 1]["Tahun"] = rate.Year == lastYear ? drLast[0] + (isEnglish ? " onward" : " dst") : drLast[0] + " - " + rate.Year;
                }
            }

            return dtAllocation;
        }

        public static DataTable AllocationRateSingle(List<ProductAllocationRate> productAllocationRates, Product product)
        {
            var dtAllocation = new DataTable { TableName = "AllocationRateSingle" };

            dtAllocation.Columns.Add("Premi");
            dtAllocation.Columns.Add("Investasi");
            dtAllocation.Columns.Add("Akuisisi");

            var rate = productAllocationRates.FirstOrDefault();

            var newRow = dtAllocation.NewRow();
            newRow["Premi"] = App_Data.Text.PremiTunggal;
            newRow["Investasi"] = rate.RatePremi + "%";
            newRow["Akuisisi"] = (100 - rate.RatePremi) + "%";
            dtAllocation.Rows.Add(newRow);

            newRow = dtAllocation.NewRow();
            newRow["Premi"] = App_Data.Text.PremiTopUpTunggal;
            newRow["Investasi"] = rate.RateSingleTopUp + "%";
            newRow["Akuisisi"] = (100 - rate.RateSingleTopUp) + "%";
            dtAllocation.Rows.Add(newRow);

            return dtAllocation;
        }

        public static DataTable DataInput1(SummaryViewModel dataInput, bool isEnglish)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var dtData = new DataTable { TableName = "DataInput1" };
            dtData.Columns.Add("DataType");
            dtData.Columns.Add("Data");

            string insName;
            int? age;
            string gender;
            string relation = "";
            RiskClass pekerjaan;
            string dobTTU;
            int? umurTTU;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
            {
                insName = dataInput.Nasabah.NamaPemegangPolis;
                age = dataInput.Nasabah.UmurPemegangPolis;
                gender = dataInput.Nasabah.JenisKelaminPemegangPolis;
                pekerjaan = obj.GetRiskClass(Int32.Parse(dataInput.Nasabah.StatusPekerjaanPemegangPolis));
                dobTTU = dataInput.Nasabah.TanggalLahirPemegangPolis;
            }
            else
            {
                insName = dataInput.Nasabah.NamaTertanggungUtama;
                age = dataInput.Nasabah.UmurTertanggungUtama;
                gender = dataInput.Nasabah.JenisKelaminTertanggungUtama;
                relation = dataInput.Nasabah.Relation;
                pekerjaan = obj.GetRiskClass(Int32.Parse(dataInput.Nasabah.StatusPekerjaanTertanggungUtama));
                dobTTU = dataInput.Nasabah.TanggalLahirTertanggungUtama;
            }

            var newRow = dtData.NewRow();
            newRow["DataType"] = dataInput.Nasabah.NamaProduk == "HLKIDS" ? App_Data.Text.NamaTertanggungOrtu : dataInput.Nasabah.NamaProduk == "HLLIFE" ? App_Data.Text.NamaCalonTertanggungUtama : App_Data.Text.NamaTertanggung;
            newRow["Data"] = insName;
            dtData.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk == "HLFUTURE" || dataInput.Nasabah.NamaProduk == "HLSAVING")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.TanggalLahir + " / " + App_Data.Text.Usia;
                newRow["Data"] = DateTime.ParseExact(dobTTU, "dd/MM/yyyy", null).ToString("dd MMM yyyy") + " / " + age + App_Data.Text.TahunUmurSpasi;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.JenisKelamin;
                newRow["Data"] = (gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtData.Rows.Add(newRow);
                if (dataInput.Nasabah.NamaProduk == "HLSAVING")
                {
                    newRow = dtData.NewRow();
                    newRow["DataType"] = App_Data.Text.PekerjaanGolden;
                    newRow["Data"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                    dtData.Rows.Add(newRow);
                }
                else
                {
                    newRow = dtData.NewRow();
                    newRow["DataType"] = App_Data.Text.Pekerjaan;
                    newRow["Data"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                    dtData.Rows.Add(newRow);
                }
            }
            else if (dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLGOLDENB")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
                newRow["Data"] = age + App_Data.Text.TahunUmurSpasi + " / " + (gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.PekerjaanGolden;
                newRow["Data"] = isEnglish ? pekerjaan.RiskClassDesc_EN : pekerjaan.RiskClassDesc;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.KetMedisGolden;
                newRow["Data"] = MainServices.GetKeteranganMedis(dataInput, "utama", age.Value, null);
                dtData.Rows.Add(newRow);

            }
            else
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
                newRow["Data"] = age + App_Data.Text.TahunUmurSpasi + " / " + (gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtData.Rows.Add(newRow);
            }

            if (dataInput.Nasabah.NamaProduk == "HLLIFE")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.NamaCalonPemegangPolis;
                newRow["Data"] = dataInput.Nasabah.NamaPemegangPolis;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
                newRow["Data"] = dataInput.Nasabah.UmurPemegangPolis + App_Data.Text.TahunUmurSpasi + " / " + (dataInput.Nasabah.JenisKelaminPemegangPolis == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtData.Rows.Add(newRow);
            }
            newRow = dtData.NewRow();
            newRow["DataType"] = dataInput.Nasabah.NamaProduk == "HLLIFE" ? App_Data.Text.HubunganDgTTU : App_Data.Text.Hubungan;
            newRow["Data"] = dataInput.Nasabah.NamaProduk == "HLLIFE" ? MainServices.GetRelationshipText(dataInput.Premi.additionalInsured.TertanggungTambahan[0].Relationship) : MainServices.GetRelationshipText(relation);
            dtData.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk != "HLGOLDENA" && dataInput.Nasabah.NamaProduk != "HLGOLDENB" && dataInput.Nasabah.NamaProduk != "HLSIN" && dataInput.Nasabah.NamaProduk != "HLEDU" && dataInput.Nasabah.NamaProduk != "HLLIFE" && dataInput.Nasabah.NamaProduk != "HLFUTURE" && dataInput.Nasabah.NamaProduk != "HLSAVING")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunJamak;
                dtData.Rows.Add(newRow);
            }

            if (dataInput.Nasabah.NamaProduk == "HLSIN" || dataInput.Nasabah.NamaProduk == "HLEDU")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                {
                    newRow["Data"] = App_Data.Text.Sekaligus;
                }
                else
                {
                    var payTerm = dataInput.Premi.PilihanMasaPembayaran == "18" ? (Convert.ToInt32(dataInput.Premi.PilihanMasaPembayaran) - dataInput.Nasabah.UmurAnak).ToString() : dataInput.Premi.PilihanMasaPembayaran;
                    newRow["Data"] = payTerm + App_Data.Text.TahunJamak;
                }
                dtData.Rows.Add(newRow);
            }

            if (dataInput.Nasabah.NamaProduk != "HLGOLDENA" && dataInput.Nasabah.NamaProduk != "HLGOLDENB" && dataInput.Nasabah.NamaProduk != "HLLIFE" && dataInput.Nasabah.NamaProduk != "HLFUTURE" && dataInput.Nasabah.NamaProduk != "HLSAVING")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = dataInput.Nasabah.NamaProduk == "HLKIDS" ? App_Data.Text.NamaTertanggungAnak : App_Data.Text.NamaAnak;
                newRow["Data"] = dataInput.Nasabah.NamaAnak;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
                newRow["Data"] = dataInput.Nasabah.UmurAnak + App_Data.Text.TahunUmurSpasi + " / " + (dataInput.Nasabah.JenisKelaminAnak == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
                dtData.Rows.Add(newRow);
            }

            if (dataInput.Nasabah.NamaProduk == "HLLIFE")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.UangPertanggunganAwal;
                newRow["Data"] = dataInput.Premi.UangPertanggungan;
                dtData.Rows.Add(newRow);

            }

            return dtData;
        }

        public static DataTable DataInput2(SummaryViewModel dataInput)
        {
            var dtData = new DataTable { TableName = "DataInput2" };
            dtData.Columns.Add("Caption");
            dtData.Columns.Add("Value");
            var premi = dataInput.Nasabah.NamaProduk == "HLLIFE" ? dataInput.Insurance.BiayaAsuransiTotal : dataInput.Premi.PremiBerkala;

            var newRow = dtData.NewRow();
            newRow["Caption"] = App_Data.Text.MataUang;
            newRow["Value"] = dataInput.Premi.MataUang == "IDR" ? App_Data.Text.Rupiah : App_Data.Text.Rupiah;
            dtData.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk != "HLLIFE")
            {
                newRow = dtData.NewRow();
                newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLFUTURE" ? App_Data.Text.UangPertanggunganAwal : App_Data.Text.UangPertanggungan;
                newRow["Value"] = dataInput.StrCurrency + dataInput.Premi.UangPertanggungan;
                dtData.Rows.Add(newRow);
            }

            var total = CalculatorServices.CurrencyToDecimal(premi) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);
            string caraBayar, berkala;

            if (dataInput.Premi.CaraBayar == "1")
            {
                caraBayar = dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLGOLDENB" ? App_Data.Text.TahunanGolden : App_Data.Text.Tahunan;
                berkala = App_Data.Text.TahunSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "2")
            {
                caraBayar = App_Data.Text.Semesteran;
                berkala = App_Data.Text.SemeterSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "4")
            {
                caraBayar = App_Data.Text.Kuartalan;
                berkala = App_Data.Text.KuartalSpasi;
            }
            else
            {
                caraBayar = App_Data.Text.Bulanan;
                berkala = App_Data.Text.BulanSpasi;
            }

            newRow = dtData.NewRow();

            newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLEDU" || dataInput.Nasabah.NamaProduk == "HLKIDS" || dataInput.Nasabah.NamaProduk == "HLLIFE" || dataInput.Nasabah.NamaProduk == "HLFUTURE" || dataInput.Nasabah.NamaProduk == "HLSAVING" ? App_Data.Text.PremiAsuransiDasar : App_Data.Text.PremiDasar;


            if (dataInput.Premi.ModeBayarPremi == "sekaligus" && dataInput.Nasabah.NamaProduk == "HLEDU")
            {
                newRow["Value"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.PremiAsuransiTambahan;
                newRow["Value"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.TotalPremi;
                newRow["Value"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total);
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLEDU" ? App_Data.Text.CaraPembayaranPremi : App_Data.Text.CaraPembayaran;
                newRow["Value"] = App_Data.Text.Sekaligus;
                dtData.Rows.Add(newRow);
            }
            else
            {
                newRow["Value"] = dataInput.StrCurrency + premi + " /" + berkala;

                if (dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLGOLDENB")
                {
                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.PremiAsuransi;
                    newRow["Value"] = dataInput.StrCurrency + premi + " /" + berkala;
                }

                dtData.Rows.Add(newRow);

                if (dataInput.Nasabah.NamaProduk != "HLLIFE" && dataInput.Nasabah.NamaProduk != "HLGOLDENB" && dataInput.Nasabah.NamaProduk != "HLGOLDENA" && dataInput.Nasabah.NamaProduk != "HLFUTURE" && dataInput.Nasabah.NamaProduk != "HLSAVING")
                {
                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.PremiAsuransiTambahan;
                    newRow["Value"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan + " /" + berkala;
                    dtData.Rows.Add(newRow);

                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.TotalPremi;
                    newRow["Value"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) + " /" + berkala;
                    dtData.Rows.Add(newRow);
                }

                newRow = dtData.NewRow();
                newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLGOLDENB" || dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLEDU" || dataInput.Nasabah.NamaProduk == "HLKIDS" || dataInput.Nasabah.NamaProduk == "HLLIFE" || dataInput.Nasabah.NamaProduk == "HLFUTURE" || dataInput.Nasabah.NamaProduk == "HLSAVING" ? App_Data.Text.CaraPembayaranPremi : App_Data.Text.CaraPembayaran;
                newRow["Value"] = caraBayar;
                dtData.Rows.Add(newRow);
                if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                {
                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.MasaPembayaranPremi;
                    newRow["Value"] = dataInput.Premi.RencanaMasaPembayaran;
                    dtData.Rows.Add(newRow);

                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.MasaAsuransi;
                    newRow["Value"] = dataInput.Premi.RencanaMasaPembayaran;
                    dtData.Rows.Add(newRow);
                }
                if (dataInput.Nasabah.NamaProduk == "HLFUTURE" || dataInput.Nasabah.NamaProduk == "HLSAVING" || dataInput.Nasabah.NamaProduk == "HLGOLDENA" || dataInput.Nasabah.NamaProduk == "HLGOLDENB")
                {
                    newRow = dtData.NewRow();
                    newRow["Caption"] = App_Data.Text.MasaPembayaranPremi;
                    newRow["Value"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunJamak;
                    dtData.Rows.Add(newRow);
                }
            }

            return dtData;
        }

        public static DataTable DanaTahapan(SummaryViewModel dataInput, List<ProductTahapanRate> tahapanRates)
        {
            var dtTahapan = new DataTable { TableName = "DanaTahapan" };
            dtTahapan.Columns.AddRange(new[] { new DataColumn("UsiaAnak"), new DataColumn("Percentage"), new DataColumn("Rupiah") });
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            foreach (var t in tahapanRates)
            {
                var fund = dtTahapan.NewRow();
                fund["UsiaAnak"] = t.Year;
                fund["Percentage"] = t.RateTahapan + "%";
                fund["Rupiah"] = CalculatorServices.DecimalToCurrency(Convert.ToDecimal(Convert.ToDecimal(t.RateTahapan) * up / 100));
                dtTahapan.Rows.Add(fund);
            }

            return dtTahapan;
        }

        public static DataTable CreateRingkasanRegular(DataTable dtFund, SummaryViewModel dataInput)
        {
            var dtRegular = new DataTable { TableName = "InvestBenefit" };

            dtRegular.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("Usia"),

                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopUp"),
                new DataColumn("Withdrawal"),

                new DataColumn("Rendah"),
                new DataColumn("Sedang"),
                new DataColumn("Tinggi"),

                new DataColumn("PremiAllTime"),
                new DataColumn("RendahAllTime"),
                new DataColumn("SedangAllTime"),
                new DataColumn("TinggiAllTime"),

                new DataColumn("MatiRendah"),
                new DataColumn("MatiSedang"),
                new DataColumn("MatiTinggi"),

                new DataColumn("MatiRendahAllTime"),
                new DataColumn("MatiSedangAllTime"),
                new DataColumn("MatiTinggiAllTime")
            });

            if (dtFund == null) return dtRegular;
            var investments = dataInput.Premi.Investments;
            int lastRow = Convert.ToInt32(dtFund.Rows[dtFund.Rows.Count - 1]["Usia"].ToString());

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["Usia"]);

                if (tahunKe == 26)
                {
                    var drSkip = dtRegular.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["Usia"] = ":";
                    drSkip["UangPertanggungan"] = ":";
                    drSkip["PremiPerTahun"] = ":";
                    drSkip["TopUp"] = ":";
                    drSkip["Withdrawal"] = ":";
                    drSkip["PremiAllTime"] = ":";
                    drSkip["Rendah"] = ":";
                    drSkip["Sedang"] = ":";
                    drSkip["Tinggi"] = ":";
                    drSkip["RendahAllTime"] = ":";
                    drSkip["SedangAllTime"] = ":";
                    drSkip["TinggiAllTime"] = ":";
                    drSkip["MatiRendah"] = ":";
                    drSkip["MatiSedang"] = ":";
                    drSkip["MatiTinggi"] = ":";
                    drSkip["MatiRendahAllTime"] = ":";
                    drSkip["MatiSedangAllTime"] = ":";
                    drSkip["MatiTinggiAllTime"] = ":";

                    dtRegular.Rows.Add(drSkip);
                }

                if (dtRegular.Rows.Count >= 26)
                {
                    if (usiaKe < 55) continue;
                    if (usiaKe >= 56 && usiaKe < 60) continue;
                    if (usiaKe >= 61 && usiaKe < 65) continue;
                    if (usiaKe >= 66 && usiaKe < 70) continue;
                    if (usiaKe >= 71 && usiaKe < 75) continue;
                    if (usiaKe >= 76 && usiaKe < 80) continue;
                    if (usiaKe >= 81 && usiaKe < 85) continue;
                    if (usiaKe >= 86 && usiaKe < 90) continue;
                    if (usiaKe >= 91 && usiaKe < 95) continue;
                    if (usiaKe >= 96 && usiaKe < lastRow) continue;
                }

                var dr = dtRegular.NewRow();
                dr["Tahun"] = tahunKe;
                dr["Usia"] = usiaKe;

                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                dr["PremiPerTahun"] = dtFund.Rows[i]["PremiPerTahun"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahun"])) / 1000) : "";

                dr["TopUp"] = dtFund.Rows[i]["TopupPremi"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["TopupPremi"])) / 1000) : "";

                dr["Withdrawal"] = dtFund.Rows[i]["Withdrawal"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["Withdrawal"])) / 1000) : "";

                if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    dr["PremiAllTime"] = dtFund.Rows[i]["PremiPerTahunSeumurHidup"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahunSeumurHidup"])) / 1000) : "";

                var invRendah = 0m;
                var invSedang = 0m;
                var invTinggi = 0m;

                var invAllTimeRendah = 0m;
                var invAllTimeSedang = 0m;
                var invAllTimeTinggi = 0m;

                foreach (var fundType in investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
                {
                    invRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_low"]));
                    invSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_moderate"]));
                    invTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_high"]));

                    if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    {
                        invAllTimeRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_low"]));
                        invAllTimeSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_moderate"]));
                        invAllTimeTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_high"]));
                    }

                    if (dataInput.Nasabah.NamaProduk == "HLINVEST")
                    {
                        invRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_low"]));
                        invSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_moderate"]));
                        invTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_high"]));
                        invAllTimeRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_low"]));
                        invAllTimeSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_moderate"]));
                        invAllTimeTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_high"]));
                    }
                }

                dr["Rendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendah / 1000) : "*****";
                dr["Sedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedang / 1000) : "*****";
                dr["Tinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggi / 1000) : "*****";

                if (dataInput.Nasabah.NamaProduk != "HLSIN")
                {
                    dr["RendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendah / 1000) : "*****";
                    dr["SedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedang / 1000) : "*****";
                    dr["TinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggi / 1000) : "*****";
                }

                if (dtFund.Rows[i]["UangPertanggungan"] != null)
                {
                    var up = Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"]);

                    var invRendahDeath = invRendah + up;
                    var invSedangDeath = invSedang + up;
                    var invTinggiDeath = invTinggi + up;

                    dr["MatiRendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendahDeath / 1000) : "*****";
                    dr["MatiSedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedangDeath / 1000) : "*****";
                    dr["MatiTinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggiDeath / 1000) : "*****";

                    if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    {
                        var invAllTimeRendahDeath = invAllTimeRendah + up;
                        var invAllTimeSedangDeath = invAllTimeSedang + up;
                        var invAllTimeTinggiDeath = invAllTimeTinggi + up;

                        dr["MatiRendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendahDeath / 1000) : "*****";
                        dr["MatiSedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedangDeath / 1000) : "*****";
                        dr["MatiTinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggiDeath / 1000) : "*****";
                    }
                }

                dtRegular.Rows.Add(dr);
            }

            return dtRegular;
        }

        public static DataTable CreateSurrenderRegular(DataTable dtFund, SummaryViewModel dataInput, List<ProductAllocationRate> allocationRate)
        {
            var dtRegular = new DataTable { TableName = "InvestSurrender" };

            dtRegular.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("Usia"),

                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopUp"),
                new DataColumn("Withdrawal"),

                new DataColumn("Rendah"),
                new DataColumn("Sedang"),
                new DataColumn("Tinggi"),

                new DataColumn("PremiAllTime"),
                new DataColumn("RendahAllTime"),
                new DataColumn("SedangAllTime"),
                new DataColumn("TinggiAllTime"),

                new DataColumn("MatiRendah"),
                new DataColumn("MatiSedang"),
                new DataColumn("MatiTinggi"),

                new DataColumn("MatiRendahAllTime"),
                new DataColumn("MatiSedangAllTime"),
                new DataColumn("MatiTinggiAllTime")
            });

            if (dtFund == null) return dtRegular;
            var investments = dataInput.Premi.Investments;
            int lastRow = Convert.ToInt32(dtFund.Rows[dtFund.Rows.Count - 1]["Usia"].ToString());

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["Usia"]);
                var allocationRateThisYear = allocationRate.Where(x => x.Year == tahunKe).FirstOrDefault();
                decimal surrenderRateThisYear = allocationRateThisYear == null ? 1 : 1 - (Convert.ToDecimal(allocationRateThisYear.RateDeathBenefit) / 100);

                if (tahunKe == 26)
                {
                    var drSkip = dtRegular.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["Usia"] = ":";
                    drSkip["UangPertanggungan"] = ":";
                    drSkip["PremiPerTahun"] = ":";
                    drSkip["TopUp"] = ":";
                    drSkip["Withdrawal"] = ":";
                    drSkip["PremiAllTime"] = ":";
                    drSkip["Rendah"] = ":";
                    drSkip["Sedang"] = ":";
                    drSkip["Tinggi"] = ":";
                    drSkip["RendahAllTime"] = ":";
                    drSkip["SedangAllTime"] = ":";
                    drSkip["TinggiAllTime"] = ":";
                    drSkip["MatiRendah"] = ":";
                    drSkip["MatiSedang"] = ":";
                    drSkip["MatiTinggi"] = ":";
                    drSkip["MatiRendahAllTime"] = ":";
                    drSkip["MatiSedangAllTime"] = ":";
                    drSkip["MatiTinggiAllTime"] = ":";

                    dtRegular.Rows.Add(drSkip);
                }

                if (dtRegular.Rows.Count >= 26)
                {
                    if (usiaKe < 55) continue;
                    if (usiaKe >= 56 && usiaKe < 60) continue;
                    if (usiaKe >= 61 && usiaKe < 65) continue;
                    if (usiaKe >= 66 && usiaKe < 70) continue;
                    if (usiaKe >= 71 && usiaKe < 75) continue;
                    if (usiaKe >= 76 && usiaKe < 80) continue;
                    if (usiaKe >= 81 && usiaKe < 85) continue;
                    if (usiaKe >= 86 && usiaKe < 90) continue;
                    if (usiaKe >= 91 && usiaKe < 95) continue;
                    if (usiaKe >= 96 && usiaKe < lastRow) continue;
                }

                var dr = dtRegular.NewRow();
                dr["Tahun"] = tahunKe;
                dr["Usia"] = usiaKe;

                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                dr["PremiPerTahun"] = dtFund.Rows[i]["PremiPerTahun"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahun"])) / 1000) : "";

                dr["TopUp"] = dtFund.Rows[i]["TopupPremi"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["TopupPremi"])) / 1000) : "";

                dr["Withdrawal"] = dtFund.Rows[i]["Withdrawal"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["Withdrawal"])) / 1000) : "";

                //if (dataInput.Nasabah.NamaProduk != "HLSIN")
                dr["PremiAllTime"] = dtFund.Rows[i]["PremiPerTahunSeumurHidup"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahunSeumurHidup"])) / 1000) : "";

                var invRendah = 0m;
                var invSedang = 0m;
                var invTinggi = 0m;

                var invAllTimeRendah = 0m;
                var invAllTimeSedang = 0m;
                var invAllTimeTinggi = 0m;

                foreach (var fundType in investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
                {
                    invRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_low"]) * surrenderRateThisYear);
                    invSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_moderate"]) * surrenderRateThisYear);
                    invTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_high"]) * surrenderRateThisYear);

                    //if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    //{
                    invAllTimeRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_low"]) * surrenderRateThisYear);
                    invAllTimeSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_moderate"]) * surrenderRateThisYear);
                    invAllTimeTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_high"]) * surrenderRateThisYear);
                    //}

                    if (dataInput.Nasabah.NamaProduk == "HLINVEST")
                    {
                        invRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_low"]));
                        invSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_moderate"]));
                        invTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_topup_high"]));
                        invAllTimeRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_low"]));
                        invAllTimeSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_moderate"]));
                        invAllTimeTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_topup_high"]));
                    }
                }

                dr["Rendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendah / 1000) : "*****";
                dr["Sedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedang / 1000) : "*****";
                dr["Tinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggi / 1000) : "*****";

                //if (dataInput.Nasabah.NamaProduk != "HLSIN")
                //{
                dr["RendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendah / 1000) : "*****";
                dr["SedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedang / 1000) : "*****";
                dr["TinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggi / 1000) : "*****";
                //}

                if (dtFund.Rows[i]["UangPertanggungan"] != null)
                {
                    var up = Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"]);

                    var invRendahDeath = invRendah + up;
                    var invSedangDeath = invSedang + up;
                    var invTinggiDeath = invTinggi + up;

                    dr["MatiRendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendahDeath / 1000) : "*****";
                    dr["MatiSedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedangDeath / 1000) : "*****";
                    dr["MatiTinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggiDeath / 1000) : "*****";

                    //if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    //{
                    var invAllTimeRendahDeath = invAllTimeRendah + up;
                    var invAllTimeSedangDeath = invAllTimeSedang + up;
                    var invAllTimeTinggiDeath = invAllTimeTinggi + up;

                    dr["MatiRendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendahDeath / 1000) : "*****";
                    dr["MatiSedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedangDeath / 1000) : "*****";
                    dr["MatiTinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggiDeath / 1000) : "*****";
                    //}
                }

                dtRegular.Rows.Add(dr);
            }

            return dtRegular;
        }
        //add masa by alif
        public static DataTable CreateTraditionalHeader(SummaryViewModel dataInput)
        {
            var dt = new DataTable { TableName = "InvestBenefitTraditionalHeader" };

            dt.Columns.Add("Data");
            dt.Columns.Add("Masa");

            var dtRow = dt.NewRow();
            dtRow["Data"] = dataInput.Premi.ModeBayarPremi == "sekaligus" ? App_Data.Text.PremiDasar : App_Data.Text.PremiTahunan;
            //--Hanya untuk mengisi formula di report 
            dtRow["Masa"] = dataInput.Premi.RencanaMasaPembayaran;
            dt.Rows.Add(dtRow);
            return dt;

        }

        public static DataTable CreateRingkasanTraditional(DataTable dtFund, Product product)
        {
            var dtTraditional = new DataTable { TableName = "InvestBenefitTraditional" };

            dtTraditional.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            if (dtFund == null) return dtTraditional;

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["UsiaTertanggung"]);
                var usiaAnakKe = Convert.ToInt32(dtFund.Rows[i]["UsiaAnak"]);

                if (tahunKe == 23)
                {
                    var drSkip = dtTraditional.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["UsiaAnak"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["AkumulasiPremiTahunan"] = ":";
                    drSkip["PremiTahunan"] = ":";
                    drSkip["DanaTahapan"] = ":";
                    drSkip["AkumulasiDanaTahapan"] = ":";
                    drSkip["Reserve"] = ":";
                    drSkip["CashValue"] = ":";
                    drSkip["DeathBenefit"] = ":";
                    drSkip["Total"] = ":";

                    dtTraditional.Rows.Add(drSkip);
                }

                if (dtTraditional.Rows.Count >= 23)
                {
                    if (usiaAnakKe < 25) continue;
                    if (usiaAnakKe >= 26 && usiaAnakKe < 30) continue;
                    if (usiaAnakKe >= 31 && usiaAnakKe < 35) continue;
                    if (usiaAnakKe >= 36 && usiaAnakKe < 40) continue;
                }

                var dr = dtTraditional.NewRow();
                dr["Tahun"] = tahunKe;
                dr["UsiaTertanggung"] = usiaKe;
                dr["UsiaAnak"] = usiaAnakKe;

                if (product.ProductCode == "HLFUTURE" || product.ProductCode == "HLSAVING")
                {
                    dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"]))) : "";
                    dr["PremiTahunan"] = dtFund.Rows[i]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiTahunan"]))) : "";
                    dr["AkumulasiPremiTahunan"] = dtFund.Rows[i]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiPremiTahunan"]))) : "";
                    dr["DanaTahapan"] = dtFund.Rows[i]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DanaTahapan"]))) : "";
                    dr["AkumulasiDanaTahapan"] = dtFund.Rows[i]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiDanaTahapan"]))) : "";
                    dr["Reserve"] = dtFund.Rows[i]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Reserve"]))) : "";
                    dr["CashValue"] = dtFund.Rows[i]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["CashValue"]))) : "";
                    dr["DeathBenefit"] = dtFund.Rows[i]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DeathBenefit"]))) : "";
                    dr["Total"] = dtFund.Rows[i]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Total"]))) : "";
                }
                else
                {
                    dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                    dr["PremiTahunan"] = dtFund.Rows[i]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiTahunan"])) / 1000) : "";
                    dr["AkumulasiPremiTahunan"] = dtFund.Rows[i]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiPremiTahunan"])) / 1000) : "";
                    dr["DanaTahapan"] = dtFund.Rows[i]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DanaTahapan"])) / 1000) : "";
                    dr["AkumulasiDanaTahapan"] = dtFund.Rows[i]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiDanaTahapan"])) / 1000) : "";
                    dr["Reserve"] = dtFund.Rows[i]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Reserve"])) / 1000) : "";
                    dr["CashValue"] = dtFund.Rows[i]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["CashValue"])) / 1000) : "";
                    dr["DeathBenefit"] = dtFund.Rows[i]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DeathBenefit"])) / 1000) : "";
                    dr["Total"] = dtFund.Rows[i]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Total"])) / 1000) : "";
                }

                dtTraditional.Rows.Add(dr);
            }

            return dtTraditional;
        }

        //Add by Fitri 11/06/2020
        public static DataTable CreateRingkasanTraditionalGoldenLife(DataTable dtFund, Product product)
        {
            var dtTraditional = new DataTable { TableName = "InvestBenefitTraditional" };

            dtTraditional.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            if (dtFund == null) return dtTraditional;

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["UsiaTertanggung"]);
                var usiaAnakKe = Convert.ToInt32(dtFund.Rows[i]["UsiaAnak"]);


                if (tahunKe == 21)
                {
                    var drSkip = dtTraditional.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["UsiaAnak"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["AkumulasiPremiTahunan"] = ":";
                    drSkip["PremiTahunan"] = ":";
                    drSkip["DanaTahapan"] = ":";
                    drSkip["AkumulasiDanaTahapan"] = ":";
                    drSkip["Reserve"] = ":";
                    drSkip["CashValue"] = ":";
                    drSkip["DeathBenefit"] = ":";
                    drSkip["Total"] = ":";

                    dtTraditional.Rows.Add(drSkip);
                }

                if (dtTraditional.Rows.Count >= 21)
                {
                    if (dtTraditional.Rows.Count == 21)
                    {
                        for (int a = 0; a < dtFund.Rows.Count; a++)
                        {
                            var tahunPolisKe = Convert.ToInt32(dtFund.Rows[a]["Tahun"]);
                            var usiaTertanggungKe = Convert.ToInt32(dtFund.Rows[a]["UsiaTertanggung"]);
                            var usiaAnakKeBerapa = Convert.ToInt32(dtFund.Rows[a]["UsiaAnak"]);

                            if (usiaTertanggungKe < 65) continue;
                            if (usiaTertanggungKe > 65 && usiaTertanggungKe < 70) continue;
                            if (usiaTertanggungKe > 70 && usiaTertanggungKe < 80) continue;
                            if (usiaTertanggungKe > 80 && usiaTertanggungKe < 90) continue;
                            if (usiaTertanggungKe > 90 && usiaTertanggungKe < 100) continue;

                            var dr2 = dtTraditional.NewRow();
                            dr2["Tahun"] = tahunPolisKe;
                            dr2["UsiaTertanggung"] = usiaTertanggungKe;
                            dr2["UsiaAnak"] = usiaAnakKeBerapa;

                            dr2["UangPertanggungan"] = dtFund.Rows[a]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["UangPertanggungan"]))) : "";
                            dr2["PremiTahunan"] = dtFund.Rows[a]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["PremiTahunan"]))) : "";
                            dr2["AkumulasiPremiTahunan"] = dtFund.Rows[a]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["AkumulasiPremiTahunan"]))) : "";
                            dr2["DanaTahapan"] = dtFund.Rows[a]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["DanaTahapan"]))) : "";
                            dr2["AkumulasiDanaTahapan"] = dtFund.Rows[a]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["AkumulasiDanaTahapan"]))) : "";
                            dr2["Reserve"] = dtFund.Rows[a]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["Reserve"]))) : "";
                            dr2["CashValue"] = dtFund.Rows[a]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["CashValue"]))) : "";
                            dr2["DeathBenefit"] = dtFund.Rows[a]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["DeathBenefit"]))) : "";
                            dr2["Total"] = dtFund.Rows[a]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["Total"]))) : "";

                            dtTraditional.Rows.Add(dr2);
                        }
                    }

                    //if (tahunKe >= 21 && usiaKe < 65) continue;
                    //if (usiaKe > 65 && usiaKe < 70) continue;
                    //if (usiaKe > 70 && usiaKe < 80) continue;
                    //if (usiaKe > 80 && usiaKe < 90) continue;
                    //if (usiaKe > 90 && usiaKe < 100) continue;
                    continue;
                }
                var dr = dtTraditional.NewRow();
                dr["Tahun"] = tahunKe;
                dr["UsiaTertanggung"] = usiaKe;
                dr["UsiaAnak"] = usiaAnakKe;

                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"]))) : "";
                dr["PremiTahunan"] = dtFund.Rows[i]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiTahunan"]))) : "";
                dr["AkumulasiPremiTahunan"] = dtFund.Rows[i]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiPremiTahunan"]))) : "";
                dr["DanaTahapan"] = dtFund.Rows[i]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DanaTahapan"]))) : "";
                dr["AkumulasiDanaTahapan"] = dtFund.Rows[i]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiDanaTahapan"]))) : "";
                dr["Reserve"] = dtFund.Rows[i]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Reserve"]))) : "";
                dr["CashValue"] = dtFund.Rows[i]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["CashValue"]))) : "";
                dr["DeathBenefit"] = dtFund.Rows[i]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DeathBenefit"]))) : "";
                dr["Total"] = dtFund.Rows[i]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Total"]))) : "";

                dtTraditional.Rows.Add(dr);
            }

            return dtTraditional;
        }
        //End add

        public static DataTable CreateRingkasanTraditionalSmartCI(DataTable dtFund, Product product)
        {
            var dtTraditional = new DataTable { TableName = "InvestBenefitTraditional" };

            dtTraditional.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            if (dtFund == null) return dtTraditional;

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["UsiaTertanggung"]);


                //if (tahunKe == 16)
                //{
                //    var drSkip = dtTraditional.NewRow();

                //    drSkip["Tahun"] = ":";
                //    drSkip["UsiaTertanggung"] = ":";
                //    drSkip["UsiaAnak"] = ":";
                //    drSkip["UsiaTertanggung"] = ":";
                //    drSkip["AkumulasiPremiTahunan"] = ":";
                //    drSkip["PremiTahunan"] = ":";
                //    drSkip["DanaTahapan"] = ":";
                //    drSkip["AkumulasiDanaTahapan"] = ":";
                //    drSkip["Reserve"] = ":";
                //    drSkip["CashValue"] = ":";
                //    drSkip["DeathBenefit"] = ":";
                //    drSkip["Total"] = ":";

                //    dtTraditional.Rows.Add(drSkip);
                //}

                //if (dtTraditional.Rows.Count >= 16)
                //{
                //    if (dtTraditional.Rows.Count == 16)
                //    {
                //        for (int a = 0; a < dtFund.Rows.Count; a++)
                //        {
                //            var tahunPolisKe = Convert.ToInt32(dtFund.Rows[a]["Tahun"]);
                //            var usiaTertanggungKe = Convert.ToInt32(dtFund.Rows[a]["UsiaTertanggung"]);

                //            if (usiaTertanggungKe < 79 && usiaTertanggungKe < 80) continue;

                //            var dr2 = dtTraditional.NewRow();
                //            dr2["Tahun"] = tahunPolisKe;
                //            dr2["UsiaTertanggung"] = usiaTertanggungKe;

                //            dr2["UangPertanggungan"] = dtFund.Rows[a]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["UangPertanggungan"])) / 1000) : "";
                //            dr2["PremiTahunan"] = dtFund.Rows[a]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["PremiTahunan"])) / 1000) : "";
                //            dr2["AkumulasiPremiTahunan"] = dtFund.Rows[a]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["AkumulasiPremiTahunan"])) / 1000) : "";
                //            dr2["DanaTahapan"] = dtFund.Rows[a]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["DanaTahapan"])) / 1000) : "";
                //            dr2["AkumulasiDanaTahapan"] = dtFund.Rows[a]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["AkumulasiDanaTahapan"])) / 1000) : "";
                //            dr2["Reserve"] = dtFund.Rows[a]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["Reserve"])) / 1000) : "";
                //            dr2["CashValue"] = dtFund.Rows[a]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["CashValue"])) / 1000) : "";
                //            dr2["DeathBenefit"] = dtFund.Rows[a]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["DeathBenefit"])) / 1000) : "";
                //            dr2["Total"] = dtFund.Rows[a]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[a]["Total"])) / 1000) : "";

                //            dtTraditional.Rows.Add(dr2);
                //        }
                //    }
                //    continue;
                //}
                var dr = dtTraditional.NewRow();
                dr["Tahun"] = tahunKe;
                dr["UsiaTertanggung"] = usiaKe;


                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                dr["PremiTahunan"] = dtFund.Rows[i]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiTahunan"])) / 1000) : "";
                dr["AkumulasiPremiTahunan"] = dtFund.Rows[i]["AkumulasiPremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiPremiTahunan"])) / 1000) : "";
                dr["DanaTahapan"] = dtFund.Rows[i]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DanaTahapan"])) / 1000) : "";
                dr["AkumulasiDanaTahapan"] = dtFund.Rows[i]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiDanaTahapan"])) / 1000) : "";
                dr["Reserve"] = dtFund.Rows[i]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Reserve"])) / 1000) : "";
                dr["CashValue"] = dtFund.Rows[i]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["CashValue"])) / 1000) : "";
                dr["DeathBenefit"] = dtFund.Rows[i]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DeathBenefit"])) / 1000) : "";
                dr["Total"] = dtFund.Rows[i]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Total"])) / 1000) : "";

                dtTraditional.Rows.Add(dr);
            }

            return dtTraditional;
        }
        //End add

        public static DataTable CalculateFundBenefitRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var totalPremiTambahanTahunIni = 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1)
                    {
                        biayaAdminTahunIni = 0;
                        biayaAsuransiTahunPertama = biayaAsuransiTahunIni;
                        biayaAsuransiTahunIni = 0;
                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if (tahunKe == 2 || tahunKe == 3)
                        {
                            biayaAdminTahunIni *= 1.5m;
                            biayaAsuransiTahunIni += (decimal)(biayaAsuransiTahunPertama / 2);
                            lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                        monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            if ((dataInput.Nasabah.TertanggungUtama == "Ya" && dataInput.Nasabah.UmurPemegangPolis <= (product.ChildMaxAge / 12)) ||
                (dataInput.Nasabah.TertanggungUtama != "Ya" && dataInput.Nasabah.UmurTertanggungUtama <= (product.ChildMaxAge / 12)))
            {
                if (CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) > product.MaxAmountSumInsuredChild)
                {
                    errorMsgList.Add("Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp " + CalculatorServices.DecimalToCurrency(product.MaxAmountSumInsuredChild.Value).ToString());
                }
            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}


            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                    else if (product.ProductCode.Equals("HLPREF"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                // Hanwha Wizer cov term 70 year
                                if (product.ProductCode.Equals("HLWIZ"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                                else if (product.ProductCode.Equals("HLPREF"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                if (product.ProductCategory == "preferred" || product.ProductCategory == "maxpro")
                {
                    loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                    loyaltyBonusRateTahunIni = loyaltyAndSupplementaryTahunIni == null ? 0 : loyaltyAndSupplementaryTahunIni.LoyaltyBonusPerc;
                    supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null && tahunKe <= 10 ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                }

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIni = dataInput.Premi.RencanaMasaPembayaran >= (tahunKe - 1) ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIniAlwaysPremi = (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala;

                    if (product.ProductCategory != "maxpro")
                        loyaltyBonusTahunIniAlwaysPremi = loyaltyBonusTahunIni;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1 && !product.ProductCode.Equals("HLPREF"))
                    {
                        if (product.ProductCode.Equals("HLWIZ"))
                        {
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                        }
                        else
                        {
                            biayaAdminTahunIni = 0;
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                            biayaAsuransiTahunIni = 0 + biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred;
                        }

                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if ((tahunKe == 2 || tahunKe == 3) && !product.ProductCode.Equals("HLPREF"))
                        {
                            if (product.ProductCode.Equals("HLWIZ"))
                            {
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }
                            else
                            {
                                biayaAdminTahunIni *= 1.5m;
                                biayaAsuransiTahunIni += (biayaAsuransiTahunPertama / 2);
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }

                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        if (product.ProductCategory == "preferred" || product.ProductCategory == "maxpro")
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                            monthFundMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                            monthFundHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                            monthFundLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                            monthFundMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                            monthFundHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);
                        }
                        else
                        {

                            monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                            monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                InvestmentFundRow tahunKeInvestmentRow = null;

                if (product.ProductCode == "HLMAXPRO")
                {
                    tahunKeInvestmentRow = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && x.Tahun <= 5 && x.Low < 0);
                }

                tahunKeInvestmentRow = tahunKeInvestmentRow != null ? tahunKeInvestmentRow :
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }
            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderInvest(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_topup_low"),
                    new DataColumn(fundType + "_alltime_topup_low"),

                    new DataColumn(fundType + "_topup_moderate"),
                    new DataColumn(fundType + "_alltime_topup_moderate"),

                    new DataColumn(fundType + "_topup_high"),
                    new DataColumn(fundType + "_alltime_topup_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;
            int? loyaltyFirstYear = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().Year;
            int loyaltyYear = loyaltyFirstYear.Value;
            int? loyaltyPerc = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().LoyaltyBonusPerc;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIni = topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIniAlwaysPremi = topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = MainServices.CalculateCostOfInsurance(dataInput, (i - 1));
                var premiAsuransiDasarTahunIniAllYear = premiAsuransiDasarTahunIni;

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                loyaltyBonusRateTahunIni = loyaltyYear == tahunKe ? loyaltyPerc : 0;
                supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                loyaltyYear = tahunKe == loyaltyYear ? tahunKe + 2 : loyaltyYear;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIniAlwaysPremi = tahunKe >= loyaltyFirstYear ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIni = dataInput.Premi.RencanaMasaPembayaran >= (tahunKe) ? loyaltyBonusTahunIniAlwaysPremi : 0;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupLow = 0;
                    decimal lastYearInvestmentTopupLowAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupMed = 0;
                    decimal lastYearInvestmentTopupMedAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupHigh = 0;
                    decimal lastYearInvestmentTopupHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        lastYearInvestmentTopupLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_low"]);
                        lastYearInvestmentTopupMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_moderate"]);
                        lastYearInvestmentTopupHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_high"]);

                        lastYearInvestmentTopupLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_low"]);
                        lastYearInvestmentTopupMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_moderate"]);
                        lastYearInvestmentTopupHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? topupLow = 0;
                    decimal? topupMed = 0;
                    decimal? topupHigh = 0;
                    decimal? topupLowAllTime = 0;
                    decimal? topupMedAllTime = 0;
                    decimal? topupHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;
                    decimal? monthTopupLow = 0;
                    decimal? monthTopupMed = 0;
                    decimal? monthTopupHigh = 0;
                    decimal? monthTopupLowAllTime = 0;
                    decimal? monthTopupMedAllTime = 0;
                    decimal? monthTopupHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? lastMonthInvestmentTopupLow = 0;
                        decimal? lastMonthInvestmentTopupMed = 0;
                        decimal? lastMonthInvestmentTopupHigh = 0;
                        decimal? lastMonthInvestmentTopupLowAllTime = 0;
                        decimal? lastMonthInvestmentTopupMedAllTime = 0;
                        decimal? lastMonthInvestmentTopupHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;
                        decimal? totalAlokasiTopupBulanan = 0;
                        decimal? totalAlokasiTopupBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;

                            lastMonthInvestmentTopupLow = lastYearInvestmentTopupLow;
                            lastMonthInvestmentTopupMed = lastYearInvestmentTopupMed;
                            lastMonthInvestmentTopupHigh = lastYearInvestmentTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = lastYearInvestmentTopupLowAlwaysPremi;
                            lastMonthInvestmentTopupMedAllTime = lastYearInvestmentTopupMedAlwaysPremi;
                            lastMonthInvestmentTopupHighAllTime = lastYearInvestmentTopupHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;

                            lastMonthInvestmentTopupLow = monthTopupLow;
                            lastMonthInvestmentTopupMed = monthTopupMed;
                            lastMonthInvestmentTopupHigh = monthTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = monthTopupLowAllTime;
                            lastMonthInvestmentTopupMedAllTime = monthTopupMedAllTime;
                            lastMonthInvestmentTopupHighAllTime = monthTopupHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                                totalAlokasiTopupBulanan = totalAlokasiTopupTahunIni * fundAllocationInPoint;
                                totalAlokasiTopupBulananAlwaysPremi = totalAlokasiTopupTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthTopupLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLow, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                        monthTopupMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMed, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                        monthTopupHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHigh, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                        monthTopupLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLowAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                        monthTopupMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMedAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                        monthTopupHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHighAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    topupLow = monthTopupLow;
                    topupMed = monthTopupMed;
                    topupHigh = monthTopupHigh;

                    topupLowAllTime = monthTopupLowAllTime;
                    topupMedAllTime = monthTopupMedAllTime;
                    topupHighAllTime = monthTopupHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    drFund[fundType + "_topup_low"] = topupLow;
                    drFund[fundType + "_topup_moderate"] = topupMed;
                    drFund[fundType + "_topup_high"] = topupHigh;

                    drFund[fundType + "_alltime_topup_low"] = topupLowAllTime;
                    drFund[fundType + "_alltime_topup_moderate"] = topupMedAllTime;
                    drFund[fundType + "_alltime_topup_high"] = topupHighAllTime;

                    investmentFundRow.Low += fundLow.Value + topupLow.Value;
                    investmentFundRow.Med += fundMed.Value + topupMed.Value;
                    investmentFundRow.High += fundHigh.Value + topupHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value + topupLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value + topupMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value + topupHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high"),

                    new DataColumn(fundType + "_alltime_death_low"),
                    new DataColumn(fundType + "_alltime_death_moderate"),
                    new DataColumn(fundType + "_alltime_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                TopUpWithdrawal topupTahunIni = null;
                TopUpWithdrawal withdrawalTahunIni = null;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                    else if (product.ProductCode.Equals("HLPREF"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                // Hanwha Wizer cov term 70 year
                                if (product.ProductCode.Equals("HLWIZ"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                                else if (product.ProductCode.Equals("HLPREF"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    topupTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => x.IsTopUp && x.Year == tahunKe);
                    withdrawalTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => !x.IsTopUp && x.Year == tahunKe);
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalTopUpTahunIni = topupTahunIni != null ? CalculatorServices.CurrencyToDecimal(topupTahunIni.Amount) * (decimal)(allocationRate.RateSingleTopUp / 100) : 0;
                var totalWithdrawalTahunIni = withdrawalTahunIni != null ? CalculatorServices.CurrencyToDecimal(withdrawalTahunIni.Amount) : 0;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                var checkWithdrawalForLoyalty = dataInput.TopUp.TopupWithdrawals == null ? 0 : dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false && (x.Year + 1) <= tahunKe).Count();
                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                if (product.ProductCategory == "preferred" || product.ProductCategory == "maxpro")
                {
                    loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                    loyaltyBonusRateTahunIni = loyaltyAndSupplementaryTahunIni == null ? 0 : loyaltyAndSupplementaryTahunIni.LoyaltyBonusPerc;
                    supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null && tahunKe <= 10 ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                }


                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;

                if (topupTahunIni != null) drFund["TopupPremi"] = topupTahunIni.Amount;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;
                if (withdrawalTahunIni != null) drFund["Withdrawal"] = withdrawalTahunIni.Amount;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIni = checkWithdrawalForLoyalty <= 0 && dataInput.Premi.RencanaMasaPembayaran >= (product.ProductCategory == "maxpro" ? tahunKe : (tahunKe - 1)) ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIniAlwaysPremi = checkWithdrawalForLoyalty <= 0 ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;

                    if (product.ProductCategory != "maxpro")
                        loyaltyBonusTahunIniAlwaysPremi = loyaltyBonusTahunIni;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1 && !product.ProductCode.Equals("HLPREF"))
                    {
                        if (product.ProductCode.Equals("HLWIZ"))
                        {
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                        }
                        else
                        {
                            biayaAdminTahunIni = 0;
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                            biayaAsuransiTahunIni = 0 + biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred;
                        }

                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if ((tahunKe == 2 || tahunKe == 3) && !product.ProductCode.Equals("HLPREF"))
                        {
                            if (product.ProductCode.Equals("HLWIZ"))
                            {
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }
                            else
                            {
                                biayaAdminTahunIni *= 1.5m;
                                biayaAsuransiTahunIni += (biayaAsuransiTahunPertama / 2);
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }

                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        if (product.ProductCategory == "preferred" || product.ProductCategory == "maxpro")
                        {
                            if (iMonth == 1)
                            {
                                monthFundLow = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundMed = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundHigh = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundLowAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                                monthFundMedAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                                monthFundHighAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);
                            }
                            else if (iMonth == 12)
                            {
                                monthFundLow = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundMed = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundHigh = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundLowAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundMedAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundHighAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);
                            }
                            else
                            {
                                monthFundLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                                monthFundHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);
                            }
                        }
                        else
                        {
                            if (iMonth == 1)
                            {
                                monthFundLow = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                                monthFundMed = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                                monthFundHigh = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                                monthFundLowAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                                monthFundMedAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                                monthFundHighAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                            }
                            else if (iMonth == 12)
                            {
                                monthFundLow = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                                monthFundMed = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                                monthFundHigh = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);

                                monthFundLowAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                                monthFundMedAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                                monthFundHighAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);
                            }
                            else
                            {
                                monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                                monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                                monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                                monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                                monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                                monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                    biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                            }
                        }
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                InvestmentFundRow tahunKeInvestmentRow = null;

                if (product.ProductCode == "HLMAXPRO")
                {
                    tahunKeInvestmentRow = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && x.Tahun <= 5 && x.Low < 0);
                }

                tahunKeInvestmentRow = tahunKeInvestmentRow != null ? tahunKeInvestmentRow :
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            if (product.MinRemainingBalance.HasValue)
            {
                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    foreach (var withdrawal in dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false))
                    {
                        //validasi Wizer
                        //if(product.MinWithdrawalBalance.HasValue && product.ProductCode == "HLWIZ" && display)
                        //{
                        //    var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiAngsuran) * withdrawal.Year;
                        //    if (withdrawal.Year == 2)
                        //        totalPremi = 90 * totalPremi / 100;

                        //    var cekEstimasiInvestasi = Math.Min(totalPremi, product.MinWithdrawalBalance.Value);

                        //    var tahunAllCekWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year);
                        //    if (withdrawal.Year <= 2 && tahunAllCekWithdrawal != null && tahunAllCekWithdrawal.Low < cekEstimasiInvestasi)
                        //    {
                        //        display = false;
                        //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)cekEstimasiInvestasi) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        //    }

                        //    tahunAllCekWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Low < product.MinWithdrawalBalance);
                        //    if(withdrawal.Year > 2 && tahunAllCekWithdrawal != null && display)
                        //    {
                        //        display = false;
                        //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinWithdrawalBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        //    }
                        //}
                        // end

                        var remainingBalanceAfterWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Med < product.MinRemainingBalance);

                        if (remainingBalanceAfterWithdrawal != null && display)
                        {
                            display = false;
                            errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinRemainingBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        }
                    }
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderAndTopupWithdrawalInvest(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_topup_low"),
                    new DataColumn(fundType + "_alltime_topup_low"),

                    new DataColumn(fundType + "_topup_moderate"),
                    new DataColumn(fundType + "_alltime_topup_moderate"),

                    new DataColumn(fundType + "_topup_high"),
                    new DataColumn(fundType + "_alltime_topup_high"),

                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high"),

                    new DataColumn(fundType + "_alltime_death_low"),
                    new DataColumn(fundType + "_alltime_death_moderate"),
                    new DataColumn(fundType + "_alltime_death_high"),

                    new DataColumn(fundType + "_LoyaltyBonus")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;
            List<string> checkWithdrawalForLoyalty = new List<string>();
            List<string> checkWithdrawalForLoyaltyAlwaysPremi = new List<string>();
            int? loyaltyFirstYear = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().Year;
            int loyaltyYear = loyaltyFirstYear.Value;
            int? loyaltyPerc = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().LoyaltyBonusPerc;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                TopUpWithdrawal topupTahunIni = null;
                TopUpWithdrawal withdrawalTahunIni = null;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                    else if (product.ProductCode.Equals("HLPREF"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                // Hanwha Wizer cov term 70 year
                                if (product.ProductCode.Equals("HLWIZ"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;
                                else if (product.ProductCode.Equals("HLPREF"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermPreferred;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if ((riderItem.Rider.Category.Equals("Choice") && riderItem.Rider.RiderCode != "HHP") || product.ProductCategory == "preferred")
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    topupTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => x.IsTopUp && x.Year == tahunKe);
                    withdrawalTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => !x.IsTopUp && x.Year == tahunKe);
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalTopUpTahunIni = topupTahunIni != null ? CalculatorServices.CurrencyToDecimal(topupTahunIni.Amount) * (decimal)(allocationRate.RateSingleTopUp / 100) : 0;
                var totalWithdrawalTahunIni = withdrawalTahunIni != null ? CalculatorServices.CurrencyToDecimal(withdrawalTahunIni.Amount) : 0;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIni = topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIniAlwaysPremi = topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = MainServices.CalculateCostOfInsurance(dataInput, (i - 1));
                var premiAsuransiDasarTahunIniAllYear = premiAsuransiDasarTahunIni;

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                loyaltyBonusRateTahunIni = loyaltyYear == tahunKe ? loyaltyPerc : 0;
                supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                loyaltyYear = tahunKe == loyaltyYear ? tahunKe + 2 : loyaltyYear;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;

                if (topupTahunIni != null) drFund["TopupPremi"] = topupTahunIni.Amount;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;
                if (withdrawalTahunIni != null) drFund["Withdrawal"] = withdrawalTahunIni.Amount;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIniAlwaysPremi = checkWithdrawalForLoyaltyAlwaysPremi.Where(x => x.Contains(fundType)).Count() <= 0 && tahunKe >= loyaltyFirstYear ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIni = checkWithdrawalForLoyalty.Where(x => x.ToString() == fundType).Count() <= 0 && dataInput.Premi.RencanaMasaPembayaran >= (tahunKe) ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;
                    decimal lastYearInvestmentTopupLow = 0;
                    decimal lastYearInvestmentTopupLowAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupMed = 0;
                    decimal lastYearInvestmentTopupMedAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupHigh = 0;
                    decimal lastYearInvestmentTopupHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;


                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        lastYearInvestmentTopupLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_low"]);
                        lastYearInvestmentTopupMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_moderate"]);
                        lastYearInvestmentTopupHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_high"]);

                        lastYearInvestmentTopupLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_low"]);
                        lastYearInvestmentTopupMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_moderate"]);
                        lastYearInvestmentTopupHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? topupLow = 0;
                    decimal? topupMed = 0;
                    decimal? topupHigh = 0;
                    decimal? topupLowAllTime = 0;
                    decimal? topupMedAllTime = 0;
                    decimal? topupHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;
                    decimal? monthTopupLow = 0;
                    decimal? monthTopupMed = 0;
                    decimal? monthTopupHigh = 0;
                    decimal? monthTopupLowAllTime = 0;
                    decimal? monthTopupMedAllTime = 0;
                    decimal? monthTopupHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? lastMonthInvestmentTopupLow = 0;
                        decimal? lastMonthInvestmentTopupMed = 0;
                        decimal? lastMonthInvestmentTopupHigh = 0;
                        decimal? lastMonthInvestmentTopupLowAllTime = 0;
                        decimal? lastMonthInvestmentTopupMedAllTime = 0;
                        decimal? lastMonthInvestmentTopupHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;
                        decimal? totalAlokasiTopupBulanan = 0;
                        decimal? totalAlokasiTopupBulananAlwaysPremi = 0;
                        decimal withdrawalFund = totalWithdrawalTahunIni * fundAllocationInPoint;
                        decimal withdrawalPremi = 0;
                        decimal withdrawalTopup = 0;
                        decimal withdrawalPremiAlwaysPremi = 0;
                        decimal withdrawalTopupAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;

                            lastMonthInvestmentTopupLow = lastYearInvestmentTopupLow;
                            lastMonthInvestmentTopupMed = lastYearInvestmentTopupMed;
                            lastMonthInvestmentTopupHigh = lastYearInvestmentTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = lastYearInvestmentTopupLowAlwaysPremi;
                            lastMonthInvestmentTopupMedAllTime = lastYearInvestmentTopupMedAlwaysPremi;
                            lastMonthInvestmentTopupHighAllTime = lastYearInvestmentTopupHighAlwaysPremi;
                            drFund[fundType + "_LoyaltyBonus"] = loyaltyBonusTahunIni * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;

                            lastMonthInvestmentTopupLow = monthTopupLow;
                            lastMonthInvestmentTopupMed = monthTopupMed;
                            lastMonthInvestmentTopupHigh = monthTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = monthTopupLowAllTime;
                            lastMonthInvestmentTopupMedAllTime = monthTopupMedAllTime;
                            lastMonthInvestmentTopupHighAllTime = monthTopupHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                                totalAlokasiTopupBulanan = totalAlokasiTopupTahunIni * fundAllocationInPoint;
                                totalAlokasiTopupBulananAlwaysPremi = totalAlokasiTopupTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        if (iMonth == 1)
                        {
                            monthTopupLow = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupLow, totalAlokasiTopupBulanan, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                            monthTopupMed = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupMed, totalAlokasiTopupBulanan, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                            monthTopupHigh = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupHigh, totalAlokasiTopupBulanan, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                            monthTopupLowAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupLowAllTime, totalAlokasiTopupBulananAlwaysPremi, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                            monthTopupMedAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupMedAllTime, totalAlokasiTopupBulananAlwaysPremi, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                            monthTopupHighAllTime = CalculatorServices.CalculateFundWithTopupSupplementaryAndLoyalty(lastMonthInvestmentTopupHighAllTime, totalAlokasiTopupBulananAlwaysPremi, totalTopUpTahunIni, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                        }
                        else if (iMonth == 12)
                        {
                            withdrawalTopup = lastMonthInvestmentTopupLow >= withdrawalFund ? withdrawalFund : lastMonthInvestmentTopupLow.Value;
                            withdrawalPremi = withdrawalFund - withdrawalTopup;
                            withdrawalTopupAlwaysPremi = lastMonthInvestmentTopupLowAllTime >= withdrawalFund ? withdrawalFund : lastMonthInvestmentTopupLowAllTime.Value;
                            withdrawalPremiAlwaysPremi = withdrawalFund - withdrawalTopupAlwaysPremi;

                            if (withdrawalPremi > 0)
                                checkWithdrawalForLoyalty.Add(fundType);

                            if (withdrawalPremiAlwaysPremi > 0)
                                checkWithdrawalForLoyaltyAlwaysPremi.Add(fundType);

                            monthTopupLow = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupLow, totalAlokasiTopupBulanan, withdrawalTopup, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 100, 0, 0);

                            monthTopupMed = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupMed, totalAlokasiTopupBulanan, withdrawalTopup, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 100, 0, 0);

                            monthTopupHigh = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupHigh, totalAlokasiTopupBulanan, withdrawalTopup, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 100, 0, 0);

                            monthTopupLowAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupLowAllTime, totalAlokasiTopupBulananAlwaysPremi, withdrawalTopupAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 100, 0, 0);

                            monthTopupMedAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupMedAllTime, totalAlokasiTopupBulananAlwaysPremi, withdrawalTopupAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 100, 0, 0);

                            monthTopupHighAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentTopupHighAllTime, totalAlokasiTopupBulananAlwaysPremi, withdrawalTopupAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 100, 0, 0);
                        }
                        else
                        {

                            monthTopupLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLow, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                            monthTopupMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMed, totalAlokasiTopupBulanan, 0,
                                    0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                            monthTopupHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHigh, totalAlokasiTopupBulanan, 0,
                                    0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                            monthTopupLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLowAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                    0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                            monthTopupMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMedAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                    0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                            monthTopupHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHighAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                    0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);
                        }

                        monthFundLow = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, withdrawalPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundMed = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, withdrawalPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundHigh = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, withdrawalPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundLowAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, withdrawalPremiAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundMedAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, withdrawalPremiAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundHighAllTime = CalculatorServices.CalculateFundWithWithdrawalSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, withdrawalPremiAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal, supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    topupLow = monthTopupLow;
                    topupMed = monthTopupMed;
                    topupHigh = monthTopupHigh;

                    topupLowAllTime = monthTopupLowAllTime;
                    topupMedAllTime = monthTopupMedAllTime;
                    topupHighAllTime = monthTopupHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    drFund[fundType + "_topup_low"] = topupLow;
                    drFund[fundType + "_topup_moderate"] = topupMed;
                    drFund[fundType + "_topup_high"] = topupHigh;

                    drFund[fundType + "_alltime_topup_low"] = topupLowAllTime;
                    drFund[fundType + "_alltime_topup_moderate"] = topupMedAllTime;
                    drFund[fundType + "_alltime_topup_high"] = topupHighAllTime;
                    investmentFundRow.Low += fundLow.Value + topupLow.Value;
                    investmentFundRow.Med += fundMed.Value + topupMed.Value;
                    investmentFundRow.High += fundHigh.Value + topupHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value + topupLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value + topupMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value + topupHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            if (product.MinRemainingBalance.HasValue)
            {
                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    foreach (var withdrawal in dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false))
                    {

                        var remainingBalanceAfterWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Med < product.MinRemainingBalance);

                        if (remainingBalanceAfterWithdrawal != null && display)
                        {
                            display = false;
                            errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinRemainingBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        }
                    }
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }
        public static DataTable CalculateFundBenefitSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("Premi"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["Premi"] = premiSekaligus;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }


            if ((dataInput.Nasabah.TertanggungUtama == "Ya" && dataInput.Nasabah.UmurPemegangPolis <= (product.ChildMaxAge / 12)) ||
                (dataInput.Nasabah.TertanggungUtama != "Ya" && dataInput.Nasabah.UmurTertanggungUtama <= (product.ChildMaxAge / 12)))
            {
                if (CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) > product.MaxAmountSumInsuredChild)
                {
                    errorMsgList.Add("Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp " + CalculatorServices.DecimalToCurrency(product.MaxAmountSumInsuredChild.Value).ToString());
                }
            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("Premi"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                decimal biayaAsuransiTambahan = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;
                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;
                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["Premi"] = premiSekaligus;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi + biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                TopUpWithdrawal topupTahunIni = null;
                TopUpWithdrawal withdrawalTahunIni = null;
                decimal biayaAsuransiTambahan = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;
                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;
                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    topupTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => x.IsTopUp && x.Year == tahunKe);
                    withdrawalTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => !x.IsTopUp && x.Year == tahunKe);
                }

                var totalTopUpTahunIni = topupTahunIni != null ? CalculatorServices.CurrencyToDecimal(topupTahunIni.Amount) * (decimal)(allocationRate.RateSingleTopUp / 100) : 0;
                var totalWithdrawalTahunIni = withdrawalTahunIni != null ? CalculatorServices.CurrencyToDecimal(withdrawalTahunIni.Amount) : 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["PremiPerTahun"] = premiSekaligus + (dataInput.Nasabah.NamaProduk == "HLSIN" ? topupSekaligus : 0);
                if (topupTahunIni != null) drFund["TopupPremi"] = topupTahunIni.Amount;
                if (withdrawalTahunIni != null) drFund["Withdrawal"] = withdrawalTahunIni.Amount;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi + biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        if (iMonth == 1)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                        else if (iMonth == 12)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                            monthFundMed = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                            monthFundHigh = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);
                        }
                        else
                        {
                            monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            if (product.MinRemainingBalance.HasValue)
            {
                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    foreach (var withdrawal in dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false))
                    {
                        var remainingBalanceAfterWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Med < product.MinRemainingBalance);

                        if (remainingBalanceAfterWithdrawal != null && display)
                        {
                            display = false;
                            errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinRemainingBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        }
                    }
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWizer(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high"),

                    new DataColumn(fundType + "_alltime_death_low"),
                    new DataColumn(fundType + "_alltime_death_moderate"),
                    new DataColumn(fundType + "_alltime_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var totalPremiTambahanTahunIni = 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                        monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateBenefitEducation(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<ProductTahapanRate> tahapanList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            var period = dataInput.Premi.MasaAsuransi;
            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            decimal? akumulasiTahapanLastYear = 0;
            var insAge = 0;
            double interest = 6;
            decimal? premi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            int payTerm = 0;
            int caraBayar = 1;
            decimal rsvTahunLalu = 0;

            if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
            {
                payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            }
            else
            {
                caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
                payTerm = Convert.ToInt32(dataInput.Premi.PilihanMasaPembayaran);

                if (payTerm == 18)
                    payTerm = payTerm - dataInput.Nasabah.UmurAnak;
            }

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= (period + insAge); i++)
            {
                var tahunKe = i - insAge;
                var usiaAnak = dataInput.Nasabah.UmurAnak + tahunKe;
                decimal? lastYearRsv = 0;

                ProductAllocationRate allocationRate;
                ProductTahapanRate tahapanRate;
                CashValueRate cashValue;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == dataInput.Nasabah.NamaProduk);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaTertanggung"] = i;
                drFund["UsiaAnak"] = usiaAnak;
                drFund["UangPertanggungan"] = up;
                drFund["AkumulasiPremiTahunan"] = 0;

                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                {

                    drFund["PremiTahunan"] = tahunKe == 1 ? premi * caraBayar : null;
                }
                else
                {
                    if (dataInput.Premi.RencanaMasaPembayaran == 18)
                    {
                        drFund["PremiTahunan"] = usiaAnak <= dataInput.Premi.RencanaMasaPembayaran ? premi * caraBayar : null;
                    }
                    else
                    {
                        drFund["PremiTahunan"] = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premi * caraBayar : null;
                    }

                }

                tahapanRate = tahapanList.FirstOrDefault(x => x.Year == usiaAnak && x.ProductCode == dataInput.Nasabah.NamaProduk);
                decimal? danaTahapan = 0;

                if (tahapanRate != null)
                    danaTahapan = usiaAnak == tahapanRate.Year ? (decimal)(tahapanRate.RateTahapan / 100) * up : 0;

                drFund["DanaTahapan"] = danaTahapan;

                var akumulasiTahapanTahunIni = akumulasiTahapanLastYear * (1 + ((decimal)interest / 100)) + danaTahapan;
                akumulasiTahapanLastYear = akumulasiTahapanTahunIni;
                drFund["AkumulasiDanaTahapan"] = akumulasiTahapanTahunIni;

                cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == insAge && x.ChildAge == dataInput.Nasabah.UmurAnak && x.PaymentTerm == payTerm && x.Type == dataInput.Premi.ModeBayarPremi);
                decimal? cashValueTahunIni = 0;

                if (cashValue != null)
                    cashValueTahunIni = cashValue.CashValue * up / 10000000;

                drFund["CashValue"] = cashValueTahunIni;

                double factorX = 1 + (7.0 / 100);
                double factorY = 1 + (7.5 / 100);
                double factorZ = 40.0 / 100;

                if (tahunKe > 1)
                    lastYearRsv = Convert.ToDecimal(dtBenefit.Rows[dtBenefit.Rows.Count - 1]["Reserve"]);

                var rsvGuaranteed = cashValue != null ? cashValue.Reserve * (decimal)factorX : 0;
                var rsvAct = cashValue != null ? cashValue.Reserve * (decimal)factorY : 0;
                var delta = rsvAct - rsvGuaranteed;
                var rsv = delta * (decimal)factorZ;
                decimal reserveTahunIni = 0;
                decimal? fundReserve = 0;

                if (tahunKe > payTerm)
                {
                    reserveTahunIni = (decimal)lastYearRsv;
                    fundReserve = reserveTahunIni;
                }
                else
                {
                    reserveTahunIni = (decimal)rsvTahunLalu + (decimal)rsv;
                    fundReserve = reserveTahunIni * up / 10000000;
                }

                drFund["Reserve"] = fundReserve;
                rsvTahunLalu = (decimal)reserveTahunIni;
                var deathBenefit = dataInput.Premi.ModeBayarPremi == "berkala" ? (decimal)(allocationRate.RateDeathBenefit / 100) * up : up;
                drFund["DeathBenefit"] = deathBenefit;
                var total = deathBenefit + akumulasiTahapanTahunIni + fundReserve;
                drFund["Total"] = total;

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        public static DataTable CalculateBenefitKidsPlan(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<ProductTahapanRate> tahapanList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            var period = dataInput.Premi.MasaAsuransi;

            decimal? akumulasiTahapanLastYear = 0;
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            double interest = 0;
            decimal? premi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            int payTerm = 0;
            int caraBayar = 1;

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= (period + insAge); i++)
            {
                var tahunKe = i - insAge;
                var usiaAnak = dataInput.Nasabah.UmurAnak + tahunKe;

                ProductTahapanRate tahapanRate;
                CashValueRate cashValue;

                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaTertanggung"] = i;
                drFund["UsiaAnak"] = usiaAnak;
                drFund["UangPertanggungan"] = up;
                drFund["PremiTahunan"] = premi * caraBayar;
                akumulasiPremi = akumulasiPremi + (premi * caraBayar);
                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;

                tahapanRate = tahapanList.FirstOrDefault(x => x.Year == usiaAnak && x.ProductCode == dataInput.Nasabah.NamaProduk);
                decimal? danaTahapan = 0;

                if (tahapanRate != null)
                    danaTahapan = usiaAnak == tahapanRate.Year ? (decimal)(tahapanRate.RateTahapan / 100) * up : 0;

                drFund["DanaTahapan"] = danaTahapan;

                var akumulasiTahapanTahunIni = akumulasiTahapanLastYear * (1 + ((decimal)interest / 100)) + danaTahapan;
                akumulasiTahapanLastYear = akumulasiTahapanTahunIni;
                drFund["AkumulasiDanaTahapan"] = akumulasiTahapanTahunIni;

                cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == insAge && x.ChildAge == dataInput.Nasabah.UmurAnak);
                decimal? cashValueTahunIni = 0;

                if (cashValue != null)
                    cashValueTahunIni = cashValue.CashValue * up / 1000;

                drFund["CashValue"] = cashValueTahunIni;
                drFund["DeathBenefit"] = up;

                var total = up + akumulasiTahapanTahunIni;
                drFund["Total"] = total;

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        public static DataTable CalculateBenefitLifePro(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            var period = dataInput.Premi.MasaAsuransi;
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            var phAge = dataInput.Nasabah.UmurPemegangPolis;
            decimal interest = (decimal)produk.RatePengembalianPremi;
            decimal upNaik = (decimal)produk.RateUP;
            decimal? premi = CalculatorServices.CurrencyToDecimal(dataInput.Insurance.BiayaAsuransiTotal);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            int payTerm = 0;
            int caraBayar = 1;

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= (period + insAge); i++)
            {
                var tahunKe = i - insAge;
                CashValueRate cashValueTTU;
                CashValueRate cashValuePH;
                CashValueRate cashValueTT;
                decimal rateCashValue = 0;

                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaAnak"] = 0;
                drFund["UsiaTertanggung"] = i;
                drFund["UangPertanggungan"] = up;
                drFund["PremiTahunan"] = premi * caraBayar;
                akumulasiPremi = akumulasiPremi + (premi * caraBayar);
                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;

                cashValueTTU = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == insAge && x.Type == "NON_PA" && x.PaymentTerm == period);
                cashValuePH = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == phAge && x.Type == "PA" && x.PaymentTerm == period);
                rateCashValue += cashValuePH != null ? cashValuePH.CashValue.Value : 0;

                decimal? cashValueTahunIni = 0;
                decimal reserve = (decimal)((interest / 100) * akumulasiPremi);

                foreach (var ttg in dataInput.Premi.additionalInsured.TertanggungTambahan)
                {
                    cashValueTT = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == ttg.Age && x.Type == "PA" && x.PaymentTerm == period);
                    rateCashValue += cashValueTT != null ? cashValueTT.CashValue.Value : 0; ;
                }
                cashValueTahunIni = CalculatorServices.Round((decimal)((upAwal * (cashValueTTU.CashValue.Value / 1000)) + (produk.SantunanDuka.Value * (rateCashValue / 1000))), -1);

                drFund["CashValue"] = cashValueTahunIni;
                drFund["Reserve"] = tahunKe == period ? CalculatorServices.DecimalToCurrency(reserve) : "0";
                var naik = (upNaik / 100);
                up = up * (1 + naik);

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        //--add alif
        public static DataTable CalculateBenefitSavingPro(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            int caraBayar = 1;
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
            var period = dataInput.Premi.RencanaMasaPembayaran;
            var mpp = dataInput.Premi.RencanaMasaPembayaran;
            //--jangka panjang static untuk report
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            var phAge = dataInput.Nasabah.UmurPemegangPolis;
            decimal upNaik = (decimal)produk.RateUP;
            decimal? premi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala), -1);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            var ProductCode = "";
            int payTerm = 0;
            decimal? jatuhTempo = 0;
            decimal? Premitahunan = 0;
            decimal? cashValueTahunIni = 0;
            var tahunKe = 0;
            decimal? bagi = 0;
            decimal? PremiTahunan = 0;


            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            if (produk.ProductCode == "HLSAVING")
            {
                var per = cashValueList.Where(x => x.ProductCode == produk.ProductCode && x.PaymentTerm == payTerm && x.PMCode == caraBayar).Count();
                period = per;
            }
            for (var i = insAge; i < (period + insAge); i++)
            {
                tahunKe = i - insAge + 1;
                CashValueRate cashValue;
                PaymentMethod Factor;


                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaAnak"] = 0;
                drFund["UsiaTertanggung"] = i;
                drFund["UangPertanggungan"] = up;

                if (produk.ProductCode.Equals("HLSAVING"))
                {
                    ProductCode = "HLSAVING";
                }
                else
                {
                    ProductCode = "0";
                }

                Factor = PaymentMethodList.FirstOrDefault(x => x.PMCode == caraBayar && x.PM_ProductCode == ProductCode);

                if (ProductCode == "HLSAVING")
                {
                    bagi = Convert.ToDecimal(Factor.Factor);
                    PremiTahunan = CalculatorServices.Round((decimal)(premi / (bagi / 100)), 0);

                    cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm && x.PMCode == caraBayar);
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue * PremiTahunan), 0);

                }
                else
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm);
                    cashValueTahunIni = cashValue == null ? 0 : CalculatorServices.Round((decimal)(cashValue.CashValue * akumulasiPremi), 0);
                }

                if (ProductCode == "HLSAVING")
                {
                    if (tahunKe > mpp)
                    {
                        akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * mpp), 0);
                    }
                    else
                    {
                        akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * tahunKe), 0);
                    }
                }
                else
                {
                    akumulasiPremi = akumulasiPremi + premi;
                }

                if (ProductCode == "HLSAVING")
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm && x.ProductCode == ProductCode);
                    jatuhTempo = tahunKe == period ? CalculatorServices.Round((decimal)(cashValue.Reserve * PremiTahunan), -1) : 0;
                }
                else
                {
                    jatuhTempo = tahunKe == period ? CalculatorServices.Round((1 + (decimal)payTerm / 100) * (decimal)akumulasiPremi, -1) : 0;
                }

                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;
                drFund["CashValue"] = cashValueTahunIni;
                drFund["Reserve"] = jatuhTempo;
                if (ProductCode != "HLSAVING")
                {
                    var naik = (upNaik / 100);
                    up = up * (1 + naik);
                }
                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        //End add 

        //Add by Fitri 09/06/2020
        public static DataTable CalculateBenefitGoldenLifeA(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var on = new OnlineEntities();
            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            int caraBayar = 1;
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
            var period = dataInput.Premi.RencanaMasaPembayaran;
            var mpp = dataInput.Premi.RencanaMasaPembayaran;
            //--jangka panjang static untuk report
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            var phAge = dataInput.Nasabah.UmurPemegangPolis;
            //decimal upNaik = (decimal)produk.RateUP;
            decimal? premi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala), -1);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            var ProductCode = "";
            int payTerm = 0;
            decimal? jatuhTempo = 0;
            decimal? Premitahunan = 0;
            decimal? cashValueTahunIni = 0;
            var tahunKe = 0;
            decimal? bagi = 0;
            decimal? UangPertanggungan = up;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            var rate = on.ProductRates.Where(x => x.InsuredAge == insAge && x.ProductCode == dataInput.Nasabah.NamaProduk && x.MPP == dataInput.Premi.RencanaMasaPembayaran).FirstOrDefault();
            decimal? rates = Convert.ToDecimal(rate.Rate) / 1000;
            Premitahunan = Convert.ToDecimal(rates * up);


            period = 100 - insAge + 1;

            for (var i = insAge; i < (period + insAge); i++)
            {
                tahunKe = i - insAge + 1;
                CashValueRate cashValue;
                PaymentMethod Factor;


                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaAnak"] = 0;
                drFund["UsiaTertanggung"] = i;

                if (i < 65)
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm);
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue * Premitahunan), 0);

                }
                else if (i == 100)
                {
                    cashValueTahunIni = 0;
                }
                else
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.PaymentTerm == payTerm && x.InsuredAge == i);
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue * up), 0);
                }

                if (tahunKe > mpp)
                {
                    akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * mpp), 0);
                }
                else
                {
                    akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * tahunKe), 0);
                }

                if (i >= 70 && i < 100)
                {
                    UangPertanggungan = CalculatorServices.Round((decimal)up * 50 / 100, 0);
                }

                if (i == 100)
                {
                    akumulasiPremi = 0;
                    UangPertanggungan = 0;
                }

                if (i < 65)
                {
                    jatuhTempo = tahunKe == period ? CalculatorServices.Round((1 + (decimal)payTerm / 100) * (decimal)akumulasiPremi, -1) : 0;

                }
                if (i == 65)
                {
                    jatuhTempo = CalculatorServices.Round((decimal)akumulasiPremi, -1);
                }
                if (i == 70 || i == 100)
                {
                    jatuhTempo = CalculatorServices.Round((decimal)up * 50 / 100, 0);
                }
                else if (i > 65 && i != 65 && i != 70 && i != 100)
                {
                    jatuhTempo = 0;
                }

                drFund["UangPertanggungan"] = UangPertanggungan;
                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;
                drFund["CashValue"] = cashValueTahunIni;
                drFund["Reserve"] = jatuhTempo;

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }


        public static DataTable CalculateBenefitGoldenLifeB(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var on = new OnlineEntities();
            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            int caraBayar = 1;
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
            var period = dataInput.Premi.RencanaMasaPembayaran;
            var mpp = dataInput.Premi.RencanaMasaPembayaran;
            //--jangka panjang static untuk report
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            var phAge = dataInput.Nasabah.UmurPemegangPolis;
            //decimal upNaik = (decimal)produk.RateUP;
            decimal? premi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala), -1);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            var ProductCode = "";
            int payTerm = 0;
            decimal? jatuhTempo = 0;
            decimal? Premitahunan = 0;
            decimal? cashValueTahunIni = 0;
            var tahunKe = 0;
            decimal? bagi = 0;
            decimal? UangPertanggungan = up;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            var rate = on.ProductRates.Where(x => x.InsuredAge == insAge && x.ProductCode == dataInput.Nasabah.NamaProduk && x.MPP == dataInput.Premi.RencanaMasaPembayaran).FirstOrDefault();
            decimal? rates = Convert.ToDecimal(rate.Rate) / 1000;
            Premitahunan = Convert.ToDecimal(rates * up);


            period = 100 - insAge + 1;

            for (var i = insAge; i < (period + insAge); i++)
            {
                tahunKe = i - insAge + 1;
                CashValueRate cashValue;
                PaymentMethod Factor;


                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaAnak"] = 0;
                drFund["UsiaTertanggung"] = i;

                if (i < 65)
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm);
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue * Premitahunan), 0);

                }
                else if (i == 100)
                {
                    cashValueTahunIni = 0;
                }
                else
                {
                    cashValue = cashValueList.FirstOrDefault(x => x.PaymentTerm == payTerm && x.InsuredAge == i);
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue * up), 0);
                }

                if (tahunKe > mpp)
                {
                    akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * mpp), 0);
                }
                else
                {
                    akumulasiPremi = CalculatorServices.Round((decimal)(premi * caraBayar * tahunKe), 0);
                }


                if (i >= 65 && i < 70)
                {
                    UangPertanggungan = CalculatorServices.Round((decimal)up * 80 / 100, 0);
                }
                if (i >= 70 && i < 80)
                {
                    UangPertanggungan = CalculatorServices.Round((decimal)up * 70 / 100, 0);
                }
                if (i >= 80 && i < 90)
                {
                    UangPertanggungan = CalculatorServices.Round((decimal)up * 60 / 100, 0);
                }
                if (i >= 90 && i < 100)
                {
                    UangPertanggungan = CalculatorServices.Round((decimal)up * 50 / 100, 0);
                }

                if (i == 100)
                {
                    akumulasiPremi = 0;
                    UangPertanggungan = 0;
                }

                if (i < 65)
                {
                    jatuhTempo = tahunKe == period ? CalculatorServices.Round((1 + (decimal)payTerm / 100) * (decimal)akumulasiPremi, -1) : 0;

                }
                if (i == 65)
                {
                    jatuhTempo = CalculatorServices.Round((decimal)akumulasiPremi, -1);
                }
                if (i == 100)
                {
                    jatuhTempo = CalculatorServices.Round((decimal)up * 50 / 100, 0);
                }
                if (i > 65 && i != 65 && i != 100)
                {
                    jatuhTempo = 0;
                }

                drFund["UangPertanggungan"] = UangPertanggungan;
                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;
                drFund["CashValue"] = cashValueTahunIni;
                drFund["Reserve"] = jatuhTempo;

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        //End add
        public static DataTable CalculateBenefitFuturePro(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            int caraBayar = 1;
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
            var period = dataInput.Premi.RencanaMasaPembayaran;
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var insAge = 0;
            var phAge = dataInput.Nasabah.UmurPemegangPolis;
            decimal upNaik = (decimal)produk.RateUP;
            decimal? premi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) * caraBayar, -1);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            int payTerm = 0;
            decimal? jatuhTempo = 0;

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge; i < (period + insAge); i++)
            {
                var tahunKe = i - insAge + 1;
                CashValueRate cashValue;

                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaAnak"] = 0;
                drFund["UsiaTertanggung"] = i;
                drFund["UangPertanggungan"] = up;
                drFund["PremiTahunan"] = premi * caraBayar;

                cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm);

                decimal? cashValueTahunIni = cashValue == null ? 0 : CalculatorServices.Round((decimal)(cashValue.CashValue * akumulasiPremi), 0);
                akumulasiPremi = akumulasiPremi + premi;
                jatuhTempo = tahunKe == payTerm ? CalculatorServices.Round((1 + (decimal)payTerm / 100) * (decimal)akumulasiPremi, 0) : 0;

                drFund["AkumulasiPremiTahunan"] = akumulasiPremi;
                drFund["CashValue"] = cashValueTahunIni;
                drFund["Reserve"] = jatuhTempo;
                var naik = (upNaik / 100);
                up = up * (1 + naik);

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
        public static DataTable CalculateFundBenefitPrefferedMaxPro(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var totalPremiTambahanTahunIni = 0;

                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                if (product.ProductCategory == "preferred" || product.ProductCategory == "maxpro")
                {
                    loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                    loyaltyBonusRateTahunIni = loyaltyAndSupplementaryTahunIni == null ? 0 : loyaltyAndSupplementaryTahunIni.LoyaltyBonusPerc;
                    supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null && tahunKe <= 10 ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                }

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIni = dataInput.Premi.RencanaMasaPembayaran >= (tahunKe - 1) ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIniAlwaysPremi = (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala;

                    if (product.ProductCategory != "maxpro")
                        loyaltyBonusTahunIniAlwaysPremi = loyaltyBonusTahunIni;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1 && product.ProductCode == "HLMAXPRO")
                    {
                        biayaAdminTahunIni = 0;
                        biayaAsuransiTahunPertama = biayaAsuransiTahunIni;
                        biayaAsuransiTahunIni = 0;
                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);
                    }

                    if ((tahunKe == 2 || tahunKe == 3) && product.ProductCode == "HLMAXPRO")
                    {
                        biayaAdminTahunIni *= 1.5m;
                        biayaAsuransiTahunIni += (decimal)(biayaAsuransiTahunPertama / 2);
                        lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                InvestmentFundRow tahunKeInvestmentRow = null;

                if (product.ProductCode == "HLMAXPRO")
                {
                    tahunKeInvestmentRow = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && x.Tahun <= 5 && x.Low < 0);
                }

                tahunKeInvestmentRow = tahunKeInvestmentRow != null ? tahunKeInvestmentRow :
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            //if ((dataInput.Nasabah.TertanggungUtama == "Ya" && dataInput.Nasabah.UmurPemegangPolis <= (product.ChildMaxAge / 12)) ||
            //    (dataInput.Nasabah.TertanggungUtama != "Ya" && dataInput.Nasabah.UmurTertanggungUtama <= (product.ChildMaxAge / 12)))
            //{
            //    if (CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) > product.MaxAmountSumInsuredChild)
            //    {
            //        errorMsgList.Add("Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp " + CalculatorServices.DecimalToCurrency(product.MaxAmountSumInsuredChild.Value).ToString());
            //    }
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitInvest(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg, List<LoyaltySupplementary> loyaltyAndSupplementary)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_topup_low"),
                    new DataColumn(fundType + "_alltime_topup_low"),

                    new DataColumn(fundType + "_topup_moderate"),
                    new DataColumn(fundType + "_alltime_topup_moderate"),

                    new DataColumn(fundType + "_topup_high"),
                    new DataColumn(fundType + "_alltime_topup_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;
            int? loyaltyFirstYear = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().Year;
            int loyaltyYear = loyaltyFirstYear.Value;
            int? loyaltyPerc = loyaltyAndSupplementary.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().LoyaltyBonusPerc;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIni = topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100);
                var totalAlokasiTopupTahunIniAlwaysPremi = topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = MainServices.CalculateCostOfInsurance(dataInput, (i - 1));
                var premiAsuransiDasarTahunIniAllYear = premiAsuransiDasarTahunIni;

                var totalPremiTambahanTahunIni = 0;

                LoyaltySupplementary loyaltyAndSupplementaryTahunIni;
                int? loyaltyBonusRateTahunIni = 0;
                decimal? supplementaryFeeTahunIni = 0;

                loyaltyAndSupplementaryTahunIni = loyaltyAndSupplementary.Where(x => x.Year == tahunKe).FirstOrDefault();
                loyaltyBonusRateTahunIni = loyaltyYear == tahunKe ? loyaltyPerc : 0;
                supplementaryFeeTahunIni = loyaltyAndSupplementaryTahunIni != null ? loyaltyAndSupplementaryTahunIni.SupplementaryMonthtPerc : 0;
                loyaltyYear = tahunKe == loyaltyYear ? tahunKe + 2 : loyaltyYear;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);
                    var loyaltyBonusTahunIniAlwaysPremi = tahunKe >= loyaltyFirstYear ? (decimal)loyaltyBonusRateTahunIni / 100 * premiBerkala : 0;
                    var loyaltyBonusTahunIni = dataInput.Premi.RencanaMasaPembayaran >= (tahunKe) ? loyaltyBonusTahunIniAlwaysPremi : 0;

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupLow = 0;
                    decimal lastYearInvestmentTopupLowAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupMed = 0;
                    decimal lastYearInvestmentTopupMedAlwaysPremi = 0;

                    decimal lastYearInvestmentTopupHigh = 0;
                    decimal lastYearInvestmentTopupHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        lastYearInvestmentTopupLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_low"]);
                        lastYearInvestmentTopupMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_moderate"]);
                        lastYearInvestmentTopupHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_topup_high"]);

                        lastYearInvestmentTopupLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_low"]);
                        lastYearInvestmentTopupMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_moderate"]);
                        lastYearInvestmentTopupHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_topup_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? topupLow = 0;
                    decimal? topupMed = 0;
                    decimal? topupHigh = 0;
                    decimal? topupLowAllTime = 0;
                    decimal? topupMedAllTime = 0;
                    decimal? topupHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;
                    decimal? monthTopupLow = 0;
                    decimal? monthTopupMed = 0;
                    decimal? monthTopupHigh = 0;
                    decimal? monthTopupLowAllTime = 0;
                    decimal? monthTopupMedAllTime = 0;
                    decimal? monthTopupHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? lastMonthInvestmentTopupLow = 0;
                        decimal? lastMonthInvestmentTopupMed = 0;
                        decimal? lastMonthInvestmentTopupHigh = 0;
                        decimal? lastMonthInvestmentTopupLowAllTime = 0;
                        decimal? lastMonthInvestmentTopupMedAllTime = 0;
                        decimal? lastMonthInvestmentTopupHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;
                        decimal? totalAlokasiTopupBulanan = 0;
                        decimal? totalAlokasiTopupBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;

                            lastMonthInvestmentTopupLow = lastYearInvestmentTopupLow;
                            lastMonthInvestmentTopupMed = lastYearInvestmentTopupMed;
                            lastMonthInvestmentTopupHigh = lastYearInvestmentTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = lastYearInvestmentTopupLowAlwaysPremi;
                            lastMonthInvestmentTopupMedAllTime = lastYearInvestmentTopupMedAlwaysPremi;
                            lastMonthInvestmentTopupHighAllTime = lastYearInvestmentTopupHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;

                            lastMonthInvestmentTopupLow = monthTopupLow;
                            lastMonthInvestmentTopupMed = monthTopupMed;
                            lastMonthInvestmentTopupHigh = monthTopupHigh;
                            lastMonthInvestmentTopupLowAllTime = monthTopupLowAllTime;
                            lastMonthInvestmentTopupMedAllTime = monthTopupMedAllTime;
                            lastMonthInvestmentTopupHighAllTime = monthTopupHighAllTime;
                            loyaltyBonusTahunIni = 0;
                            loyaltyBonusTahunIniAlwaysPremi = 0;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                                totalAlokasiTopupBulanan = totalAlokasiTopupTahunIni * fundAllocationInPoint;
                                totalAlokasiTopupBulananAlwaysPremi = totalAlokasiTopupTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIni);

                        monthFundLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthFundHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), supplementaryFeeTahunIni, loyaltyBonusTahunIniAlwaysPremi);

                        monthTopupLow = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLow, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                        monthTopupMed = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMed, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                        monthTopupHigh = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHigh, totalAlokasiTopupBulanan, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);

                        monthTopupLowAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupLowAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), 0, 0);

                        monthTopupMedAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupMedAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), 0, 0);

                        monthTopupHighAllTime = CalculatorServices.CalculateFundWithSupplementaryAndLoyalty(lastMonthInvestmentTopupHighAllTime, totalAlokasiTopupBulananAlwaysPremi, 0,
                                0, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), 0, 0);
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    topupLow = monthTopupLow;
                    topupMed = monthTopupMed;
                    topupHigh = monthTopupHigh;

                    topupLowAllTime = monthTopupLowAllTime;
                    topupMedAllTime = monthTopupMedAllTime;
                    topupHighAllTime = monthTopupHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    drFund[fundType + "_topup_low"] = topupLow;
                    drFund[fundType + "_topup_moderate"] = topupMed;
                    drFund[fundType + "_topup_high"] = topupHigh;

                    drFund[fundType + "_alltime_topup_low"] = topupLowAllTime;
                    drFund[fundType + "_alltime_topup_moderate"] = topupMedAllTime;
                    drFund[fundType + "_alltime_topup_high"] = topupHighAllTime;

                    investmentFundRow.Low += fundLow.Value + topupLow.Value;
                    investmentFundRow.Med += fundMed.Value + topupMed.Value;
                    investmentFundRow.High += fundHigh.Value + topupHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value + topupLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value + topupMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value + topupHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }


        public static DataTable CalculateBenefitSmartCI(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<PaymentMethod> PaymentMethodList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var on = new OnlineEntities();
            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("AkumulasiPremiTahunan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            int caraBayar = 1;
            caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
            var period = dataInput.Premi.RencanaMasaPembayaran;
            var mpp = dataInput.Premi.RencanaMasaPembayaran;
            //--jangka panjang static untuk report
            var produk = obj.GetProduct(dataInput.Nasabah.NamaProduk);
            decimal? akumulasiPremi = 0;
            var ProductCodes = "HSCI";
            var insAge = 0;
            decimal? premi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala), -1);
            decimal? totalpremi = CalculatorServices.Round(CalculatorServices.CurrencyToDecimal(dataInput.Insurance.BiayaAsuransiTotal), -1);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? upAwal = up;
            int payTerm = 0;
            decimal? jatuhTempo = 0;
            decimal? cashValueTahunIni = 0;
            var tahunKe = 0;
            decimal? bagi = 0;
            decimal? PremiTahunan = 0;
            decimal? critical = 0;
            decimal? serious = 0;
            decimal? angioplasti = 0;

            payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            critical = CalculatorServices.Round((decimal)(up * 75 / 100), 0);
            serious = CalculatorServices.Round((decimal)(up * 25 / 100), 0);
            angioplasti = CalculatorServices.Round((decimal)(up * 20 / 100), 0);
            if (angioplasti > 100000000) { angioplasti = 100000000; }

            period = 80 - insAge + 1;

            for (var i = insAge; i < (period + insAge); i++)
            {
                tahunKe = i - insAge + 1;
                CashValueRate cashValue;


                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaTertanggung"] = i;

                cashValueTahunIni = 0;
                cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.PaymentTerm == payTerm && x.ProductCode == ProductCodes && x.InsuredAge == insAge);

                if (cashValue != null)
                    cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue / 1000 * up), 0);


                if (tahunKe > mpp)
                {
                    akumulasiPremi = CalculatorServices.Round((decimal)(totalpremi * caraBayar * mpp), 0);
                }
                else { akumulasiPremi = CalculatorServices.Round((decimal)(totalpremi * caraBayar * tahunKe), 0); }
                if (i == 80)
                {
                    jatuhTempo = CalculatorServices.Round((decimal)up * 50 / 100, 0);
                }
                else { jatuhTempo = 0; }

                if (i == 80)
                {
                    drFund["AkumulasiPremiTahunan"] = 0;
                    drFund["CashValue"] = 0;
                    drFund["DanaTahapan"] = 0;
                    drFund["AkumulasiDanaTahapan"] = 0;
                    drFund["Reserve"] = 0;
                    drFund["DeathBenefit"] = 0;
                }
                else
                {
                    drFund["AkumulasiPremiTahunan"] = akumulasiPremi;
                    drFund["CashValue"] = cashValueTahunIni;
                    drFund["DanaTahapan"] = critical;
                    drFund["AkumulasiDanaTahapan"] = serious;
                    drFund["Reserve"] = angioplasti;
                    drFund["DeathBenefit"] = up;
                }

                drFund["UangPertanggungan"] = up;
                drFund["Total"] = jatuhTempo;
                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }

        public static DataTable WithdrawalRate(List<ProductAllocationRate> productAllocationRates, Product product, bool isEnglish)
        {
            var dtAllocation = new DataTable { TableName = "AllocationRateWithdrawal" };

            dtAllocation.Columns.Add("Tahun");
            dtAllocation.Columns.Add("BiayaPartialWithdrawal");
            dtAllocation.Columns.Add("BiayaPenebusan");


            foreach (var rate in productAllocationRates)
            {
                var newRow = dtAllocation.NewRow();

                if (rate.Year == productAllocationRates.Max(x => x.Year).Value)
                    newRow["Tahun"] = rate.Year + (isEnglish ? " onward" : " dst");
                else
                    newRow["Tahun"] = rate.Year;

                newRow["BiayaPartialWithdrawal"] = rate.RateWithdrawal == 0 ? App_Data.Text.TidakDiperkenankan : (100 - rate.RateWithdrawal) + "%";
                newRow["BiayaPenebusan"] = rate.RateDeathBenefit + "%";

                dtAllocation.Rows.Add(newRow);
            }

            return dtAllocation;
        }

        public static DataTable LoyaltyRate(List<LoyaltySupplementary> LoyaltyRates, SummaryViewModel dataInput, Product product, bool isEnglish)
        {
            var dtLoyalty = new DataTable { TableName = "LoyaltyRate" };

            dtLoyalty.Columns.Add("Tahun");
            dtLoyalty.Columns.Add("Loyalty");
            dtLoyalty.Columns.Add("Loyalty2");

            var premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            int masaPembayaran = dataInput.Premi.RencanaMasaPembayaran;

            if (product.ProductCode == "HLMAXPRO")
            {
                var checkWithdrawalForLoyalty = dataInput.TopUp.TopupWithdrawals == null ? 0 : dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false && (x.Year) < LoyaltyRates[0].Year).Count();
                var checkWithdrawalForLoyalty2 = dataInput.TopUp.TopupWithdrawals == null ? 0 : dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false && (x.Year) < LoyaltyRates[1].Year).Count();

                var newRow1 = dtLoyalty.NewRow();
                newRow1["Tahun"] = masaPembayaran;
                newRow1["Loyalty"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty <= 0 && masaPembayaran >= LoyaltyRates[0].Year ? (decimal)LoyaltyRates[0].LoyaltyBonusPerc / 100 * premiBerkala : 0);
                newRow1["Loyalty2"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty2 <= 0 && masaPembayaran >= LoyaltyRates[1].Year ? (decimal)LoyaltyRates[1].LoyaltyBonusPerc / 100 * premiBerkala : 0);
                dtLoyalty.Rows.Add(newRow1);

                var newRow2 = dtLoyalty.NewRow();
                newRow2["Tahun"] = (product.CovAge / 12) - (dataInput.Nasabah.TertanggungUtama == "Ya" ? dataInput.Nasabah.UmurPemegangPolis : dataInput.Nasabah.UmurTertanggungUtama);
                newRow2["Loyalty"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty <= 0 ? (decimal)LoyaltyRates[0].LoyaltyBonusPerc / 100 * premiBerkala : 0);
                newRow2["Loyalty2"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty2 <= 0 ? (decimal)LoyaltyRates[1].LoyaltyBonusPerc / 100 * premiBerkala : 0);
                dtLoyalty.Rows.Add(newRow2);
            }
            else
            {
                var newRow1 = dtLoyalty.NewRow();
                newRow1["Tahun"] = isEnglish ? "At the end of (Year)" : "Akhir Tahun Ke -";
                newRow1["Loyalty"] = "Loyalty Bonus **)";
                dtLoyalty.Rows.Add(newRow1);

                foreach (var rate in LoyaltyRates.Where(x => x.LoyaltyBonusPerc > 0))
                {
                    var checkWithdrawalForLoyalty = dataInput.TopUp.TopupWithdrawals == null ? 0 : dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false && (x.Year + 1) <= rate.Year).Count();
                    var newRow = dtLoyalty.NewRow();

                    newRow["Tahun"] = rate.Year - 1;
                    newRow["Loyalty"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty <= 0 && dataInput.Premi.RencanaMasaPembayaran >= (rate.Year - 1) ? (decimal)rate.LoyaltyBonusPerc / 100 * premiBerkala : 0);

                    dtLoyalty.Rows.Add(newRow);
                }
            }

            return dtLoyalty;
        }

        public static DataTable LoyaltyRateInvest(DataTable dtFund, List<Fund> funds, List<LoyaltySupplementary> LoyaltyRates, SummaryViewModel dataInput, bool isEnglish)
        {
            var dtLoyalty = new DataTable { TableName = "LoyaltyRate" };

            dtLoyalty.Columns.Add("Tahun");
            dtLoyalty.Columns.Add("Loyalty");
            dtLoyalty.Columns.Add("Loyalty2");

            var premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);

            var newRow1 = dtLoyalty.NewRow();
            newRow1["Tahun"] = isEnglish ? "At the end of (Year)" : "Awal Tahun Ke -";
            newRow1["Loyalty"] = "Loyalty Bonus **)";
            dtLoyalty.Rows.Add(newRow1);

            var beginYear = LoyaltyRates.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().Year;

            for (var i = 0; i <= 2; i++)
            {
                var newRow = dtLoyalty.NewRow();
                var checkWithdrawalForLoyalty = dataInput.TopUp.TopupWithdrawals == null ? 0 : dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false && x.Year < beginYear).Count();
                decimal loyaltyAllFund = 0;

                newRow["Tahun"] = i == 2 ? isEnglish ? "onward" : "dst" : beginYear.ToString();
                //newRow["Loyalty"] = CalculatorServices.DecimalToCurrency(checkWithdrawalForLoyalty <= 0 && dataInput.Premi.RencanaMasaPembayaran >= beginYear ? (decimal)LoyaltyRates.Where(x => x.LoyaltyBonusPerc > 0).FirstOrDefault().LoyaltyBonusPerc / 100 * premiBerkala : 0);

                foreach (var fund in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    loyaltyAllFund += CalculatorServices.CurrencyToDecimal(dtFund.Rows[beginYear.Value - 1][fund.InvestmentCode.ToLower() + "_LoyaltyBonus"].ToString());
                }

                newRow["Loyalty"] = CalculatorServices.DecimalToCurrency(loyaltyAllFund);

                beginYear = beginYear + 2;
                dtLoyalty.Rows.Add(newRow);
            }

            return dtLoyalty;
        }


        public static DataTable DetailSmartCI(SummaryViewModel dataInput, List<CashValueRate> cashValueList, out string[] errorMsg)
        {
            errorMsg = null;
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            var on = new OnlineEntities();
            var dtRegular = new DataTable { TableName = "InvestBenefit" };

            dtRegular.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("Usia"),

                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopUp"),
                new DataColumn("Withdrawal"),

                new DataColumn("Rendah"),
                new DataColumn("Sedang"),
                new DataColumn("Tinggi"),

                new DataColumn("PremiAllTime"),
                new DataColumn("RendahAllTime"),
                new DataColumn("SedangAllTime"),
                new DataColumn("TinggiAllTime"),

                new DataColumn("MatiRendah"),
                new DataColumn("MatiSedang"),
                new DataColumn("MatiTinggi"),

                new DataColumn("MatiRendahAllTime"),
                new DataColumn("MatiSedangAllTime"),
                new DataColumn("MatiTinggiAllTime")
            });


            if (dataInput == null) return dtRegular;

            var insAge = 0;
            decimal critical = 0;
            decimal serious = 0;
            decimal angioplasti = 0;
            decimal totalcritical = 0;
            decimal manfaatakhir = 0;
            decimal cashValueTahunIni = 0;
            decimal totalmanfaat = 0;
            decimal up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            int payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            CashValueRate cashValue;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var rider = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Count();
            critical = CalculatorServices.Round((decimal)(up * 75 / 100), 0);
            serious = CalculatorServices.Round((decimal)(up * 25 / 100), 0);
            angioplasti = CalculatorServices.Round((decimal)(up * 20 / 100), 0);
            if (angioplasti > 100000000) { angioplasti = 100000000; }

            manfaatakhir = CalculatorServices.Round((decimal)up * 50 / 100, 0);
            totalcritical = CalculatorServices.Round((decimal)(critical - angioplasti - serious), 0);

            cashValueTahunIni = 0;
            cashValue = cashValueList.FirstOrDefault(x => x.Year == 14 && x.PaymentTerm == payTerm && x.ProductCode == "HSCI" && x.InsuredAge == insAge);

            if (cashValue != null)
                cashValueTahunIni = CalculatorServices.Round((decimal)(cashValue.CashValue / 1000 * up), 0);

            totalmanfaat = CalculatorServices.Round((decimal)(manfaatakhir + angioplasti + serious + totalcritical), 0);

            var newRow = dtRegular.NewRow();
            newRow["Tahun"] = CalculatorServices.DecimalToCurrency(angioplasti);
            newRow["Usia"] = CalculatorServices.DecimalToCurrency(serious);
            newRow["UangPertanggungan"] = CalculatorServices.DecimalToCurrency(critical);
            newRow["PremiPerTahun"] = CalculatorServices.DecimalToCurrency(totalcritical);
            newRow["TopUp"] = CalculatorServices.DecimalToCurrency(manfaatakhir);
            newRow["Withdrawal"] = CalculatorServices.DecimalToCurrency(up);
            newRow["Rendah"] = CalculatorServices.DecimalToCurrency(cashValueTahunIni);
            newRow["Sedang"] = CalculatorServices.DecimalToCurrency(totalmanfaat);
            if (dataInput.Nasabah.TertanggungUtama == "Ya")
            {
                newRow["Tinggi"] = (dataInput.Nasabah.JenisKelaminPemegangPolis == "Pria" ? App_Data.Text.Bapak : App_Data.Text.Ibu) + " " + dataInput.Nasabah.NamaPemegangPolis;
                newRow["PremiAllTime"] = dataInput.Nasabah.UmurPemegangPolis + " " + App_Data.Text.TahunUmurSpasi;
            }
            else
            {
                newRow["Tinggi"] = (dataInput.Nasabah.JenisKelaminPemegangPolis == "Pria" ? App_Data.Text.Bapak : App_Data.Text.Ibu) + " " + dataInput.Nasabah.NamaTertanggungUtama;
                newRow["PremiAllTime"] = dataInput.Nasabah.UmurTertanggungUtama + " " + App_Data.Text.TahunUmurSpasi;
            }

            newRow["RendahAllTime"] = dataInput.Premi.RencanaMasaPembayaran + " " + App_Data.Text.TahunJamak;

            dtRegular.Rows.Add(newRow);

            return dtRegular;
        }
    }
}
