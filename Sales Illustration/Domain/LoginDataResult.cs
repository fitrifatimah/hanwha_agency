﻿namespace Sales.Illustration.Web.Domain
{
    public class LoginDataResult
    {
        public int result_code { get; set; }

        public LoginData data { get; set; }
        
    }

    public class apiInvitation
    {
        public int result_code { get; set; }
        public string message { get; set; }
    }
    public class LoginResultCode
    {
        public static int AGENT_CODE_NOT_EXIST = 1;
        public static int WRONG_PASSWORD = 2;
        public static int TERMINATE = 3;
        public static int SUCCESS = 0;
    }

    public class LoginData
    {
        public string agent_code { get; set; }
        public string agent_name { get; set; }
        public string license_aaji { get; set; }
        public string join_date { get; set; }
        public string enc_pass { get; set; }
        public string license_expiry_date { get; set; }
        public string email { get; set; }
        public string roleuser { get; set; }
    }
}