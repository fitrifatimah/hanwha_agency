﻿using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class ProfileViewModel
    {
        [DisplayName("Nama")]
        public string Name { get; set; }
        [DisplayName("Kode Agen")]
        public string AgentCode { get; set; }
        [DisplayName("Join Date")]
        public string JoinDate { get; set; }
        [DisplayName("AAJI Lisensi")]
        public string LicenseNo { get; set; }
        //public string AgentType { get; set; }
    }
}