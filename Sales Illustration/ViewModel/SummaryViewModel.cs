﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Sales.Illustration.Web.Services;

namespace Sales.Illustration.Web.ViewModel
{
    public class SummaryViewModel
    {
        public NasabahDataViewModel Nasabah { get; set; }
        public PremiumAndInvestmentViewModel Premi { get; set; }
        public RiderViewModel Rider { get; set; }
        public TopUpWithdrawalViewModel TopUp { get; set; }
        public AdditionalInsuredViewModel Additional { get; set; }
        public InsuranceCostAndRatioViewModel Insurance { get; set; }

        public string StrCurrency { get { return (Premi.MataUang == "IDR") ? "Rp. " : "Rp. "; } }

        public DateTime nearestBirthday
        {
            get
            {
                var dates = new List<DateTime>();

                dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirPemegangPolis, "dd/MM/yyyy", null));
                if (Nasabah.TanggalLahirTertanggungUtama != null)
                {
                    if (Nasabah.TanggalLahirTertanggungUtama.Length > 1)
                    {
                        dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", null));
                    }
                }

                if (Nasabah.TanggalLahirAnak != null)
                {
                    dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirAnak, "dd/MM/yyyy", null));
                }

                if (Additional != null)
                {
                    if (Additional.TertanggungTambahan != null)
                    {
                        if (Additional.TertanggungTambahan.Count != 0)
                        {
                            foreach (var item in Additional?.TertanggungTambahan)
                            {
                                dates.Add(DateTime.ParseExact(item.DateOfBirth, "dd/MM/yyyy", null));
                            }
                        }
                    }
                }

                return dates.Select(CalculatorServices.CalculateNearestBirthday).Select(c => DateTime.ParseExact(c.Nearest, "dd/MM/yyyy", null)).OrderBy(c => c).First();
            }
        }

        public DateTime latestBirthday
        {
            get
            {
                var dates = new List<DateTime>();

                dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirPemegangPolis, "dd/MM/yyyy", null));
                if (Nasabah.TanggalLahirTertanggungUtama != null)
                {
                    if (Nasabah.TanggalLahirTertanggungUtama.Length > 1)
                    {
                        dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", null));
                    }
                }

                if (Nasabah.TanggalLahirAnak != null)
                {
                    dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirAnak, "dd/MM/yyyy", null));
                }

                if (Additional != null)
                {
                    if (Additional.TertanggungTambahan != null)
                    {
                        if (Additional.TertanggungTambahan.Count != 0)
                        {
                            foreach (var item in Additional?.TertanggungTambahan)
                            {
                                dates.Add(DateTime.ParseExact(item.DateOfBirth, "dd/MM/yyyy", null));
                            }
                        }
                    }
                }

                return dates.Select(CalculatorServices.CalculateLatestBirthday).Select(c => DateTime.ParseExact(c.Latest, "dd/MM/yyyy", null)).OrderBy(c => c).First();
            }
        }


        public DateTime nearestBirthdayTTU
        {
            get
            {
                var dates = new List<DateTime>();

                if (Nasabah.TanggalLahirTertanggungUtama == null)
                {
                    Nasabah.TanggalLahirTertanggungUtama = Nasabah.TanggalLahirPemegangPolis;
                }
                dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", null));

                if (Nasabah.TanggalLahirAnak != null)
                {
                    dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirAnak, "dd/MM/yyyy", null));
                }

                if (Additional != null)
                {
                    if (Additional.TertanggungTambahan != null)
                    {
                        if (Additional.TertanggungTambahan.Count != 0)
                        {
                            foreach (var item in Additional?.TertanggungTambahan)
                            {
                                dates.Add(DateTime.ParseExact(item.DateOfBirth, "dd/MM/yyyy", null));
                            }
                        }
                    }
                }
                return dates.Select(CalculatorServices.CalculateNearestBirthday).Select(c => DateTime.ParseExact(c.Nearest, "dd/MM/yyyy", null)).OrderBy(c => c).First();
            }
        }


        public DateTime latestBirthdayTTU
        {
            get
            {
                var dates = new List<DateTime>();

                if (Nasabah.TanggalLahirTertanggungUtama == null)
                {
                    Nasabah.TanggalLahirTertanggungUtama = Nasabah.TanggalLahirPemegangPolis;
                }
                dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirTertanggungUtama, "dd/MM/yyyy", null));

                if (Nasabah.TanggalLahirAnak != null)
                {
                    dates.Add(DateTime.ParseExact(Nasabah.TanggalLahirAnak, "dd/MM/yyyy", null));
                }

                if (Additional != null)
                {
                    if (Additional.TertanggungTambahan != null)
                    {
                        if (Additional.TertanggungTambahan.Count != 0)
                        {
                            foreach (var item in Additional?.TertanggungTambahan)
                            {
                                dates.Add(DateTime.ParseExact(item.DateOfBirth, "dd/MM/yyyy", null));
                            }
                        }
                    }
                }
                return dates.Select(CalculatorServices.CalculateLatestBirthday).Select(c => DateTime.ParseExact(c.Latest, "dd/MM/yyyy", null)).OrderBy(c => c).First();
            }
        }

    }


 
}