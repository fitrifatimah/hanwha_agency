﻿using Sales.Illustration.Web.Domain;
using System.Collections.Generic;
using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class AdditionalInsuredViewModel
    {
        public List<AdditionalInsured> TertanggungTambahan { get; set; }
        public List<RiderItem> Riders { get; set; }
        public List<RiderItem> RidersTertanggungUtama { get; set; }
        public List<RiderItem> RiderNoData { get; set; }
        public NasabahDataViewModel NasabahData { get; set; }
        public PremiumAndInvestmentViewModel DataPremi { get; set; }
        public string BiayaAsuransiTertanggungTambahan { get; set; }
    }
    public class AdditionalInsuredTraditionalViewModel
    {
        public List<AdditionalInsured> TertanggungTambahan { get; set; }
        public NasabahDataViewModel NasabahData { get; set; }
    }
}