﻿using Sales.Illustration.Web.Domain;
using System.Collections.Generic;
using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class RiderViewModel
    {
        public string BiayaAsuransiTambahan { get; set; }
        public string TotalBiayaPremiAsuransi { get; set; }
        public List<RiderItem> Riders { get; set; }
        public string ddlRiderCoverage { get; set; }
        public NasabahDataViewModel NasabahData { get; set; }

    }
}