﻿using Sales.Illustration.Web.Domain;
using System.Collections.Generic;
using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class TopUpWithdrawalViewModel
    {
        public List<TopUpWithdrawal> TopupWithdrawals { get; set; }
    }
}