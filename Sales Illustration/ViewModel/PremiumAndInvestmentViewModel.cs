﻿using Sales.Illustration.Web.Domain;
using System.Collections.Generic;
using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class PremiumAndInvestmentViewModel
    {
        public string MataUang { get; set; }
        public string CaraBayar { get; set; }
        public int RencanaMasaPembayaran { get; set; }
        public string PremiBerkala { get; set; }
        public string PremiInpatient { get; set; }
        public string PremiOutpatient { get; set; }
        public string PremiTahunan { get; set; }
        public string TopupBerkala { get; set; }
        public string PremiAngsuran { get; set; }
        public string UangPertanggungan { get; set; }
        public string BiayaAsuransi { get; set; }

        public string PilihanMasaPembayaran { get; set; }
        public string ModeBayarPremi { get; set; }
        public int? UsiaMasukAnak { get; set; }
        public int? MasaAsuransi { get; set; }

        public int? UnitWizer { get; set; }
        public string JenisPlan { get; set; }
        public string ManfaatRawatJalan { get; set; }
        public string AgentType { get; set; }
        public List<Investment> Investments
        {
            get; set;
        }
        public int? TotalInvestment { get; set; }
        public AdditionalInsuredTraditionalViewModel additionalInsured { get; set; }
    }
}