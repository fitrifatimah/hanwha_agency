﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Sales.Illustration.Web.ViewModel
{
    public class NasabahDataViewModel
    {
        public string AgentType { get; set; }
        public string NamaPemegangPolis { get; set; }
        public string TanggalLahirPemegangPolis { get; set; }
        public string JenisKelaminPemegangPolis { get; set; }
        public string StatusMerokokPemegangPolis { get; set; }
        public string StatusPekerjaanPemegangPolis { get; set; }
        public string KelasPekerjaanPemegangPolis { get; set; }
        public int UmurPemegangPolis { get; set; }

        public string TertanggungUtama { get; set; }

        public string NamaTertanggungUtama { get; set; }
        public string TanggalLahirTertanggungUtama { get; set; }
        public string JenisKelaminTertanggungUtama { get; set; }
        public string StatusMerokokTertanggungUtama { get; set; }
        public string StatusPekerjaanTertanggungUtama { get; set; }
        public string KelasPekerjaanTertanggungUtama { get; set; }
        public int? UmurTertanggungUtama { get; set; }

        public string Relation { get; set; }

        public string NamaAnak { get; set; }
        public string TanggalLahirAnak { get; set; }
        public string JenisKelaminAnak { get; set; }
        public int UmurAnak { get; set; }

        public string TipeProduk { get; set; }
        public string NamaProduk { get; set; }

        //add 25012020
        public Nullable<System.DateTime> nearestBirthday
        {
            get; set;
        }

        //add 25012020
        public Nullable<System.DateTime> nearestBirthdayTTU
        {
            get; set;
        }
        
    }
}