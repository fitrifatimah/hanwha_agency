﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.ViewModel
{
    public class SendApiInvitationViewModel
    {
        public string CustomerAddress { get; set; }
        [Phone]
        public string Phone { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public string Body { get; set; }
        public string Source { get; set; }
    }
}