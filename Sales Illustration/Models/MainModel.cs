﻿using Newtonsoft.Json;
using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Models
{
    public class MainModel : Controller
    {
        //
        // GET: /MainModel/
        GeneralModel general = new GeneralModel();
        UserMembershipModel user = new UserMembershipModel();
        ProductModel product = new ProductModel();
        RiderModel rider = new RiderModel();
        TransLogModel trans = new TransLogModel();

        #region General
        public IEnumerable<Fund> GetActiveFund()
        {
            return general.GetActiveFund();
        }

        public IEnumerable<PaymentMethod> GetActivePaymentMethod(string productCode)//disini
        {
            return general.GetActivePaymentMethod(productCode);
        }

        public IEnumerable<Currency> GetActiveCurrency()
        {
            return general.GetActiveCurrency();
        }

        public IEnumerable<Language> GetActiveLanguage()
        {
            return general.GetActiveLanguage();
        }

        public IEnumerable<RiskClass> GetActiveRiskClass()
        {
            return general.GetActiveRiskClass();
        }

        public RiskClass GetRiskClass(int id)
        {
            return general.GetRiskClass(id);
        }

        public TraditionalRate GetTraditionalRate(string type, int childAge, int insAge, int term, int period)
        {
            return general.GetTraditionalRate(type, childAge, insAge, term, period);
        }

        public ProductRate GetProductRate(string productCode, int? childAge, int insAge, int? mpp)
        {
            return general.GetProductRate(productCode, childAge, insAge, mpp);
        }
        public TraditionalRiderRate GetTraditionalRiderRate(string code, int? riderType, string type, int? insAge, int? basicCov, int? basicTerm, int? riderCov, int? riderTerm)
        {
            return general.GetTraditionalRiderRate(code, riderType, type, insAge, basicCov, basicTerm, riderCov, riderTerm);
        }

        public IEnumerable<Fund> GetFundByProduct(string code)
        {
            return general.GetFundByProduct(code);
        }

        public ProductPaymentMethodPremium GetProductPaymentMethodPremium(string productCode, int pmCode, string type)
        {
            return general.GetProductPaymentMethodPremium(productCode, pmCode, type);
        }

        public PaymentMethod GetPaymentMethod(int code, string productcode)
        {
            return general.GetPaymentMethod(code, productcode);
        }

        public RiderRate GetRiderRate(string code, int age, string category, int mpp)
        {
            return general.GetRiderRate(code, age, category, mpp);
        }

        public MinimalPremi GetMinimalPremi(string productCode, int code, int mpp)
        {
            return general.GetMinimalPremi(productCode,code, mpp);
        }

        public SumInsuredRate GetSumInsured(string productCode, int insage, int mpp)
        {
            return general.GetSumInsured(productCode, insage, mpp);
        }

        public AnnuityFactor GetAnnuityFactor(int? year, int? factor, string productType)
        {
            return general.GetAnnuityFactor(year, factor, productType);
        }

        public RiderRate GetUnitLinkRate(string riderCode, int? riderTypeId, int? age, int? riskClass, string category)
        {
            return general.GetUnitLinkRate(riderCode, riderTypeId, age, riskClass, category);
        }

        public Language GetLanguage(string code)
        {
            return general.GetLanguage(code);
        }

        public void CreateLog(string agentCode, string agentName)
        {
            general.CreateLog(agentCode,agentName);
        }
        public SAMultiplier GetSAMultiplier(string productCode, int age, string type)
        {
            return general.GetSAMultiplier(productCode, age, type);
        }
        public TarifPremi GetTarifPremi(string productCode, string type,int? age,int? term, int? planValue)
        {
            return general.GetTarifPremi(productCode, type, age, term, planValue);
        }

        public Relation GetRelation(string relationCode)
        {
            return product.GetRelation(relationCode);
        }
        public InfoMedical GetInfoMedical(int age, decimal sumInsured, string productType)
        {
            return general.GetInfoMedical(age, sumInsured, productType);
        }
        public TrigerSumInsured GetTrigerSumInsured(int age, string productCode)
        {
            return general.GetTrigerSumInsured(age, productCode);
        }
        #endregion

        #region UserMembership
        public LoginDataResult LoginOnlie(string username, string password)
        {
            LoginDataResult dataResult;

            using (var wc = new WebClient())
            {
                var url = ConfigurationManager.AppSettings["RemoteAuthUrl"];
                var json =
                    wc.DownloadString(string.Format("{2}/agent_login?username={0}&password={1}",
                        HttpUtility.UrlEncode(username), HttpUtility.UrlEncode(password), url));
                dataResult = JsonConvert.DeserializeObject<LoginDataResult>(json);
                if (dataResult.data != null)
                {
                    if (dataResult.data.license_aaji is null)
                    {
                        dataResult.data.license_aaji = "";
                    }
                }
            }

            return dataResult;
        }

        public LoginDataResult LoginOffline(string username, string password)
        {
            var rs = user.CheckLogin(username, password);

            LoginDataResult result = rs != null ? new LoginDataResult
            {
                result_code = LoginResultCode.SUCCESS,
                data = new LoginData
                {
                    agent_code = rs.AgentCode,
                    agent_name = rs.AgentName,
                    license_aaji = rs.AAJILicense,
                    join_date = String.Format("{0:dd MMM yyyy}", rs.JoinDate),
                    license_expiry_date = String.Format("{0:dd MMM yyyy}", rs.ExpiredDate),
                    roleuser = rs.RoleUser
                }
            } : null;

            if (result != null && result.data.license_aaji == null)
            {
                result.data.license_aaji = "";
            } 

            return result;
        }

        public LoginDataResult LoginOfflineUAT(string username)
        {
            using (OnlineEntities on = new OnlineEntities())
            {
                UserMembershipModel user = new UserMembershipModel();
                if (username.ToString().Length > 10)
                    username = username.ToString().Replace(".", "").Substring(0, 10);
       
                var rs = on.UserMemberships.Where(x => x.AgentCode == username && x.IsActive == true).FirstOrDefault();

                LoginDataResult result = rs != null ? new LoginDataResult
                {
                    result_code = LoginResultCode.SUCCESS,
                    data = new LoginData
                    {
                        agent_code = rs.AgentCode,
                        agent_name = rs.AgentName,
                        license_aaji = rs.AAJILicense,
                        join_date = String.Format("{0:dd MMM yyyy}", rs.JoinDate),
                        license_expiry_date = String.Format("{0:dd MMM yyyy}", rs.ExpiredDate),
                        roleuser = rs.RoleUser
                    }
                } : null;

                if (result != null && result.data.license_aaji == null)
                {
                    result.data.license_aaji = "";
                }

                return result;
            }
                
        }


        public void UserAdd(UserMembership data)
        {
            user.UserAdd(data);
        }

        public void UserEdit(UserMembership data)
        {
            user.UserEdit(data);
        }
        public void UserEditApi(UserMembership data)
        {
            user.UserEditApi(data);
        }
        #endregion

        #region Product
        public IEnumerable<Product> GetActiveProduct()
        {
            return product.GetActiveProduct();
        }

        public IEnumerable<Product> GetProductByType(string type)
        {
            return product.GetProductByType(type);
        }

        public IEnumerable<ProductRelation> GetRelationByProduct(string productCode, string type)
        {
            return product.GetRelationByProduct(productCode, type);
        }

        public Product GetProduct(string code)
        {
            return product.GetProduct(code);
        }

        public IEnumerable<ProductPlan> GetProductPlan(string code)
        {
            return product.GetProductPlan(code);
        }

        public ProductPlan GetProductPlanbyProductCode(string code, string planCode)
        {
            return product.GetProductPlanbyProductCode(code,planCode);
        }
        #endregion

        #region Rider
        public IEnumerable<Rider> GetActiveRider()
        {
            return rider.GetActiveRider();
        }

        public IEnumerable<Rider> GetRiderByProduct(string productCode)
        {
            return rider.GetRiderByProduct(productCode);
        }

        public IEnumerable<RiderProduct> GetRiderProduct(string productCode, string riderCode)
        {
            return rider.GetRiderProduct(productCode, riderCode);
        }

        public UnitMapping GetUnitMapping(string riderCode, int? riderType, decimal up)
        {
            return rider.GetUnitMapping(riderCode, riderType, up);
        }

        public UnitMapping GetMaxUnitMapping(string riderCode, int? riderType, decimal up)
        {
            return rider.GetMaxUnitMapping(riderCode, riderType, up);
        }

        public SantunanHarian GetSantunanHarian(string riderCode, int riderType)
        {
            return rider.GetSantunanHarian(riderCode, riderType);
        }
        #endregion

        #region TransLog
        public TransLog GetTransLog(string code)
        {
            return trans.GetTransLog(code);
        }

        public IEnumerable<TransLog> GetTransLogByAgent(string agentCode)
        {
            return trans.GetTransLogByAgent(agentCode);
        }

        public void AddTransLog(TransLog data)
        {
            trans.AddTransLog(data);
        }

        public void EditTransLog(TransLog data)
        {
            trans.EditTransLog(data);
        }

        public void AddNasabahData(NasabahData data)
        {
            trans.AddNasabahData(data);
        }

        public void EditNasabahData(NasabahData data)
        {
            trans.EditNasabahData(data);
        }

        public void AddPremiumData(PremiumData data)
        {
            trans.AddPremiumData(data);
        }

        public void EditPremiumData(PremiumData data)
        {
            trans.EditPremiumData(data);
        }

        public void AddInvestmentData(InvestmentData data)
        {
            trans.AddInvestmentData(data);
        }

        public void DeleteInvestmentData(InvestmentData data)
        {
            trans.DeleteInvestmentData(data);
        }

        public void AddRiderData(RiderData data)
        {
            trans.AddRiderData(data);
        }

        public void DeleteRiderData(string transCode)
        {
            trans.DeleteRiderData(transCode);
        }

        public void AddTopUpWithdrawalData(TopUpWithdrawalData data)
        {
            trans.AddTopUpWithdrawalData(data);
        }

        public void DeleteTopUpWithdrawalData(TopUpWithdrawalData data)
        {
            trans.DeleteTopUpWithdrawalData(data);
        }
        public void DeleteTopUpWithdrawalData(string transcode)
        {
            trans.DeleteTopUpWithdrawalData(transcode);
        }

        public void AddAdditionalInsuredData(AdditionalInsuredData data)
        {
            trans.AddAdditionalInsuredData(data);
        }

        public void DeleteAdditionalInsuredData(AdditionalInsuredData data)
        {
            trans.DeleteAdditionalInsuredData(data);
        }

        public void DeleteAdditionalInsuredData(string transCode)
        {
            trans.DeleteAdditionalInsuredData(transCode);
        }

        public AdditionalInsuredData GetAdditionalInsuredData(int y,string transcode, string name, DateTime? dob, string gender, int? riskClass, int? age, string relation)
        {
            return trans.GetAdditionalInsuredData(y,transcode, name, dob, gender, riskClass, age, relation);
        }

        public void AddAdditionalInsuredRiderData(AdditionalInsuredRiderData data)
        {
            trans.AddAdditionalInsuredRiderData(data);
        }

        public void DeleteAdditionalInsuredRiderData(AdditionalInsuredRiderData data)
        {
            trans.DeleteAdditionalInsuredRiderData(data);
        }
        #endregion

        #region Log
        public IEnumerable<Log> GetLogHistory(string monthyear)
        {
            return general.GetLogHistory(monthyear);
        }

        #endregion

    }
}
