﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc; 

namespace Sales.Illustration.Web.Models
{
    public class GeneralModel : Controller
    {
        //
        // GET: /GeneralModel/

        private OnlineEntities on = new OnlineEntities();
        private OfflineEntities off = new OfflineEntities();
        private Sales.Illustration.Web.Helper.Generator gen = new Sales.Illustration.Web.Helper.Generator();

        #region Currency
        public IEnumerable<Currency> GetAllCurrency()
        {
            if (_session.AppMode == "offline")
                return off.Currencies.AsNoTracking().ToList();
            else
                return on.Currencies.AsNoTracking().ToList();               
        }

        public IEnumerable<Currency> GetActiveCurrency()
        {
            if (_session.AppMode == "offline")
                return off.Currencies.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Currencies.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public Currency GetCurrency(string code)
        {
            if (_session.AppMode == "offline")
                return off.Currencies.Where(x => x.CurrCode == code).AsNoTracking().FirstOrDefault();
            else
                return on.Currencies.Where(x => x.CurrCode == code).AsNoTracking().FirstOrDefault();
        }

        public void AddCurrency(Currency data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditCurrency(Currency data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Fund
        public IEnumerable<Fund> GetAllFund()
        {
            if (_session.AppMode == "offline")
                return off.Funds.AsNoTracking().ToList();
            else
                return on.Funds.AsNoTracking().ToList();
        }

        public IEnumerable<Fund> GetActiveFund()
        {
            if (_session.AppMode == "offline")
                return off.Funds.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Funds.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public Fund GetFund(string code)
        {
            if (_session.AppMode == "offline")
                return off.Funds.Where(x => x.FundCode == code).AsNoTracking().FirstOrDefault();
            else
                return on.Funds.Where(x => x.FundCode == code).AsNoTracking().FirstOrDefault();
        }

        public IEnumerable<Fund> GetFundByProduct(string code)
        {
            if (_session.AppMode == "offline")
                return off.ProductFunds.Where(x => x.ProductCode == code)
                    .Join(off.Funds, x => x.FundCode, y => y.FundCode, (x, y) => y).OrderBy(x => x.FundCode)
                    .ToList();
            else
                return on.ProductFunds.Where(x => x.ProductCode == code)
                    .Join(on.Funds, x => x.FundCode, y => y.FundCode, (x, y) => y).OrderBy(x => x.FundCode)
                    .ToList();
        }

        public void AddFund(Fund data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }

        public void EditFund(Fund data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Language
        public IEnumerable<Language> GetAllLanguage()
        {
            if (_session.AppMode == "offline")
                return off.Languages.AsNoTracking().ToList();
            else
                return on.Languages.AsNoTracking().ToList();
        }

        public IEnumerable<Language> GetActiveLanguage()
        {
            if (_session.AppMode == "offline")
                return off.Languages.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Languages.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public Language GetLanguage(string code)
        {
            if (_session.AppMode == "offline")
                return off.Languages.Where(x => x.CultureId == code).AsNoTracking().FirstOrDefault();
            else
                return on.Languages.Where(x => x.CultureId == code).AsNoTracking().FirstOrDefault();
        }

        public void AddLanguage(Language data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditLanguage(Language data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Payment Method
        public IEnumerable<PaymentMethod> GetAllPaymentMethod()
        {
            if (_session.AppMode == "offline")
                return off.PaymentMethods.AsNoTracking().ToList();
            else
                return on.PaymentMethods.AsNoTracking().ToList();
        }

        public IEnumerable<PaymentMethod> GetActivePaymentMethod(string productCode)
        {
            if (_session.AppMode == "offline")
                return off.PaymentMethods.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
            { 
                if (productCode == "HLSAVING")
                {
                    return on.PaymentMethods.Where(x => x.IsActive == true && x.PM_ProductCode == "HLSAVING").AsNoTracking().ToList();
                }
                else if (productCode == "HLFUTURE")
                {
                    return on.PaymentMethods.Where(x => x.IsActive == true && x.PM_ProductCode == "0" && (x.PMCode == 1 || x.PMCode == 12)).AsNoTracking().ToList();
                }
                else if (productCode == "HPCP")
                {
                    return on.PaymentMethods.Where(x => x.IsActive == true && x.PM_ProductCode == "0" && x.PMCode != 12).AsNoTracking().ToList();
                }
                else
                {
                    return on.PaymentMethods.Where(x => x.IsActive == true && x.PM_ProductCode == "0").AsNoTracking().ToList();
                }
            }
        }

        public ProductPaymentMethodPremium GetProductPaymentMethodPremium(string productCode, int pmCode, string type)
        {
            if (_session.AppMode == "offline")
                return off.ProductPaymentMethodPremiums.Where(x => x.PMCode == pmCode && x.ProductCode == productCode && x.Type == type).FirstOrDefault();
            else
                return on.ProductPaymentMethodPremiums.Where(x => x.PMCode == pmCode && x.ProductCode == productCode && x.Type == type).FirstOrDefault();
        }

        public PaymentMethod GetPaymentMethod(int code, string productcode)
        {
            if (_session.AppMode == "offline")
                return off.PaymentMethods.Where(x => x.PMCode == code).AsNoTracking().FirstOrDefault();
            else
                if (productcode == "HLSAVING")
                {
                    return on.PaymentMethods.Where(x => x.PMCode == code && x.PM_ProductCode == productcode).AsNoTracking().FirstOrDefault();
                }
                else
                {
                    return on.PaymentMethods.Where(x => x.PMCode == code && x.PM_ProductCode == "0").AsNoTracking().FirstOrDefault();
                }

        }

        public RiderRate GetRiderRate(string code, int age, string category, int mpp)
        {
            return on.RiderRates.Where(x => x.RiderCode == code && x.Age == age && x.Category == category && x.MPP == mpp).AsNoTracking().FirstOrDefault();

        }

        public SumInsuredRate GetSumInsured(string productCode, int insage, int mpp)
        {
            return on.SumInsuredRates.Where(x => x.ProductCode == productCode && x.InsuredAge == insage && x.MPP == mpp).AsNoTracking().FirstOrDefault();

        }

        public MinimalPremi GetMinimalPremi(string productCode, int code, int mpp)
        {
            //if (_session.AppMode == "offline")
            //    return off.MinimalPremis.Where(x => x.PMCode == code).AsNoTracking().FirstOrDefault();
            //else
            if (productCode == "HLGOLDENA" || productCode == "HLGOLDENB")
            {
                return on.MinimalPremis.Where(x => x.ProductCode == productCode && x.MPP == mpp).AsNoTracking().FirstOrDefault();
            }
            else
            {
                return on.MinimalPremis.Where(x => x.ProductCode == productCode && x.PMCode == code && x.MPP == mpp).AsNoTracking().FirstOrDefault();
            }
        }


        public void AddPaymentMethod(PaymentMethod data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditPaymentMethod(PaymentMethod data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region RiskClass
        public InfoMedical GetInfoMedical(int age, decimal sumInsured, string productType)
        {
            if (_session.AppMode == "offline")
                return on.InfoMedicals.Where(x => x.ProductType == productType && age >= x.MinAge && age <= x.MaxAge && sumInsured >= x.MinSumInsured && sumInsured <= x.MaxSumInsured).FirstOrDefault();
            else
                return on.InfoMedicals.Where(x => x.ProductType == productType && age >= x.MinAge && age <= x.MaxAge && sumInsured >= x.MinSumInsured && sumInsured <= x.MaxSumInsured).FirstOrDefault();
        }
        public TrigerSumInsured GetTrigerSumInsured(int age, string productCode)
        {
            if (_session.AppMode == "offline")
                return on.TrigerSumInsureds.Where(x => x.ProductCode == productCode && age >= x.MinAge && age <= x.MaxAge).FirstOrDefault();
            else
                return on.TrigerSumInsureds.Where(x => x.ProductCode == productCode && age >= x.MinAge && age <= x.MaxAge).FirstOrDefault();
        }
        public IEnumerable<RiskClass> GetAllRiskClass()
        {
            if (_session.AppMode == "offline")
                return off.RiskClasses.AsNoTracking().ToList();
            else
                return on.RiskClasses.AsNoTracking().ToList();
        }

        public IEnumerable<RiskClass> GetActiveRiskClass()
        {
            if (_session.AppMode == "offline")
                return off.RiskClasses.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.RiskClasses.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public RiskClass GetRiskClass(int id)
        {
            if (_session.AppMode == "offline")
                return off.RiskClasses.Where(x => x.RiskClassId == id).AsNoTracking().FirstOrDefault();
            else
                return on.RiskClasses.Where(x => x.RiskClassId == id).AsNoTracking().FirstOrDefault();
        }

        public void AddRiskClass(RiskClass data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditRiskClass(RiskClass data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Rate
        public TraditionalRate GetTraditionalRate(string type, int childAge, int insAge, int term, int period)
        {
            if (_session.AppMode == "offline")
                return off.TraditionalRates.Where(x => x.TraditionalRateType == type && x.ChildAge == childAge && x.InsuredAge == insAge && x.PremiumTerm == term && x.InsurancePeriod == period).AsNoTracking().FirstOrDefault();
            else
                return on.TraditionalRates.Where(x => x.TraditionalRateType == type && x.ChildAge == childAge && x.InsuredAge == insAge && x.PremiumTerm == term && x.InsurancePeriod == period).AsNoTracking().FirstOrDefault();
        }
        public ProductRate GetProductRate(string productCode, int? childAge, int insAge, int? mpp)
        {
            if (_session.AppMode == "offline")
                return off.ProductRates.Where(x => x.ProductCode == productCode && x.InsuredAge == insAge && x.Age == childAge && x.MPP == mpp).AsNoTracking().FirstOrDefault();
            else
                return on.ProductRates.Where(x => x.ProductCode == productCode && x.InsuredAge == insAge && (childAge.HasValue ? x.Age == childAge.Value : true) && x.MPP == mpp).AsNoTracking().FirstOrDefault();
        }

        public TraditionalRiderRate GetTraditionalRiderRate(string code, int? riderType, string type, int? insAge, int? basicCov, int? basicTerm, int? riderCov, int? riderTerm)
        {
            if (_session.AppMode == "offline")
                return on.TraditionalRiderRates.Where(x => x.RiderCode == code && x.RiderTypeId == riderType && x.TraditionalRateType == type && x.InsuredAge == insAge &&
                    x.BasicCov == basicCov && x.BasicPremiTerm == basicTerm && x.RiderCov == riderCov && x.RiderPremiTerm == riderTerm).AsNoTracking().FirstOrDefault();
            else
                return on.TraditionalRiderRates.Where(x => x.RiderCode == code && x.RiderTypeId == riderType && x.TraditionalRateType == type && x.InsuredAge == insAge &&
                    x.BasicCov == basicCov && x.BasicPremiTerm == basicTerm && x.RiderCov == riderCov && x.RiderPremiTerm == riderTerm).AsNoTracking().FirstOrDefault();
        }

        public AnnuityFactor GetAnnuityFactor(int? year, int? factor, string productType)
        {
            if (_session.AppMode == "offline")
                return on.AnnuityFactors.Where(x => x.Year == year && x.Factor == factor && x.ProductType == productType).FirstOrDefault();
            else
                return on.AnnuityFactors.Where(x => x.Year == year && x.Factor == factor && x.ProductType == productType).FirstOrDefault();
        }

        public RiderRate GetUnitLinkRate(string riderCode, int? riderTypeId, int? age, int? riskClass, string category)
        {
            if (_session.AppMode == "offline")
                return on.RiderRates.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderTypeId && x.Age == age &&
                    x.RiskClass == riskClass && x.Category == category).AsNoTracking().FirstOrDefault();
            else
                return on.RiderRates.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderTypeId && x.Age == age &&
                    x.RiskClass == riskClass && x.Category == category).AsNoTracking().FirstOrDefault();
        }
        public SAMultiplier GetSAMultiplier(string productCode, int age, string type)
        {
            if (_session.AppMode == "offline")
                return on.SAMultipliers.Where(x => x.ProductCode == productCode && x.EntryAge ==  age && x.Type == type).AsNoTracking().FirstOrDefault();
            else
                return on.SAMultipliers.Where(x => x.ProductCode == productCode && x.EntryAge == age && x.Type == type).AsNoTracking().FirstOrDefault();
        }
        public TarifPremi GetTarifPremi(string productCode ,string type, int? age,int? term, int? planValue)
        {
            if (_session.AppMode == "offline")
                return on.TarifPremis.Where(x => x.ProductCode == productCode && x.Type == type && x.EntryAge == age && x.MasaPembayaran == term).AsNoTracking().FirstOrDefault();
            else
                return on.TarifPremis.Where(x => x.ProductCode == productCode && x.Type == type && x.EntryAge == age && x.MasaPembayaran == term && x.PlanValue == planValue).AsNoTracking().FirstOrDefault();
        }
        #endregion

        #region Log 
        public void CreateLog(string agentCode,string agentName)
        {
            Sales.Illustration.Web.Models.Log data = new Sales.Illustration.Web.Models.Log();

            data.AgentCode = agentCode;
            data.AgentName = agentName;
            data.LoginTime = DateTime.Now;

            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public IEnumerable<Log> GetLogHistory(string monthyear)
        {
            var arrmonthyear = monthyear.Split(new [] {"-"}, StringSplitOptions.None);
            var month = Int32.Parse(arrmonthyear[0]);
            var year = arrmonthyear[1];
            if (_session.AppMode == "offline")
                return null;// off.Logs.Where(x => x.LoginTime.Year.ToString() == year && x.LoginTime.Month == month).GroupBy(x => x.AgentCode).Select(x => x.OrderByDescending(y => y.LoginTime).FirstOrDefault()).AsNoTracking().ToList();
            else
            {
                return on.Logs.Where(x => Convert.ToDateTime(x.LoginTime).Year.ToString() == year && Convert.ToDateTime(x.LoginTime).Month == month).GroupBy(x => x.AgentCode).Select(x => x.OrderByDescending(y => y.LoginTime).FirstOrDefault()).OrderByDescending(x => x.LoginTime).AsNoTracking().ToList();
            }

                
            }

        #endregion 
    }
    
}
