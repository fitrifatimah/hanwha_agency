﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Models
{
    public class RiderModel : Controller
    {
        //
        // GET: /RiderModel/

        private OnlineEntities on = new OnlineEntities();
        private OfflineEntities off = new OfflineEntities();
        private Sales.Illustration.Web.Helper.Generator gen = new Sales.Illustration.Web.Helper.Generator();

        public IEnumerable<Rider> GetAllRider()
        {
            if (_session.AppMode == "offline")
                return off.Riders.AsNoTracking().ToList();
            else
                return on.Riders.AsNoTracking().ToList();
        }

        public IEnumerable<Rider> GetActiveRider()
        {
            if (_session.AppMode == "offline")
                return off.Riders.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Riders.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public IEnumerable<Rider> GetRiderByProduct(string productCode)
        {
            if (_session.AppMode == "offline")
                return off.RiderProducts.Where(x => x.ProductCode == productCode)
                    .Join(off.Riders, x => x.RiderCode, y => y.RiderCode, (x, y) => y).OrderBy(x => x.OrderNo)
                    .ToList();
            else
                return on.RiderProducts.Where(x => x.ProductCode == productCode)
                    .Join(on.Riders, x => x.RiderCode, y => y.RiderCode, (x, y) => y).OrderBy(x => x.OrderNo)
                    .ToList();
        }

        public IEnumerable<RiderProduct> GetRiderProduct(string productCode, string riderCode)
        {
            if (_session.AppMode == "offline")
                return off.RiderProducts.Where(x => x.ProductCode == productCode && x.RiderCode == riderCode).ToList();
            else
                return on.RiderProducts.Where(x => x.ProductCode == productCode && x.RiderCode == riderCode).ToList();
        }

        public Rider GetRider(string code)
        {
            if (_session.AppMode == "offline")
                return off.Riders.Where(x => x.RiderCode == code).AsNoTracking().FirstOrDefault();
            else
                return on.Riders.Where(x => x.RiderCode == code).AsNoTracking().FirstOrDefault();
        }

        public UnitMapping GetUnitMapping(string riderCode, int? riderType, decimal up)
        {
            if (_session.AppMode == "offline")
                return on.UnitMappings.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.SIFrom <= up && x.SITo >= up).OrderByDescending(x => x.Unit).FirstOrDefault();
            else
                return on.UnitMappings.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.SIFrom <= up && x.SITo >= up).OrderByDescending(x => x.Unit).FirstOrDefault();
        }

        public UnitMapping GetMaxUnitMapping(string riderCode, int? riderType, decimal up)
        {
            UnitMapping ret = new UnitMapping();
            if (_session.AppMode == "offline")
                ret = on.UnitMappings.Where(x => x.RiderCode == riderCode && x.SIFrom <= up && x.SITo >= up && x.Unit > 0).OrderByDescending(x => x.RiderTypeId).FirstOrDefault();
            else
                ret =  on.UnitMappings.Where(x => x.RiderCode == riderCode && x.SIFrom <= up && x.SITo >= up && x.Unit > 0).OrderByDescending(x => x.RiderTypeId).FirstOrDefault();

            if(ret == null)
                ret = on.UnitMappings.Where(x => x.RiderCode == riderCode).OrderByDescending(x => x.SITo).FirstOrDefault();

            return ret;
        }

        public SantunanHarian GetSantunanHarian(string riderCode, int riderType)
        {
            if (_session.AppMode == "offline")
                return on.SantunanHarians.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderType).FirstOrDefault();
            else
                return on.SantunanHarians.Where(x => x.RiderCode == riderCode && x.RiderTypeId == riderType).FirstOrDefault();
        }

        public void AddRider(Rider data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditRider(Rider data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }

    }
}
