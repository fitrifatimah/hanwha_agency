﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Models
{
    public class TransLogModel : Controller
    {
        //
        // GET: /TransLogModel/

        private OnlineEntities on = new OnlineEntities();
        private OfflineEntities off = new OfflineEntities();
        private Sales.Illustration.Web.Helper.Generator gen = new Sales.Illustration.Web.Helper.Generator();

        #region Header
        public TransLog GetTransLog(string code)
        {
            if (_session.AppMode == "offline")
                return off.TransLogs.Where(x => x.TransCode == code).AsNoTracking().FirstOrDefault();
            else
                return on.TransLogs.Where(x => x.TransCode == code).AsNoTracking().FirstOrDefault();
        }

        public IEnumerable<TransLog> GetTransLogByAgent(string agentcode)
        {
            if (_session.AppMode == "offline")
                return off.TransLogs.Where(x => x.AgentCode == agentcode).OrderByDescending(x => x.TransDate).Take(100).ToList();
            else
                return on.TransLogs.Where(x => x.AgentCode == agentcode).OrderByDescending(x => x.TransDate).Take(100).ToList();
        }

        public void AddTransLog(TransLog data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditTransLog(TransLog data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Nasabah Data
        public NasabahData GetNasabahData(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.NasabahDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
            else
                return on.NasabahDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
        }

        public void AddNasabahData(NasabahData data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditNasabahData(NasabahData data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Premium Data
        public PremiumData GetPremiumData(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.PremiumDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
            else
                return on.PremiumDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
        }

        public void AddPremiumData(PremiumData data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditPremiumData(PremiumData data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }
        #endregion

        #region Fund Investment Data
        public InvestmentData GetInvestmentData(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.InvestmentDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
            else
                return on.InvestmentDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
        }

        public void AddInvestmentData(InvestmentData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offAdd = new OnlineEntities())
                {
                    var add = new InvestmentData();
                    add.TransCode = data.TransCode;
                    add.FundCode = data.FundCode;
                    add.FundValue = data.FundValue;

                    offAdd.Entry(add).State = EntityState.Added;
                    offAdd.SaveChanges();
                }
            }
            else
            {
                using (var onAdd = new OnlineEntities())
                {
                    var add = new InvestmentData();
                    add.TransCode = data.TransCode;
                    add.FundCode = data.FundCode;
                    add.FundValue = data.FundValue;

                    onAdd.Entry(add).State = EntityState.Added;
                    onAdd.SaveChanges();
                }
            }
        }

        public void DeleteInvestmentData(InvestmentData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offDel = new OfflineEntities())
                {
                    var del = offDel.InvestmentDatas.Where(x => x.TransCode == data.TransCode);
                    offDel.InvestmentDatas.RemoveRange(del);
                    offDel.SaveChanges();
                }
            }
            else
            {
                using (var onDel = new OnlineEntities())
                {
                    var del = onDel.InvestmentDatas.Where(x => x.TransCode == data.TransCode);
                    onDel.InvestmentDatas.RemoveRange(del);
                    onDel.SaveChanges();
                }
            }
        }
        #endregion

        #region Rider
        public RiderData GetRiderData(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.RiderDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
            else
                return on.RiderDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
        }

        public void AddRiderData(RiderData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offAdd = new OnlineEntities())
                {
                    var add = new RiderData();
                    add.TransCode = data.TransCode;
                    add.RiderCode = data.RiderCode;
                    add.RiderTypeId = data.RiderTypeId;
                    add.Unit = data.Unit;
                    add.COR = data.COR;
                    add.SumInsured = data.SumInsured;
                    add.AllocationPerc = data.AllocationPerc;
                    add.ddlRiderCoverage = data.ddlRiderCoverage;

                    offAdd.Entry(add).State = EntityState.Added;
                    offAdd.SaveChanges();
                }
            }
            else
            {
                using (var onAdd = new OnlineEntities())
                {
                    var add = new RiderData();
                    add.TransCode = data.TransCode;
                    add.RiderCode = data.RiderCode;
                    add.RiderTypeId = data.RiderTypeId;
                    add.Unit = data.Unit;
                    add.COR = data.COR;
                    add.SumInsured = data.SumInsured;
                    add.AllocationPerc = data.AllocationPerc;
                    add.ddlRiderCoverage = data.ddlRiderCoverage;

                    onAdd.Entry(add).State = EntityState.Added;
                    onAdd.SaveChanges();
                }
            }
        }

        public void DeleteRiderData(string transCode)
        {
            if (_session.AppMode == "offline")
            {

                using (var on = new OfflineEntities())
                {
                    var allDatas = on.RiderDatas.Where(x => x.TransCode == transCode);
                    on.RiderDatas.RemoveRange(allDatas);
                    on.SaveChanges();
                }
            }
            else
            {
                using (var on = new OnlineEntities())
                {
                    var allDatas = on.RiderDatas.Where(x => x.TransCode == transCode);
                    on.RiderDatas.RemoveRange(allDatas);
                    on.SaveChanges();
                }
            }
        }

        //public void DeleteRiderData(RiderData data)
        //{
        //    if (_session.AppMode == "offline")
        //    {
        //        using (var offDel = new OfflineEntities())
        //        {
        //            var del = offDel.RiderDatas.Where(x => x.TransCode == data.TransCode);
        //            offDel.RiderDatas.RemoveRange(del);
        //            offDel.SaveChanges();
        //        }
        //    }
        //    else
        //    {
        //        using (var onDel = new OnlineEntities())
        //        {
        //            var del = onDel.RiderDatas.Where(x => x.TransCode == data.TransCode);
        //            onDel.RiderDatas.RemoveRange(del);
        //            onDel.SaveChanges();
        //        }
        //    }
        //}
        #endregion

        #region Additional Insured
        public AdditionalInsuredData GetAdditionalInsuredData(int element,string transcode, string name, DateTime? dob, string gender, int? riskClass, int? age, string relation)
        {
            var data = new List<AdditionalInsuredData>();
            var ret = new AdditionalInsuredData();
            if (_session.AppMode == "offline")
                data = off.AdditionalInsuredDatas.Where(x => x.TransCode == transcode && x.AdditionalInsuredAge == age && x.AdditionalInsuredDOB == dob && x.AdditionalInsuredGender == gender && x.AdditionalInsuredName == name && x.AdditionalInsuredRelation == relation && x.AdditionalInsuredRiskClassId == riskClass).ToList();
            else
                data = on.AdditionalInsuredDatas.Where(x => x.TransCode == transcode && x.AdditionalInsuredAge == age && x.AdditionalInsuredDOB == dob && x.AdditionalInsuredGender == gender && x.AdditionalInsuredName == name && x.AdditionalInsuredRelation == relation && x.AdditionalInsuredRiskClassId == riskClass).ToList();

            if (data.Count() > 1)
                if (_session.AppMode == "offline")
                    ret = off.AdditionalInsuredDatas.Where(x => x.TransCode == transcode).ToList().ElementAt(element);
                else
                    ret = on.AdditionalInsuredDatas.Where(x => x.TransCode == transcode).ToList().ElementAt(element);
            else
                ret = data.ElementAt(0);    

            return ret;
        }

        public List<AdditionalInsuredData> GetAdditionalInsuredDatas(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.AdditionalInsuredDatas.Where(x => x.TransCode == transcode).ToList();
            else
                return on.AdditionalInsuredDatas.Where(x => x.TransCode == transcode).ToList();
        }

        public void DeleteAdditionalInsuredDataWithoutThis(List<AdditionalInsuredData> datas, string transCode)
        {
            if (_session.AppMode == "offline")
            {

            } else
            {
                using (var on = new OnlineEntities())
                {
                    var allDatas = GetAdditionalInsuredDatas(transCode).Where(x=>x.AdditionalInsuredDataId == 0);
                    foreach (var data in datas)
                    {
                        
                    }
                }
            }
        }

        public void DeleteAdditionalInsuredData(string transCode)
        {
            if (_session.AppMode == "offline")
            {

                using (var on = new OfflineEntities())
                {
                    var allDatas = on.AdditionalInsuredDatas.Where(x => x.TransCode == transCode);
                    foreach (var d in allDatas)
                    {
                        var delRider = on.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == d.AdditionalInsuredDataId);
                        on.AdditionalInsuredRiderDatas.RemoveRange(delRider);
                    }
                    on.AdditionalInsuredDatas.RemoveRange(allDatas);
                    on.SaveChanges();
                }
            }
            else
            {
                using (var on = new OnlineEntities())
                {
                    var allDatas = on.AdditionalInsuredDatas.Where(x => x.TransCode == transCode).AsNoTracking().ToList();
                    foreach (var d in allDatas)
                    {
                        var delRider = on.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == d.AdditionalInsuredDataId);
                        on.AdditionalInsuredRiderDatas.RemoveRange(delRider);
                    }
                    
                    on.AdditionalInsuredDatas.RemoveRange(on.AdditionalInsuredDatas.Where(x => x.TransCode == transCode));
                    on.SaveChanges();
                }
            }
        }

        public AdditionalInsuredRiderData GetAdditionalInsuredRiderData(int id)
        {
            if (_session.AppMode == "offline")
                return off.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == id).AsNoTracking().FirstOrDefault();
            else
                return on.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == id).AsNoTracking().FirstOrDefault();
        }

        public void AddAdditionalInsuredData(AdditionalInsuredData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offAdd = new OnlineEntities())
                {
                    var add = new AdditionalInsuredData();
                    add.TransCode = data.TransCode;
                    add.AdditionalInsuredName = data.AdditionalInsuredName;
                    add.AdditionalInsuredDOB = data.AdditionalInsuredDOB;
                    add.AdditionalInsuredGender = data.AdditionalInsuredGender;
                    add.AdditionalInsuredRiskClassId = data.AdditionalInsuredRiskClassId;
                    add.AdditionalInsuredAge = data.AdditionalInsuredAge;
                    add.AdditionalInsuredRelation = data.AdditionalInsuredRelation;

                    offAdd.Entry(add).State = EntityState.Added;
                    offAdd.SaveChanges();
                }
            }
            else
            {
                using (var onAdd = new OnlineEntities())
                {
                    var add = new AdditionalInsuredData();
                    add.TransCode = data.TransCode;
                    add.AdditionalInsuredName = data.AdditionalInsuredName;
                    add.AdditionalInsuredDOB = data.AdditionalInsuredDOB;
                    add.AdditionalInsuredGender = data.AdditionalInsuredGender;
                    add.AdditionalInsuredRiskClassId = data.AdditionalInsuredRiskClassId;
                    add.AdditionalInsuredAge = data.AdditionalInsuredAge;
                    add.AdditionalInsuredRelation = data.AdditionalInsuredRelation;

                    onAdd.Entry(add).State = EntityState.Added;
                    onAdd.SaveChanges();
                }
            }
        }

        public void DeleteAdditionalInsuredData(AdditionalInsuredData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offDel = new OfflineEntities())
                {
                    var del = offDel.AdditionalInsuredDatas.Where(x => x.TransCode == data.TransCode);
                    offDel.AdditionalInsuredDatas.RemoveRange(del);
                    offDel.SaveChanges();
                }
            }
            else
            {
                using (var onDel = new OnlineEntities())
                {
                    var del = onDel.AdditionalInsuredDatas.Where(x => x.TransCode == data.TransCode);
                    foreach(var d in del)
                    {
                        var delRider = onDel.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == d.AdditionalInsuredDataId);
                        onDel.AdditionalInsuredRiderDatas.RemoveRange(delRider);
                    }

                    onDel.AdditionalInsuredDatas.RemoveRange(del);
                    onDel.SaveChanges();
                }
            }
        }

        public void AddAdditionalInsuredRiderData(AdditionalInsuredRiderData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offAdd = new OnlineEntities())
                {
                    var add = new AdditionalInsuredRiderData();
                    add.AdditionalInsuredDataId = data.AdditionalInsuredDataId;
                    add.RiderCode = data.RiderCode;
                    add.RiderTypeId = data.RiderTypeId;
                    add.Unit = data.Unit;
                    add.SumInsured = data.SumInsured;
                    add.AllocationPerc = data.AllocationPerc;
                    add.COR = data.COR;

                    offAdd.Entry(add).State = EntityState.Added;
                    offAdd.SaveChanges();
                }
            }
            else
            {
                using (var onAdd = new OnlineEntities())
                {
                    var add = new AdditionalInsuredRiderData();
                    add.AdditionalInsuredDataId = data.AdditionalInsuredDataId;
                    add.RiderCode = data.RiderCode;
                    add.RiderTypeId = data.RiderTypeId;
                    add.Unit = data.Unit;
                    add.SumInsured = data.SumInsured;
                    add.AllocationPerc = data.AllocationPerc;
                    add.COR = data.COR;

                    onAdd.Entry(add).State = EntityState.Added;
                    onAdd.SaveChanges();
                }
            }
        }

        public void DeleteAdditionalInsuredRiderData(AdditionalInsuredRiderData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offDel = new OfflineEntities())
                {
                    var del = offDel.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == data.AdditionalInsuredDataId);
                    offDel.AdditionalInsuredRiderDatas.RemoveRange(del);
                    offDel.SaveChanges();
                }
            }
            else
            {
                using (var onDel = new OnlineEntities())
                {
                    var del = onDel.AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == data.AdditionalInsuredDataId);
                    onDel.AdditionalInsuredRiderDatas.RemoveRange(del);
                    onDel.SaveChanges();
                }
            }
        }
        #endregion

        #region Top Up Withdrawal
        public TopUpWithdrawalData GetTopUpWithdrawalData(string transcode)
        {
            if (_session.AppMode == "offline")
                return off.TopUpWithdrawalDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
            else
                return on.TopUpWithdrawalDatas.Where(x => x.TransCode == transcode).AsNoTracking().FirstOrDefault();
        }

        public void AddTopUpWithdrawalData(TopUpWithdrawalData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offAdd = new OnlineEntities())
                {
                    var add = new TopUpWithdrawalData();
                    add.TransCode = data.TransCode;
                    add.IsTopUp = data.IsTopUp;
                    add.TransYear = data.TransYear;
                    add.TransAmount = data.TransAmount;

                    offAdd.Entry(add).State = EntityState.Added;
                    offAdd.SaveChanges();
                }
            }
            else
            {
                using (var onAdd = new OnlineEntities())
                {
                    var add = new TopUpWithdrawalData();
                    add.TransCode = data.TransCode;
                    add.IsTopUp = data.IsTopUp;
                    add.TransYear = data.TransYear;
                    add.TransAmount = data.TransAmount;

                    onAdd.Entry(add).State = EntityState.Added;
                    onAdd.SaveChanges();
                }
            }
        }

        public void DeleteTopUpWithdrawalData(TopUpWithdrawalData data)
        {
            if (_session.AppMode == "offline")
            {
                using (var offDel = new OfflineEntities())
                {
                    var del = offDel.TopUpWithdrawalDatas.Where(x => x.TransCode == data.TransCode);
                    offDel.TopUpWithdrawalDatas.RemoveRange(del);
                    offDel.SaveChanges();
                }
            }
            else
            {
                using (var onDel = new OnlineEntities())
                {
                    var del = onDel.TopUpWithdrawalDatas.Where(x => x.TransCode == data.TransCode);
                    onDel.TopUpWithdrawalDatas.RemoveRange(del);
                    onDel.SaveChanges();
                }
            }
        }

        public void DeleteTopUpWithdrawalData(string transcode)
        {
            if (_session.AppMode == "offline")
            {
                using (var offDel = new OfflineEntities())
                {
                    var del = offDel.TopUpWithdrawalDatas.Where(x => x.TransCode == transcode);
                    offDel.TopUpWithdrawalDatas.RemoveRange(del);
                    offDel.SaveChanges();
                }
            }
            else
            {
                using (var onDel = new OnlineEntities())
                {
                    var del = onDel.TopUpWithdrawalDatas.Where(x => x.TransCode == transcode);
                    onDel.TopUpWithdrawalDatas.RemoveRange(del);
                    onDel.SaveChanges();
                }
            }
        }
        #endregion
    }
}
