﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Models
{
    public class UserMembershipModel : Controller
    {
        //
        // GET: /UserMembershipModel/
        private string AppMode = ConfigurationManager.AppSettings["ApplicationMode"];

        private OnlineEntities on = new OnlineEntities();
        private OfflineEntities off = new OfflineEntities();
        private Sales.Illustration.Web.Helper.Generator gen = new Sales.Illustration.Web.Helper.Generator();

        public List<UserMembership> GetAllUser()
        {
            if (_session.AppMode == "offline")
                return off.UserMemberships.AsNoTracking().ToList();
            else
                return on.UserMemberships.AsNoTracking().ToList();
        }

        public UserMembership GetUser(string username)
        {
            if (_session.AppMode == "offline")
                return off.UserMemberships.Where(x => x.AgentCode == username).AsNoTracking().FirstOrDefault();
            else
                return on.UserMemberships.Where(x => x.AgentCode == username).AsNoTracking().FirstOrDefault();
        }

        public UserMembership GetUserApi(string username)
        {
            if (AppMode == "offline")
                return off.UserMemberships.Where(x => x.AgentCode == username).AsNoTracking().FirstOrDefault();
            else
                return on.UserMemberships.Where(x => x.AgentCode == username).AsNoTracking().FirstOrDefault();
        }

        public UserMembership CheckLogin(string username, string password)
        {
            if (_session.AppMode == "offline")
                return off.UserMemberships.Where(x => x.AgentCode == username && x.EncPass == password).FirstOrDefault();
            else
                return on.UserMemberships.Where(x => x.AgentCode == username && x.EncPass == password && x.IsActive == true).FirstOrDefault();
        }

        public void UserAdd(UserMembership user)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(user).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(user).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void UserAddApi(UserMembership user)
        {
            if (AppMode == "offline")
            {
                off.Entry(user).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(user).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void UserEdit(UserMembership user)
        {
            var newuser = GetUser(user.AgentCode);
            if (newuser != null)
            {
                if (_session.AppMode == "offline")
                {
                    off.Entry(user).State = newuser != null ? EntityState.Modified : EntityState.Added;
                    off.SaveChanges();
                }
                else
                {
                    on.Entry(user).State = newuser != null ? EntityState.Modified : EntityState.Added;
                    on.SaveChanges();
                }
            }
            else
            {
                UserAdd(user);
                UserEdit(user);
            }
        }

        public void UserEditApi(UserMembership user)
        {
            var newuser = GetUserApi(user.AgentCode);
            if (newuser != null)
            {
                if (AppMode == "offline")
                {
                    off.Entry(user).State = newuser != null ? EntityState.Modified : EntityState.Added;
                    off.SaveChanges();
                }
                else
                {
                    on.Entry(user).State = newuser != null ? EntityState.Modified : EntityState.Added;
                    on.SaveChanges();
                }
            }
            else
            {
                UserAddApi(user);
                UserEditApi(user);
            }
        }

    }
}
