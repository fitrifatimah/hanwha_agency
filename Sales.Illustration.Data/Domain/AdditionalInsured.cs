﻿using System;

namespace Sales.Illustration.Data.Domain
{
    public class AdditionalInsured
    {
        public string Nama { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public decimal? BiayaAsuransi { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
    }
}
