﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class Product
    {
        public Product()
        {
            this.ProductAllocationRates = new HashSet<ProductAllocationRate>();
            this.ProductCurrencies = new HashSet<ProductCurrency>();
            this.ProductFunds = new HashSet<ProductFund>();
            this.ProductPaymentMethodPremiums = new HashSet<ProductPaymentMethodPremium>();
            this.RiderProducts = new HashSet<RiderProduct>();
        }

        [Key]
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductParent { get; set; }
        public string ProductType { get; set; }
        public Nullable<int> PolicyHolderMinAge { get; set; }
        public Nullable<int> PolicyHolderMaxAge { get; set; }
        public Nullable<int> InsuredMinAge { get; set; }
        public Nullable<int> InsuredMaxAge { get; set; }
        public Nullable<int> CovAge { get; set; }
        public Nullable<decimal> AdminFee { get; set; }
        public Nullable<decimal> MinRegularTopUp { get; set; }
        public Nullable<double> RegularTopUpRate { get; set; }
        public Nullable<decimal> MinSingleTopUp { get; set; }
        public Nullable<decimal> MinWithdrawal { get; set; }
        public Nullable<decimal> MinWithdrawalBalance { get; set; }
        public Nullable<int> MinPaymentYear { get; set; }
        public Nullable<int> MinPercSumInsured { get; set; }
        public Nullable<decimal> MinAmountSumInsured { get; set; }
        public Nullable<decimal> MinRemainingBalance { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<ProductAllocationRate> ProductAllocationRates { get; set; }
        public virtual ICollection<ProductCurrency> ProductCurrencies { get; set; }
        public virtual ICollection<ProductFund> ProductFunds { get; set; }
        public virtual ICollection<ProductPaymentMethodPremium> ProductPaymentMethodPremiums { get; set; }
        public virtual ICollection<RiderProduct> RiderProducts { get; set; }
    }
}
