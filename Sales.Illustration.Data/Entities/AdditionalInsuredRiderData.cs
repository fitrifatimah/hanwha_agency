﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class AdditionalInsuredRiderData
    {
        [Key]
        public long AdditionalInsuredRiderDataId { get; set; }
        public Nullable<long> AdditionalInsuredDataId { get; set; }
        public Nullable<int> RiderId { get; set; }
        public Nullable<int> RiderTypeId { get; set; }
        public Nullable<decimal> SumInsured { get; set; }
        public Nullable<decimal> COR { get; set; }

        public virtual AdditionalInsuredData AdditionalInsuredData { get; set; }
    }
}
