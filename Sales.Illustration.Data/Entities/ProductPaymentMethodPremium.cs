﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class ProductPaymentMethodPremium
    {
        [Key]
        public int ProductPaymentMethodPremiumId { get; set; }
        public string ProductCode { get; set; }
        public string PMCode { get; set; }
        public Nullable<decimal> MinPremium { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual Product Product { get; set; }
    }
}
