﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class ProductAllocationRate
    {
        [Key]
        public int ProductAllocationRateId { get; set; }
        public string ProductCode { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<double> RatePremi { get; set; }
        public Nullable<double> RateSingleTopUp { get; set; }
        public Nullable<double> RateWithdrawal { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual Product Product { get; set; }
    }
}
